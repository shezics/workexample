# Work Example#

This is Single Page Web Application Project , to be built on AngularJS, Bootstrap, SCSS, GulpJS, and other great tools.


### How to get application set up? ###

####Pre-requisite####
* `npm` from node.js
* `sass` for compiling *.scss files. ( you have to install 'rails installer' and 'compass' ) 

####Steps####
* `clone` this project.
* `cd` to project root and run `npm install && bower install`.
* run `gulp serve` for running application locally on `localhost:7000`. Any file changes will auto reload the app.

### Contributors ###
