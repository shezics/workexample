'use strict';

var gulp = require('gulp');

var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
});

gulp.task('injector:css', function() {
  return gulp.src('src/assets/styles/main.scss')
    .pipe($.sourcemaps.init())
    .pipe($.sass().on('error', $.sass.logError))
    .pipe($.sourcemaps.write('./'))
    .pipe(gulp.dest('src/assets/styles/'))
});

//gulp.task('styles', ['injector:css:preprocessor'], function () {
//  return gulp.src('src/assets/styles/main.scss')
//    .pipe($.rubySass({style: 'expanded', "sourcemap=none": true}))
//    .on('error', function handleError(err) {
//      console.error(err.toString());
//      this.emit('end');
//    })
//    .pipe($.autoprefixer())
//    .pipe(gulp.dest('src/assets/styles/'));
//});
//
//gulp.task('injector:css:preprocessor', function () {
//  return gulp.src('src/assets/styles/main.scss')
//    .pipe($.inject(gulp.src([
//        'src/assets/styles/*.scss',
//        '!src/assets/styles/main.scss'
//    ], {read: false}), {
//      transform: function(filePath) {
//        filePath = filePath.replace('src/app/', '');
//        filePath = filePath.replace('src/components/', '../components/');
//        return '@import \'' + filePath + '\';';
//      },
//      starttag: '// injector',
//      endtag: '// endinjector',
//      addRootSlash: false
//    }))
//    .pipe(gulp.dest('src/assets/styles/'));
//});
//
//gulp.task('injector:css', ['styles'], function () {
//  return gulp.src('src/index.html')
//    .pipe($.inject(gulp.src('.tmp/assets/styles/*.css', {
//        read: false
//    }), {
//      ignorePath: '.tmp',
//      addRootSlash: false
//    }))
//    .pipe(gulp.dest('src/'));
//});

gulp.task('scripts', function () {
  return gulp.src('src/**/*.js');
    //.pipe($.jshint())
    //.pipe($.jshint.reporter('jshint-stylish'));
});

gulp.task('injector:js', ['scripts'], function () {
  return gulp.src(['src/index.html', '.tmp/index.html'])
    .pipe($.inject(gulp.src([
      'src/**/*.js',
      '!src/assets/lib/**/*.js',
      '!src/**/*.spec.js',
      '!src/**/*.mock.js'
    ]).pipe($.angularFilesort()), {
      ignorePath: 'src',
      addRootSlash: false
    }))
    .pipe(gulp.dest('src/'));
});

gulp.task('partials', function () {
  //return gulp.src(['src/{app,components,partials}/**/*.html', '.tmp/{app,components,partials}/**/*.html'])
  return gulp.src(['src/**/*.html', '!src/index.html'])
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }))
    .pipe($.angularTemplatecache('templateCacheHtml.js', {
      module: 'workExample'
    }))
    .pipe(gulp.dest('.tmp/inject/'));
});

gulp.task('html', ['wiredep', 'injector:js', 'injector:css', 'partials'], function () {
  var htmlFilter = $.filter('*.html');
  var jsFilter = $.filter('**/*.js');
  var cssFilter = $.filter('**/*.css');
  var assets;

  return gulp.src(['src/*.html', '.tmp/*.html'])
    .pipe($.inject(gulp.src('.tmp/inject/templateCacheHtml.js', {read: false}), {
      starttag: '<!-- inject:partials -->',
      ignorePath: '.tmp',
      addRootSlash: false
    }))
    .pipe(assets = $.useref.assets())
    .pipe($.rev())
    .pipe(jsFilter)
    .pipe($.ngAnnotate())
    .pipe($.uglify({preserveComments: $.uglifySaveLicense}))
    .pipe(jsFilter.restore())
    .pipe(cssFilter)
    .pipe($.replace('bower_components/bootstrap-sass-official/assets/fonts/bootstrap','assets/fonts'))
    .pipe($.csso())
    .pipe(cssFilter.restore())
    .pipe(assets.restore())
    .pipe($.useref())
    .pipe($.revReplace())
    .pipe(htmlFilter)
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }))
    .pipe(htmlFilter.restore())
    .pipe(gulp.dest('dist/'))
    .pipe($.size({ title: 'dist/', showFiles: true }));
});

gulp.task('images', function () {
  return gulp.src('src/assets/images/*.*')
    .pipe($.imagemin({
      optimizationLevel: 3,
      progressive: true,
      interlaced: true
    }))
    .pipe(gulp.dest('dist/assets/images/'));
});

gulp.task('fonts', function () {
  //console.log($.mainBowerFiles());
  return gulp.src(['src/assets/fonts/*.*','bower_components/bootstrap-sass-official/assets/fonts/bootstrap/*.*'])
    //.pipe($.filter('**/*.{eot,svg,ttf,otf,woff,woff2}'))
    .pipe($.flatten())
    .pipe(gulp.dest('dist/assets/fonts/'));
});

gulp.task('misc', function () {
  return gulp.src('src/**/*.ico')
    .pipe(gulp.dest('dist/'));
});

gulp.task('clean', function (done) {
  $.del(['dist/', '.tmp/'], done);
});

gulp.task('build', ['html', 'images', 'fonts', 'misc']);
