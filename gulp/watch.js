'use strict';

var gulp = require('gulp');

gulp.task('watch', ['wiredep', 'injector:js', 'injector:css'] ,function () {
  gulp.watch('src/**/*.scss', ['injector:css']);
  gulp.watch('src/**/*.js', ['injector:js']);
  gulp.watch('src/**/images/*', ['images']);
  gulp.watch('bower.json', ['wiredep']);
});
