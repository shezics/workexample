/**
 * Created by Shahzad on 5/04/2015.
 */

//read http://webapplog.com/express-js-4-node-js-and-mongodb-rest-api-tutorial/

var express = require('express');
var app = express();

app.set('port', (process.env.PORT || 7002));
app.use(express.static(__dirname + '/dist'));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', function(req, res) {
  res.sendFile( __dirname + '/dist/index.html');
});

app.listen(app.get('port'), function() {
  console.log("workExample Node app is running at localhost:" + app.get('port'));
});
