/**
 * Created by Shahzad on 5/04/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample', [
      'ngAnimate',
      'ngCookies',
      'ngTouch',
      'ngSanitize',
      'ngResource',
      'ngStorage',
      'ui.router',
      'ui.bootstrap',
      'ui.select',
      'ngFileUpload'
    ]);

})();
