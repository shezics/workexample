/**
 * Created by Shahzad on 8/10/2015.
 */

(function () {

  'use strict';

  angular.module('workExample')
    .controller('BaseModalCtrl', BaseModalCtrl );

  BaseModalCtrl.$inject = ['$scope', '$controller', '$rootScope'];

  function BaseModalCtrl( $scope, $controller, $rootScope ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    /*listeners*/
    $scope.$on('modal.closing', onClosing);

    /*private variables*/
    var formObj = {};

    /*VM functions*/
    $scope.$listenFormStatus = $listenFormStatus;

    //close event listener for child $scope modals.
    function onClosing( evt, closingType, backdrop ) {
      //console.log('closing modal', evt);

      //invokes onClosing callback if defined.
      $scope.beforeClosing && $scope.beforeClosing();

      if ( formObj.status && formObj.status.sending ) {
        evt.preventDefault();
      }
    }

    //store reference of formObj from different child modals e.g. loginFormObj, forgotFormObj, e.t.c.
    function $listenFormStatus( currentFormObj ) {
      formObj = currentFormObj;
    }
  }

})();
