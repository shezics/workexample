/**
 * Created by Shahzad on 5/04/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .config( configFunc );

  configFunc.$inject = ['$httpProvider'];

  function configFunc( $httpProvider ) {
    //Enable cross domain calls
    //$httpProvider.defaults.useXDomain = true;

    //set withCredentials header for every http request.
    $httpProvider.defaults.withCredentials = true;

    //Removes the header used to identify ajax call that would prevent CORS from working, fixed in and after Angular 1.1
    //delete $httpProvider.defaults.headers.common['X-Requested-With'];

    //Sets Content-Type for all POST requests.
    $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
  }

})();
