/**
 * Created by Shahzad on 5/04/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .constant('appConstant', {
      baseURL : 'http://api.workExample.com/'
    });

})();
