/**
 * Created by Shahzad on 5/28/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .value('appFlags', {
      mapScriptsLoaded : false
    });

})();
