/**
 * Created by Shahzad on 5/25/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .run( viewportChecker );

  viewportChecker.$inject = [];

  function viewportChecker() {

    //animates views/posts on scroll

    $(window).scroll(function(){
      $('.animate').bind('inview', function(event, isInView) {
        if (isInView) {
          var animate = $(this).attr('data-animation');
          var speedDuration = $(this).attr('data-duration');
          var $t = $(this);
          setTimeout(function(){
            $t.addClass(animate + ' animated');
          }, speedDuration);
        }
      });
    });

  }

})();
