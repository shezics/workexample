/**
 * Created by Shahzad on 5/14/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .run( routeScrollTop );

  routeScrollTop.$inject = ['$rootScope', '$state'];

  function routeScrollTop( $rootScope, $state ) {

    //scroll on html for firefox and on body for other browsers e.g google chrome.
    var $body = angular.element('html,body');

    /*listeners*/
    $rootScope.$on('$stateChangeSuccess', onStateChangeSuccess);

    /*functions declarations*/

    //listens for any state changes, once state get changed and all content of new state
    //get loaded, it takes the web page scroll bar to top.
    function onStateChangeSuccess( event, toState, toParams, fromState, fromParams ) {
      //console.log('state changed: ' + fromState.name + ' > ' + toState.name );

      /*TODO create avoid child states functionality if more than one parent states needs to avoid scroll.*/
      //return if its manage routes.
      if ( $state.includes('user.manage') ) {
        return;
      }

      //animates and take scroll to top whenever any state/route changes.
      $body
        .delay(230)
        .animate({ scrollTop: "0px" });

    }
  }

})();
