/**
 * Created by Shahzad on 5/04/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .run( runFunc );

  runFunc.$inject = ['$rootScope', '$state', '$timeout','$localStorage', 'authFactory', 'supportFactory', 'productsFactory', 'manageOfficesFactory'];

  function runFunc( $rootScope, $state, $timeout, $localStorage, authFactory, supportFactory, productsFactory, manageOfficesFactory ) {

    //listeners
    $rootScope.$on("$stateChangeStart", stateChangeStart );

    var user, isAuth;

    //checking user logging status, on refresh
    user = authFactory.isAuthenticated();

    //broadcasting auth status.
    $timeout(function () {
      $rootScope.$broadcast('auth', user );
    });

    function stateChangeStart( event, toState, toParams, fromState, fromParams ) {

       //console.log("changing to: " + toState.name);
      isAuth = authFactory.isAuthenticated();

      switch( toState.name ) {

        // validates product details routes
        case 'public.supportCategory':
          supportFactory.validateSupportCategory( event, toState, toParams, fromState, fromParams );
          return;

        // validates product details routes
        case 'public.productDetails':
          productsFactory.validateProduct( event, toState, toParams, fromState, fromParams );
          return;

        // prevents user from sign-up page, if user is logged in.
        case 'public.signUp':
          isAuth && transition('user.home');
          return;

        // prevents none OM users from offices
        case 'user.manage.admin.offices':
          if ( isAuth && !manageOfficesFactory.isOfficeAllowed() ) {
            transition('user.home');
            return;
          }
          break;

        // for any of remaining states
        default:
          if ( !isAuth && toState.name.split('.')[0] === 'user' ) {
            $rootScope.$emit('attemptToUserPage', toState.name);
            $localStorage.redirectTo = toState.name;
            transition('public.home');
            return;
          }
      }

      // prevents the current state and transition to given state
      function transition( stateName ) {
        event.preventDefault();
        $state.transitionTo( stateName );
      }
    }
  }

})();
