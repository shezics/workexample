/**
 * Created by Shahzad on 5/04/2015.
 */

(function () {

    'use strict';

  angular
    .module('workExample')
    .constant('FIELD_TYPES', {
      user : [{
          value : 1,
          title : 'Real Estate Agent'
        }, {
          value : 2,
          title : 'Title Rep'
        }, {
          value : 3,
          title : 'Sales Manager'
        }, {
          value : 4,
          title : 'Office Manager'
        }, {
          value : 5,
          title : 'Corporate Account'
        }, {
        //  value : 6,
        //  title : 'TBD'
        //}, {
          value : 7,
          title : 'Benutech Support'
        }, {
          value : 8,
          title : 'Benutech Management'
        }],
      office: [{
        value : 0,
        title : 'Select Office Type'
      },{
        value : 1,
        title : 'Standard'
      }, {
        value : 2,
        title : 'Broker'
      }, {
        value : 3,
        title : 'Branch'
      }, {
        value : 4,
        title : 'Escrow'
      }, {
        value : 4,
        title : 'Consumer'
      }, {
        value : 4,
        title : 'Lender'
      }],
      employee : [{
        value : 0,
        title : 'Select Employee Type'
      },{
        value : 1,
        title : 'County Manager'
      }, {
        value : 2,
        title : 'Sales Manager'
      }, {
        value : 3,
        title : 'CSR'
      }, {
        value : 4,
        title : 'Title Rep'
      }],
      reports : [{
        value : 'avm',
        title : 'AVM'
      }, {
        value : 'property_profile',
        title : 'Property Profile - SB133'
      }, {
        value : 'single_page_report',
        title : 'Single Page Profile'
      }, {
        value : 'tax_bill',
        title : 'Current Tax Bill'
      }],
      deliveryFormat: [{
        value : 'link',
        title : 'PDF Document'
      }, {
        value : 'HTML',
        title : 'HTML Page'
      }, {
        value : 'CSV',
        title : 'CSV File'
      }, {
        value : 'LABELS',
        title : 'Labels'
      }],
      status: [{
        value : 0,
        title : 'Select Status Type'
      },{
        value : '1',
        title : 'Active'
      }, {
        value : '2',
        title : 'Disabled'
      }, {
        value : '3',
        title : 'Pending'
      }],
      farmOptions:[{
        value : 0,
        title : 'Select option'
        },{
          value: '1',
          title: 'option 1'
        },{
          value: '2',
          title: 'option 2'
        },{
          value: '3',
          title: 'option 3'
        }
      ]
    });

})();
