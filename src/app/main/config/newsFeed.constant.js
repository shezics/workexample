/**
 * Created by Owais raza on 8/6/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .constant('NEWS_FEED', [
      {
        title: 'OUR BRAND NEW WEBSITE LAUNCHED THIS EVENING',
        date: 'May 22, 2015',
        link: 'read more',
        summary: 'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,' +
        'nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris',
        image: '/assets/images/latest-news-thumb-1.jpg'
      },
      {
        title: 'YOU CAN SEE ABOVE A REALLY NICE BUILDING',
        date: 'May 22, 2015',
        link: 'read more',
        summary: 'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,' +
        'nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris',
        image: '/assets/images/latest-news-thumb-2.jpg'
      },
      {
        title: 'THE APARTMENT IS FOR SALE...',
        date: 'May 22, 2015',
        link: 'read more',
        summary: 'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,' +
        'nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris',
        image: '/assets/images/latest-news-thumb-3.jpg'
      },
      {
        title: 'OUR BRAND NEW WEBSITE LAUNCHED THIS EVENING',
        date: 'May 22, 2015',
        link: 'read more',
        summary: 'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,' +
        'nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris',
        image: '/assets/images/latest-news-thumb-1.jpg'
      },
      {
        title: 'YOU CAN SEE ABOVE A REALLY NICE BUILDING',
        date: 'May 22, 2015',
        link: 'read more',
        summary: 'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,' +
        'nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris',
        image: '/assets/images/latest-news-thumb-2.jpg'
      },
      {
        title: 'THE APARTMENT IS FOR SALE...',
        date: 'May 22, 2015',
        link: 'read more',
        summary: 'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,' +
        'nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris',
        image: '/assets/images/latest-news-thumb-3.jpg'
      },
      {
        title: 'OUR BRAND NEW WEBSITE LAUNCHED THIS EVENING',
        date: 'May 22, 2015',
        link: 'read more',
        summary: 'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,' +
        'nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris',
        image: '/assets/images/latest-news-thumb-1.jpg'
      },
      {
        title: 'YOU CAN SEE ABOVE A REALLY NICE BUILDING',
        date: 'May 22, 2015',
        link: 'read more',
        summary: 'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,' +
        'nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris',
        image: '/assets/images/latest-news-thumb-2.jpg'
      }
    ]);

})();
