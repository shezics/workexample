/**
 * Created by Owais raza on 12/16/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .constant('OUR_TEAM', [
      {
        name: 'Ryan Marshall',
        summary: 'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,' +
        'nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris',
        image: '/assets/images/team-member-1.png'
      },
      {
        name: 'Ryan Marshall',
        summary: 'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,' +
        'nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris',
        image: '/assets/images/team-member-2.png'
      },
      {
        name: 'Ryan Marshall',
        summary: 'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,' +
        'nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris',
        image: '/assets/images/team-member-3.png'
      },
      {
        name: 'Ryan Marshall',
        summary: 'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,' +
        'nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris',
        image: '/assets/images/team-member-4.png'
      },
      {
        name: 'Ryan Marshall',
        summary: 'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,' +
        'nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris',
        image: '/assets/images/team-member-5.png'
      },
      {
        name: 'Ryan Marshall',
        summary: 'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,' +
        'nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris',
        image: '/assets/images/team-member-6.png'
      }
    ]);

})();
