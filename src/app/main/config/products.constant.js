/**
 * Created by Shahzad on 5/26/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .constant('PRODUCTS', [
      {
        url: 'lead-tool',
        shortTitle: 'Lead Tool',
        completeTitle: 'Lead Tool with workExample',
        description: 'Awesome tool for realtors and more...',
        summary:
          'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. ' +
          'Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus' +
          ' a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.',
        templateUrl: 'app/pages/productDetails/productDetails.leadTool.view.html',
        imagePath: '../../assets/images/products-cat.jpg',
        screenShots: [
          {
            title: 'Farm View',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Detailed View',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Awesome Map Results',
            imagePath: '/assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: '/assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: '/assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: '/assets/images/sphere-tool.jpg'
          }
        ]
      }, {
        url: 'farming-tool',
        shortTitle: 'Farming Tool',
        completeTitle: 'Farming Tool with workExample',
        description: 'Awesome tool for realtors and more...',
        summary:
        'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. ' +
        'Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus' +
        ' a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.',
        templateUrl: 'app/pages/productDetails/productDetails.farmingTool.view.html',
       imagePath: 'assets/images/products-cat.jpg',
        screenShots: [
          {
            title: 'Farm View',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Detailed View',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Awesome Map Results',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }
        ]
      }, {
        url: 'research-tool',
        shortTitle: 'Research Tool',
        completeTitle: 'Research Tool with workExample',
        description: 'Awesome tool for realtors and more...',
        summary:
          'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. ' +
          'Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus' +
          ' a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.',
        templateUrl: 'app/pages/productDetails/productDetails.comingSoon.view.html',
        imagePath: 'assets/images/products-cat.jpg',
        screenShots: [
          {
            title: 'Farm View',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Detailed View',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Awesome Map Results',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }
        ]
      }, {
        url: 'tract-tool',
        shortTitle: 'Tract Tool',
        completeTitle: 'Tract Tool with workExample',
        description: 'Awesome tool for realtors and more...',
        summary:
          'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. ' +
          'Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus' +
          ' a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.',
        templateUrl: 'app/pages/productDetails/productDetails.comingSoon.view.html',
        imagePath: 'assets/images/products-cat.jpg',
        screenShots: [
          {
            title: 'Farm View',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Detailed View',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Awesome Map Results',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }
        ]
      }, {
        url: 'sphere-tool',
        shortTitle: 'Sphere Tool',
        completeTitle: 'Sphere Tool with workExample',
        description: 'Awesome tool for realtors and more...',
        summary:
          'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. ' +
          'Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus' +
          ' a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.',
        templateUrl: 'app/pages/productDetails/productDetails.sphereTool.view.html',
        imagePath: 'assets/images/products-cat.jpg',
        screenShots: [
          {
            title: 'Farm View',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Detailed View',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Awesome Map Results',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }
        ]
      }, {
        url: 'target-tool',
        shortTitle: 'Target Tool',
        completeTitle: 'Target Tool with workExample',
        description: 'Awesome tool for realtors and more...',
        summary:
          'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. ' +
          'Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus' +
          ' a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.',
        templateUrl: 'app/pages/productDetails/productDetails.comingSoon.view.html',
        imagePath: 'assets/images/products-cat.jpg',
        screenShots: [
          {
            title: 'Farm View',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Detailed View',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Awesome Map Results',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }
        ]
      }, {
        url: 'marketing-tool',
        shortTitle: 'Marketing Tool',
        completeTitle: 'Marketing Tool with workExample',
        description: 'Awesome tool for realtors and more...',
        summary:
          'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. ' +
          'Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus' +
          ' a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.',
        templateUrl: 'app/pages/productDetails/productDetails.comingSoon.view.html',
        imagePath: 'assets/images/products-cat.jpg',
        screenShots: [
          {
            title: 'Farm View',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Detailed View',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Awesome Map Results',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }
        ]
      }, {
        url: 'crm',
        shortTitle: 'CRM',
        completeTitle: 'CRM with workExample',
        description: 'Awesome tool for realtors and more...',
        summary:
          'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. ' +
          'Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus' +
          ' a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.',
        templateUrl: 'app/pages/productDetails/productDetails.comingSoon.view.html',
        imagePath: 'assets/images/products-cat.jpg',
        screenShots: [
          {
            title: 'Farm View',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Detailed View',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Awesome Map Results',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }, {
            title: 'Other Snapshot',
            imagePath: 'assets/images/sphere-tool.jpg'
          }
        ]
      }
    ]);

})();
