/**
 * Created by Shahzad on 5/26/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .constant('SUPPORT_CATEGORIES', [
      {
        url: 'getting-started',
        shortTitle: 'Getting started',
        completeTitle: 'Getting started with workExample',
        summary: 'This is Photoshop\'s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.',
        templateUrl: 'app/pages/supportCategory/supportCategory.gettingStarted.view.html'
      },
      {
        url: 'create-farms',
        shortTitle: 'Create farms',
        completeTitle: 'Create farms with workExample',
        summary: 'This is Photoshop\'s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.',
        templateUrl: 'app/pages/supportCategory/supportCategory.createFarms.view.html'
      },
      {
        url: 'advance-search',
        shortTitle: 'Advance search',
        completeTitle: 'Advance search with workExample',
        summary: 'This is Photoshop\'s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.',
        templateUrl: 'app/pages/supportCategory/supportCategory.advanceSearch.view.html'
      },
      {
        url: 'share-results',
        shortTitle: 'Share results',
        completeTitle: 'Share results with workExample',
        summary: 'This is Photoshop\'s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.',
        templateUrl: 'app/pages/supportCategory/supportCategory.comingSoon.view.html'
      },
      {
        url: 'invite-people',
        shortTitle: 'Invite people',
        completeTitle: 'Invite people with workExample',
        summary: 'This is Photoshop\'s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.',
        templateUrl: 'app/pages/supportCategory/supportCategory.comingSoon.view.html'
      },
      {
        url: 'save-results',
        shortTitle: 'Save results',
        completeTitle: 'Save results with workExample',
        summary: 'This is Photoshop\'s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.',
        templateUrl: 'app/pages/supportCategory/supportCategory.comingSoon.view.html'
      },
      {
        url: 'manage-contacts',
        shortTitle: 'Manage contacts',
        completeTitle: 'Manage contacts with workExample',
        summary: 'This is Photoshop\'s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.',
        templateUrl: 'app/pages/supportCategory/supportCategory.comingSoon.view.html'
      },
      {
        url: 'payment-options',
        shortTitle: 'Payment options',
        completeTitle: 'Payment options with workExample',
        summary: 'This is Photoshop\'s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.',
        templateUrl: 'app/pages/supportCategory/supportCategory.comingSoon.view.html'
      }
    ]);

})();
