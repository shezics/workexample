/**
 *  * Created by Shahzad on 6/01/2015.
 */

//taken from ngAutoFocus: https://github.com/hiebj/ng-autofocus

(function() {
  'use strict';

  angular
    .module('workExample')
    .directive('autofocus', Autofocus);

  Autofocus.$inject = ['$timeout'];

  function Autofocus( $timeout ) {
    return {
      restrict: 'A',
      link: link
    };

    function link($scope, $element, $attrs) {
      var node = $element[0];

      if ($attrs.autofocus) {
        $scope.$watch($attrs.autofocus, focusIf);
      } else {
        focusIf(true);
      }

      function focusIf(condition) {
        if (condition) {
          $timeout(function() {
            node.focus();
          }, $scope.$eval($attrs.autofocusDelay) || 0);
        }
      }
    }
  }
})();
