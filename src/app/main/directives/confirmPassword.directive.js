/**
 * Created by Owais on 12/17/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .directive('confirmPassword', confirmPassword );

  confirmPassword.$inject = [];

  function confirmPassword() {
    return {
      require: 'ngModel',
      //require: ['ngModel', '^form'],
      scope: {
        otherModelValue: '=confirmPassword'
      },

      link: function (scope, elm, attrs, ctrl) {
        ctrl.$validators.confirmPassword = function(modelValue) {
          return modelValue == scope.otherModelValue;
        };

        scope.$watch('otherModelValue', function() {
          ctrl.$validate();
        });
      }
    }
  }
})();
