/**
 *  * Created by Shahzad on 7/08/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .directive('flipBox', flipBox );

  flipBox.$inject = [];

  function flipBox() {
    return {
      restrict : 'A',
      link: function ( $scope, $element, $attrs ) {

        /*initialization*/
        var $flips = $element.find('.flip');
        initFlips();

        /*VM functions*/
        $scope.flip = flip;

        /*function declarations*/
        //on initialization, hides all flips except the first one
        function initFlips(){
          $flips
            .eq(0)
            .siblings('.flip')
            .hide();
        }

        //flips the view to show selected flip, and hides the reset ones.
        function flip( showIndex, hideIndex ) {
          $flips
            .eq( showIndex )
            .slideDown()
            .end()
            .eq( hideIndex )
            .slideUp();
        }
      }
    };
  }
})();
