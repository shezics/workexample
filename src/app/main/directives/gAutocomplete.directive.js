/**
 *  * Created by Shahzad on 6/18/2015.
 */

//taken from https://gist.github.com/kirschbaum/fcac2ff50f707dae75dc

(function () {

  'use strict';

  angular
    .module('workExample')
    .directive('googleAutocomplete', googleAutocomplete );

  googleAutocomplete.$inject = ['$timeout', 'appFlags', 'olFactory'];

  function googleAutocomplete( $timeout, appFlags, olFactory ) {
    return {
      require: 'ngModel',
      scope: {
        ngModel: '=',
        details: '=?',
        mapObject: '=?',
        'onPlaceChanged': '=?'
      },
      link: link
    };

    function link ($scope, $element, $attrs, model) {

      var autocomplete, autocompleteOptions,
        mapDefaults, autoCompleteListener;

      /*listeners*/
      $scope.$on('$destroy', destroyAutocomplete );

      autocompleteOptions = {
        types: ['geocode'],
        componentRestrictions: {}
      };

      // check if OpenLayers and Google Map scripts have been loaded or listen to it's availability.
      if ( appFlags.mapScriptsLoaded ) {
        initialize();
      } else {
        $scope.$on('mapScriptsLoaded', initialize );
      }

      //initialize autocomplete
      function initialize() {

        mapDefaults = olFactory.getMapDefaults();

        mapDefaults.projections = olFactory.getProjections();

        autocomplete = new google.maps.places.Autocomplete($element[0], autocompleteOptions);

        autoCompleteListener = google.maps.event.addListener(autocomplete, 'place_changed', onPlaceChanged);
      }

      //listener for 'place_changed' event of autocomplete
      function onPlaceChanged(){
        var details = $scope.details;

        if ( $scope.mapObject && $scope.mapObject.map ) {
          updateMarkerAndPopup();
        }

        $timeout(function () {

          //to fill the details object of $scope.
          olFactory.fillInAddress(autocomplete.getPlace(), details);
          $scope.details = details;

          //set the view value of autocomplete input model
          model.$setViewValue($element.val());

          //invoke the place_changed listener if defined.
          $scope.onPlaceChanged && $scope.onPlaceChanged();

          //console.log($scope.details);
        });
      }

      //updates marker and its popup on map
      function updateMarkerAndPopup(){
        var map, place, marker, vectorLayer, feature;

        place = autocomplete.getPlace();

        //return if no geometry info found with the address
        if ( !place.geometry ) {
          return;
        }

        //console.log('place changed in map', place.geometry.location.lng(), place.geometry.location.lat());

        map = $scope.mapObject.map;
        vectorLayer = $scope.mapObject.vectorLayer;

        //destroy previously added all popups
        if ( map.popups && map.popups.length ) {
          for (var i = 0; i < map.popups.length; i++) {
            map.removePopup(map.popups[i]);
          }
        }

        //destroy previously added all features/markers
        if ( vectorLayer.features && vectorLayer.features.length ) {
           //map.removeFeatures(map.features);
          vectorLayer.destroyFeatures();
        }

        marker = {
          lonLat: [place.geometry.location.lng(), place.geometry.location.lat()],
          popupInfo: {
            markup: [
              '<div class="info-box">',
              ' <h3>Search property</h3>',
              ' <p>',
              $element.val(),
              ' </p>',
              '</div>'
            ].join(' ')
          }
        };

        //create new feature/marker
        feature = olFactory.createSingleMarker(marker, mapDefaults.markerDefaults, vectorLayer );

        // add a popup for the feature
        $scope.mapObject.control.selector.select(feature);

        //waits for proper projection and set center to new location
        setTimeout(function(){
          var centerLonLat;

          centerLonLat = feature.geometry.getBounds().getCenterLonLat();
          map.setCenter(centerLonLat, $scope.mapObject ? $scope.mapObject.options.zoom : 14);

          //pan popup to get completely visible
          //feature.popup.panIntoView();
        }, 200);
      }

      //to destroy autocomplete
      function destroyAutocomplete() {

        //destroy autocomplete listener
        if ( window.google ) {
          google.maps.event.removeListener(autoCompleteListener);
        }
      }
    }
  }
})();
