/**
 *  * Created by Shahzad on 5/22/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .directive('iframeLoader', iframeLoader );

  iframeLoader.$inject = ['$timeout'];

  function iframeLoader($timeout) {
    return {
      restrict: 'A',
      link: function ($scope, $element) {

        var $iframe;

        $iframe = $element
          .addClass('loading')
          .children('iframe');

        $iframe
          .one('load', function() {
            //HACK same origin policy - can not listen for iframe document has been loaded completely.
            $timeout(function() {
              $element
                .removeClass('loading');
            }, 6000);
          });
      }
    };
  }
})();
