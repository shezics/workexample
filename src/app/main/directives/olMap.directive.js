/**
 *  * Created by Shahzad on 5/28/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .directive('olMap', olMap );

  olMap.$inject = ['appFlags', 'olFactory', '$q', '$timeout'];

  function olMap( appFlags, olFactory, $q, $timeout ) {
    var mapDefaults;

    mapDefaults = olFactory.getMapDefaults();

    return {
      restrict : 'A',
      scope: {
        onResize : '=',
        options: '=',
        mapObject: '=?'
      },
      link: function ( $scope, $element) {
        var refs;

        //references to be used from different functions/sections
        refs = {
          map: undefined,
          mapNode: undefined,
          vectorLayer: undefined,
          controls: undefined,
          options: undefined,
          resetMapHandler: undefined
        };

        //Adds loading class on map initially
        $element.addClass('loading');

        //destroy listener to destroy map
        $scope.$on('$destroy', destroyMap);

        refs.options = angular.extend({}, mapDefaults, $scope.options || {});
        refs.mapNode = $element[0];

        //To make map responsive for height;
        if ( refs.options.calculateHeight ) {
          resizeListener();
        }

        //window resize listener.
        if ( refs.options.listenResize ) {
          $(window).resize( resizeListener );
        }
        //console.log($scope.options);
        //console.log(options);

        $q.all([
          checkMapScripts(),
          getLonLat()
        ]).then(initializeMap);

        // check if OpenLayers and Google Map scripts are downloaded or listen to it's availability.
        function checkMapScripts() {
          var deferred = $q.defer();

          if ( appFlags.mapScriptsLoaded ) {
            deferred.resolve();
          } else {
            $scope.$on('mapScriptsLoaded', deferred.resolve);
          }

          return deferred.promise;
        }

        // gets LonLat to center the map.
        function getLonLat() {
          var deferred = $q.defer();

          if ( $scope.options.lonLat ) {
            deferred.resolve();
          } else if ( window.navigator ) {
            navigator.geolocation.getCurrentPosition(function(position) {
              refs.options.lonLat = [position.coords.longitude, position.coords.latitude];
              deferred.resolve();
            }, function(err) {
              deferred.resolve();
            });
          } else {
            deferred.resolve();
          }

          return deferred.promise;
        }

        //creates a map inside selected node.
        function initializeMap() {

          mapDefaults.projections = olFactory.getProjections();

          refs.map = new OpenLayers.Map( refs.mapNode, {
            controls: getControls(refs.options),

            projection: refs.options.projection,

            zoom: refs.options.zoom,

            center: new OpenLayers.LonLat(refs.options.lonLat)
              .transform( mapDefaults.projections.geographic, mapDefaults.projections.proj3857 ),

            layers:  [
              //new OpenLayers.Layer.Google( "Google Streets", {
              //  numZoomLevels: options.numZoomLevels
              //}) // the default layer
              new OpenLayers.Layer.OSM('Street Map')
            ]
          });

          //create markers/features/vectors layer
          olFactory.createVectorLayer(refs);

          //create controls for map features
          olFactory.createControl(refs);

          //exports ref to reset map handler
          refs.resetMapHandler = resetMapHandler;

          //create any given markers
          if ( refs.options.markers && refs.options.markers.length ) {
            olFactory.createMarkers(refs, true);
          }

          //exports refs to mapObject
          if ( $scope.mapObject ) {
            angular.extend($scope.mapObject, refs);
          }

          //console.log('map initialized');

          //removes loading class from map
          $element.removeClass('loading');
        }

        //to reset a map completely to its initial state
        function resetMapHandler() {
          //set map center to its initial center
          refs.map.setCenter(new OpenLayers.LonLat(refs.options.lonLat)
            .transform( mapDefaults.projections.geographic, mapDefaults.projections.proj3857 ), refs.options.zoom);

          //destroy all markers/features and their popups.
          destroyAllPopups();

          if ( refs.vectorLayer.features && refs.vectorLayer.features.length ) {
            refs.vectorLayer.removeFeatures(refs.vectorLayer.features);
          }
        }

        //returns controls according to given options e.g zoomControls : false
        function getControls() {
          var controls = undefined;

          if ( refs.options.zoomControls === false ) {
            controls = [
              new OpenLayers.Control.Navigation(),
              new OpenLayers.Control.ArgParser(),
              new OpenLayers.Control.Attribution()
            ]
          }

          return controls;
        }

        //destroys map and its components.
        function destroyMap() {
          if ( refs.map ) {
            destroyAllPopups();
            refs.map.destroy();

            //destroy resizeListener
            $(window).off('resize', resizeListener );
          }
        }

        //destroys popups.
        function destroyAllPopups() {
            if ( refs.map.popups && refs.map.popups.length ) {
              for (var i = 0; i < refs.map.popups.length; i++) {
                refs.map.removePopup(refs.map.popups[i]);
            }
          }
        }

        //Calculate window size
        function resizeListener() {
          $element.css({
            height: $(window).height()
          });
        }
      }

    };
  }
})();
