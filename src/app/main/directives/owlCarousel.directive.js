/**
 *  * Created by Shahzad on 5/17/2015.
 */

/* inspired from: http://stackoverflow.com/a/29158076 */

/* FIXES on file owl.carousel.js (2.0.0 beta )
* for issue https://github.com/smashingboxes/OwlCarousel2/issues/485
*
* added to line 72 => this._handlers = {};
*
* replaced line 696
* before:
* this.on(window, 'resize', $.proxy(this.onThrottledResize, this));
* after:
* this._handlers.onThrottledResize = $.proxy(this.onThrottledResize, this);
  this.on(window, 'resize', this._handlers.onThrottledResize );
 }
*
* added before line 1550
* if (this.settings.responsive !== false) {
    window.clearTimeout(this.resizeTimer);
    this.off(window, 'resize', this._handlers.onThrottledResize);
 }
 * */

(function () {

  'use strict';

  angular
    .module('workExample')
    .directive('owlCarousel', owlCarousel )
    .directive('owlCarouselItem', owlCarouselItem );

  owlCarousel.$inject = [];
  owlCarouselItem.$inject = [];


  //TODO replace inside carousel-item directive
  var defaultOptions;
  defaultOptions = {
    navigation: false,
    responsiveClass: true,
    items: 1
  };


  //"owl-carousel" to be used as a parent of "owl-carousel-item".
  function owlCarousel() {

    return {
      restrict: 'A',
      transclude: false,
      scope: {},
      link: function( $scope, $element, attrs ) {
        $element = $($element);

        $scope.$on('$destroy', function() {
          //$element.data('owlCarousel').destroy(); // for version 1.x
          //$element.owlCarousel('destroy');
          $element.trigger("destroy.owl.carousel");  // for version 2.x
        });

        $scope.initCarousel = function() {
          var customOptions = $scope.$eval( attrs['data-owl-carousel-options'] );
          $element.owlCarousel( angular.extend({}, defaultOptions, customOptions || {}) );
        };

        if ( $scope.$eval( attrs.owlCarouselReady) ) {
          $scope.initCarousel();
        }
      }
    }
  }

  //"owl-carousel-item" to be used as child of owl-carousel.
  // useful if dealing with ng-repeat.
  function owlCarouselItem() {
    return function( $scope, $element, attrs ) {
      if ( $scope.$last ) {

        var $parent = $element.parent();

        var customOptions = $scope.$eval( $parent.attr('data-owl-carousel-options') );

        $parent.owlCarousel( angular.extend({}, defaultOptions, customOptions || {}) );
      }
    };
  }

})();
