/**
 *  * Created by Shahzad on 5/04/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .directive('slideToggle', slideToggle );

  slideToggle.$inject = [];

  function slideToggle() {
    return {
      restrict : 'A',
      link: function ( scope, element ) {
        element.on('click', function() {
          element.next('ul').slideToggle();
        });
      }
    };
  }
})();
