/**
 * Created by Farhan on 10/04/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .directive('toggleMobileNav', ToggleMobileNav );

  ToggleMobileNav.$inject = ['$document', '$window'];

  function ToggleMobileNav( $document, $window ) {
    return {
      restrict : 'A',
      link: function ($scope, $element) {
        var $html = $document.find('html');

        $element.on('click', function() {
          if ( $window.innerWidth < 768 ) {
            $html.toggleClass('open-menu');
          };
        });
      }
    };
  }

})();
