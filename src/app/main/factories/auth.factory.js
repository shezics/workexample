/**
 * Created by Shahzad on 5/04/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .factory('authFactory', authFactory );

  authFactory.$inject = [
    '$rootScope',
    '$http',
    '$q',
    '$localStorage',
    'appConstant',
    'cacheFactory',
    'utilFactory'
  ];

  function authFactory( $rootScope, $http, $q, $localStorage, appConstant, cacheFactory, utilFactory ) {
     //var cachedUserID;

    //temporary flag.
    //$localStorage.user = true;

    return {
      login: login ,
      logout: logout,
      isAuthenticated: isAuthenticated,
      generateAndSaveAuthKey: generateAndSaveAuthKey
    };

    //gets login with given credentials
    function login( payload ) {
      var defer, request, self;

      self = this;
      defer = $q.defer();
      request = JSON.stringify( payload );

      $http.post( appConstant.baseURL + 'Webservices/login.json', request )
        .success(function( res ) {
          if ( res.response.status === 'OK' ) {

            self.generateAndSaveAuthKey( res.response.data[0] );
            $rootScope.$broadcast('auth', $localStorage.user );

            //console.log('log-in successful');
            defer.resolve( res );
          } else {
            //console.log('log-in failed');
            //console.log( res );
            defer.reject( res.response.data[0].toLowerCase() );
          }
        })
        .error(function( reason, status ) {
          //console.log('log-in failed');
          //console.log(reason);
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason );
        });

      return defer.promise;
    }

    //gets user log out
    function logout() {
      var defer;

      defer = $q.defer();

      //clears user's local authentication
      cacheFactory.clearCache();
      $rootScope.$broadcast('auth', null );

      $http.post( appConstant.baseURL + 'webservices/logout.json', {} )
        .success(function ( res ) {
          if ( res.response.status === 'OK' ) {
            //console.log('log-out successful');
            defer.resolve( res );
          } else {
            //console.log('log-out failed');
            //console.log( res );
            defer.reject(  res.response.data[0] );
          }
        })
        .error(function ( reason, status ) {
          //console.log('log-out failed');
          //console.log( reason );
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason );
        });

      return defer.promise;
    }

    //to get user authentication status.
    function isAuthenticated() {
      var user;

      //TODO validate security key with user data.
      if ( $localStorage.user ) {
        user = $localStorage.user;
      } else {
        user = null;
      }

      return user;
    }

    //generates a secret key for current logged in user
    function generateAndSaveAuthKey( userDetails ) {
      //change phone from string to number
      if(userDetails){
        angular.forEach(userDetails.TbPhone, function(phoneObj, index) {
          phoneObj.phone = phoneObj.phone ? parseInt(phoneObj.phone) : phoneObj.phone;
        });
      }
      //console.log('user Details', userDetails);
      cacheFactory.setModel('user', userDetails);

      //TODO: generate a security key against logged in user.
      cacheFactory.setModel('authKey', 'test-key');
    }
  }

})();
