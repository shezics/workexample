/**
 * Created by Shahzad on 5/04/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .factory('cacheFactory', cacheFactory );

  cacheFactory.$inject = [
    '$rootScope',
    '$localStorage',
    'utilFactory'
  ];

  function cacheFactory( $rootScope, $localStorage, utilFactory ) {

    return {
      clearCache: clearCache,
      getAllModels: getAllModels,
      getModel: getModel,
      setModel: setModel
    };

    //clears all cached items that may be save by either setModel() or direct accessing $localStorage
    function clearCache() {
      $localStorage.$reset();
      $localStorage.user = null;
      $localStorage.authKey = null;
    }

    //gets all modals saved in local storage, or to get directly local storage
    function getAllModels() {
      return $localStorage;
    }

    //gets a modal saved in local storage
    function getModel( name, clone ) {
      return clone ? angular.copy( $localStorage[name] ) : $localStorage[name];
    }

    //sets a modal to save in local storage
    function setModel( name, data ) {
      $localStorage[name] = data;
    }

  }


})();
