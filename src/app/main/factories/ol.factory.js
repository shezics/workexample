/**
 * Created by Shahzad on 7/01/2015.
 */

(function () {

    'use strict';

  angular
    .module('workExample')
    .factory('olFactory', olFactory );

  olFactory.$inject = ['$timeout', 'utilFactory'];

  function olFactory( $timeout, utilFactory ) {
    var olObject = {};

    return {
      getMapDefaults: getMapDefaults,
      getProjections: getProjections,
      createProjections: createProjections,
      createVectorLayer: createVectorLayer,
      createControl: createControl,
      createMarkers: createMarkers,
      createSingleMarker: createSingleMarker,
      fillInAddress: fillInAddress
    };

    //Create map Defaults function
    function getMapDefaults(){
      return{
        lonLat: [-117.844105, 33.683219],
        zoom: 14,
        numZoomLevels: 20,
        markerDefaults: {
          lonLat: [-117.844105, 33.683219],
          image: 'assets/images/_pin.png',
          height: 51,
          width: 36
        },
        popupDefaults: {
          width: 220,
          height: 135
        }
      };
    }

    //return instantiated projections
    function getProjections() {
      return olObject.projections || createProjections() ;
    }

    //instantiates projections
    function createProjections() {

      olObject.projections = {};

      olObject.projections.geographic = new OpenLayers.Projection("EPSG:4326");
      olObject.projections.mercator = new OpenLayers.Projection("EPSG:900913");
      olObject.projections.proj3857 = new OpenLayers.Projection("EPSG:3857");

      return olObject.projections;
    }

    //create a vector layer on given map and add its reference
    function createVectorLayer( refs ) {
      refs.vectorLayer = new OpenLayers.Layer.Vector('Overlay');

      //adds vector layer on map
      refs.map.addLayer(refs.vectorLayer);
    }

    //create a single feature on vector layer of map
    function createSingleMarker( marker, markerDefaults, vectorLayer ) {
      var feature;

      // Define markers as "features" on the vector layer:
      feature = new OpenLayers.Feature.Vector(
        new OpenLayers.Geometry.Point(marker.lonLat[0], marker.lonLat[1])
          .transform(olObject.projections.geographic, olObject.projections.proj3857),
        marker.popupInfo,
        {
          externalGraphic: marker.image || markerDefaults.image,
          graphicHeight: marker.height || markerDefaults.height,
          graphicWidth: marker.width || markerDefaults.width
        }
      );

      vectorLayer.addFeatures([feature]);

      return feature;
    }

    //create multiple markers/features on the vector layer
    function createMarkers(refs, firstMarkerPopup) {
      var marker, mapDefaults, features;

      mapDefaults = getMapDefaults();
      features = [];

      for (var i = 0; i < refs.options.markers.length; i++) {
        marker = refs.options.markers[i];

        //invoke createSingleMarker to create each marker/feature
        features[i] = createSingleMarker(marker, mapDefaults.markerDefaults, refs.vectorLayer);

      }

      //open a popup for first marker
      if ( firstMarkerPopup ) {
        refs.control.selector.select(features[0]);
      }

      return features;
    }

    //creates a control on map to add a selector control to the vectorLayer with popup functions
    function createControl(refs) {
      var mapDefaults;

      mapDefaults = getMapDefaults();

      refs.control  = {
        selector: new OpenLayers.Control.SelectFeature(refs.vectorLayer, {
          id: 'select-feature',
          onSelect: createPopup,
          onUnselect: destroyPopup,
          clickout: !refs.options.noClickout
        })
      };

      //adds vector controls on map
      refs.map.addControl(refs.control.selector);

      //activates markers to be selected and show popups
      refs.control.selector.activate();

      if(refs.options.drag === true){
        refs.map.addControl(new OpenLayers.Control.DragFeature(refs.vectorLayer, {
            autoActivate: true,
            onComplete: createDragFeature
          })
        );

        refs.map.events.register('click', refs.map, function(e){
          createClickFeature(e.xy);
        });
      }

      //creates a popup against the marker element which were clicked
      function createPopup(feature) {

        feature.popup = new OpenLayers.Popup.FramedCloud("pop",
          feature.geometry.getBounds().getCenterLonLat(),
          null,
          feature.attributes.markup,
          null,
          !refs.options.noClickout,
          refs.options.noClickout ? undefined : function(){
            refs.control.selector.unselect(feature);
          }
        );

        //feature.popup.closeOnMove = true;
        refs.map.addPopup(feature.popup);

        //since the popup is added on map, now resize it to required dimensions.
        feature.popup.setSize(
          new OpenLayers.Size(
            feature.attributes.width || mapDefaults.popupDefaults.width,
            feature.attributes.height || mapDefaults.popupDefaults.height
          )
        );

        //pan popup to get completely visible after size is changed
        feature.popup.panIntoView();
      }

      //destroys the popup which were created previously
      function destroyPopup(feature) {
        if ( feature.popup ) {
          refs.map.removePopup(feature.popup);
          feature.popup.destroy();
          feature.popup = null;
        }
      }

      //Drag marker feature funtion
      function createDragFeature(feature, pixel){
        var lonlat;

        lonlat = refs.map.getLonLatFromPixel(pixel).transform(olObject.projections.proj3857, olObject.projections.geographic);

        destroyPopup(feature);
        getGeocode(feature, lonlat);
      }

      //Click map feature function
      function createClickFeature(pixel){
        var lonlat, marker, feature;

        if (refs.vectorLayer.features[0]) {

          refs.control.selector.unselect(refs.vectorLayer.features[0]);
          refs.vectorLayer.destroyFeatures();
        }

        lonlat = refs.map.getLonLatFromPixel(pixel).transform(olObject.projections.proj3857, olObject.projections.geographic);

        marker = {
          lonLat: [lonlat.lon, lonlat.lat],
          popupInfo: {}
        };

        feature = createSingleMarker(marker, mapDefaults.markerDefaults, refs.vectorLayer);
        getGeocode(feature, lonlat);
      }

      // get Location when dragging marker or click to map
      function getGeocode(feature, lonlat){
        //Geocode request get Address in data.
        var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" +lonlat.lat+','+lonlat.lon+ "&sensor=false";
        $.get(url, function (data) {
          if( data.results.length > 0){
            feature.attributes.markup = '<div class="info-box"> ' +
              ' <h3>Search property</h3>' +
              ' <p> ' +
              data.results[0].formatted_address +
              ' </p>' +
              ' </div>';

            $timeout(function(){
              fillInAddress(data.results[0], refs.options.details);
            });
          } else{
            feature.attributes.markup = '<div class="info-box"> ' +
              ' <h3>Search property</h3>' +
              ' <p> Sorry address not found! </p>' +
              ' </div>';
          }
          createPopup(feature);

          //set feature/marker & popup in map center
          refs.map.setCenter(new OpenLayers.LonLat(lonlat.lon, lonlat.lat)
            .transform( olObject.projections.geographic, olObject.projections.proj3857 ));
        })
      }
    }

    //to fill address in lookup Address form
    function fillInAddress( place, details ) {
      var addressComp;

      details = details || {};

      //empty any previous values
      utilFactory.deleteAllPropsFromObj(details);

      // Get each component of the address from the place details
      // and create $scope.details

      for (var i = 0, len = place.address_components.length; i < len; i++) {
        addressComp = place.address_components[i];
        switch( addressComp.types[0] ) {
          case 'street_number':
            details.site_street_number = addressComp.short_name;
            break;
          case 'route':
            details.site_route = addressComp.short_name;
            break;
          case 'locality':
            details.site_city = addressComp.long_name;
            break;
          case 'administrative_area_level_1':
            details.site_state = addressComp.short_name;
            break;
          case 'administrative_area_level_2':
            details.county = addressComp.short_name;
            break;
          case 'country':
            details.country = addressComp.long_name;
            break;
          case 'postal_code':
            details.site_zip = addressComp.short_name;
            break;
        }
      }
    }
  }
})();
