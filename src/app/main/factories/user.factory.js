/**
 * Created by Shahzad on 5/04/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .factory('userFactory', userFactory );

  userFactory.$inject = [
    '$rootScope',
    '$http',
    '$q',
    '$modal',
    'appConstant',
    'utilFactory',
    'authFactory',
    'pipelineFactory'
  ];

  function userFactory( $rootScope, $http, $q, $modal, appConstant, utilFactory, authFactory, pipelineFactory ) {

    return {
      addUpdateUser: addUpdateUser,
      assignUserToRep: assignUserToRep,
      fetchRepsDropDown: fetchRepsDropDown,
      fetchRepsPipeline: fetchRepsPipeline,
      parseRepModel: parseRepModel,
      changePassword: changePassword,
      recoverPassword: recoverPassword,
      contactRequest: contactRequest,
      getEmptyUserModel: getEmptyUserModel,
      openLoginModel: openLoginModel,
      openForgotPassModel: openForgotPassModel
    };

    //adds or updates user profile
    function addUpdateUser( options ) {
      var defer, request, alias, endpoint;

      defer = $q.defer();

      if ( options.editUser ) {

        // creating alias to handle if no objects available, for each type.
        alias = {
          user: options.payload.TbUser || {},
          address:  options.payload.TbAddress && options.payload.TbAddress[0] || {},
          email: options.payload.TbEmail && options.payload.TbEmail[0] || {},
          phone: options.payload.TbPhone
        };

        //slicing extra params.
        options.payload = {
          TbUser : {
            users_id: alias.user.users_id,
            type: alias.user.type,
            status: alias.user.status,
            company_name: alias.user.company_name,
            first_name: alias.user.first_name,
            last_name: alias.user.last_name,
            office_id: alias.user.office_id,
            employee_id: alias.user.employee_id
          },
          TbAddress:[
            {
              address_id: alias.address.address_id,
              type: alias.address.type,
              address: alias.address.address,
              address_2: alias.address.address_2,
              city: alias.address.city,
              state: alias.address.state,
              zip: alias.address.zip
            }
          ],
          TbEmail: [
            {
              email_id: alias.email.email_id,
              type: alias.email.type,
              email: alias.email.email
            }
          ],
          TbPhone: alias.phone
        };

        endpoint = appConstant.baseURL + 'webservices/edit_user.json';
      } else {

        if ( options.payload.TbAssociation.parent_user_id === 'none' ) {
          delete options.payload.TbAssociation;
        }

        if ( options.payload.TbOffice.office_id === 'none' ) {
          delete options.payload.TbOffice;
        }

        options.payload.TbEmail = [{
          email: options.payload.TbUser.username
        }];

        endpoint = appConstant.baseURL + 'webservices/add_user.json';
      }

      request = JSON.stringify( options.payload );

      $http.post( endpoint, request )
        .success(function ( res ) {
          if ( res.response.status === 'OK' ) {
            //console.log('profile addUpdate successful.');
            //authFactory.generateAndSaveAuthKey( res.response.data.object );
            defer.resolve( res.response );
          } else {
            //console.log('addUpdate profile failed');
            //console.log( res );
            defer.reject( res.response.data[0] );
          }
        })
        .error(function ( reason, status ) {
          //console.log('addUpdate profile failed');
          //console.log( reason );
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason );
        });

      return defer.promise;
    }

    // to assign any user to rep
    function assignUserToRep( payload ) {
      var defer, request;

      defer = $q.defer();
      request = JSON.stringify( payload );

      $http.post( appConstant.baseURL + 'webservices/assign_user_to_rep.json', request)
        .success(function ( res ) {
          res = res.response;

          if ( res.status === 'OK' ) {
            defer.resolve({msg: 'User has been assigned to selected Rep successfully.'});
          } else if ( res.data.stop == 1 ) {
            res.data.msg += '. Please confirm that you want to re assign to another Rep.';
            defer.reject(res.data);
          } else {
            defer.reject({ msg: 'error occurred.'});
          }
        })
        .error(function ( reason, status ) {
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject({ msg: reason});
        });

      return defer.promise;
    }

    // to get all Reps via get_reps.json
    function getReps() {
      var defer;

      defer = $q.defer();
      $http.get( appConstant.baseURL + 'webservices/get_reps.json')
        .success(function ( res ) {
          defer.resolve( res );
        })
        .error(function ( reason, status ) {
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason );
        });

      return defer.promise;
    }

    //extends getReps() to get or fetch all reps for dropdown view
    function fetchRepsDropDown(){

      return getReps()
        .then(function ( res ) {

          //Adds "leave blank" item even when no reps are available
          res.unshift({
            customFullName: 'leave blank to be assigned one',
            TbUser: { users_id: 'none', type: 'none'}
          });

          for (var i = 1; i < res.length; i++) {
            res[i].customFullName =  res[i].TbUser.first_name + ' ' +  res[i].TbUser.last_name ;
          }

          //pass modified response to next then in promise chain
          return res;
        });
    }

    //extends getReps() to fetch all reps and parse for pipeline view
    function fetchRepsPipeline(options) {
      var skipRecordIndex;

      return getReps(options)
        .then(function ( res ) {

          if ( options.skipRecord ) {
            angular.forEach(res, checkSkipThenParse);
            res.splice(skipRecordIndex, 1);
          } else {
            angular.forEach(res, parseRepModel);
          }

          //pass modified response to next then in promise chain
          return {
            object: res,
            pages: 1 //TODO should be provided by API
          };
        });

      // store the index of the record that need to be eliminated from records.
      function checkSkipThenParse( record, index ) {
        if ( record.TbUser.users_id === options.skipRecord ) {
          skipRecordIndex = index;
          return ;
        }

        parseRepModel( record, index );
      }
    }

    //parse each user model and insert custom properties
    function parseRepModel( model, index ) {

      //for serial number
      model.serial_number = index;

      //for full name
      model.customFullName = model.TbUser.first_name ? model.TbUser.first_name + ' ' +  model.TbUser.last_name : '-';

      //for Name column
      model.customName = setName(model.TbUser);

      //for Address column
      model.customAddress = pipelineFactory.setAddress(model.TbAddress);

      //for Phone column
      model.customPhone = pipelineFactory.setPhone(model.TbPhone);

      //for Email column
      model.customEmail = pipelineFactory.setEmail(model.TbEmail);

      //for rep-info column
      //model.customRepInfo = setRepInfo(model);

      //returns the formatted model
      return model;

      //formats the value for name column
      function setName( nameModel ) {
        return ( model.customFullName ) +
          pipelineFactory.breakValue(nameModel.company_name ? 'Company Name: ' + nameModel.company_name : '') +
          pipelineFactory.breakValue(nameModel.type ? 'Type: ' + utilFactory.translateFieldType(nameModel.type, 'user') : '') +
          pipelineFactory.breakValue(nameModel.status ? 'Status: ' + utilFactory.translateFieldType(nameModel.status, 'status') : '' );
      }

      //formats the value for rep-info column
      //function setRepInfo( model ) {
      //  return ( model.TbUser.username || '' ) +
      //    pipelineFactory.breakValue('Last order: ' +
      //      ( model.TbUser.last_order && model.TbUser.last_order !== 'none' ?
      //        pipelineFactory.formatDate(model.TbUser.last_order) : 'none')
      //    ) +
      //    pipelineFactory.breakValue('Total orders: ' + ( model.TbUser.total_orders || 0 ) )
      //}
    }

    //changes password for current logged in user
    function changePassword( payload ) {
      var defer, request;

      defer = $q.defer();
      request = JSON.stringify( payload );

      $http.post( appConstant.baseURL + 'webservices/change_password.json', request )
        .success(function ( res ) {
          if ( res.response.status === 'OK' ) {
            //console.log('password updated successfully.');
            defer.resolve( res.response );
          } else {
            //console.log('update password failed');
            //console.log( res );
            defer.reject(  res.response.data[0] || 'error occurred.' );
          }
        })
        .error(function ( reason, status ) {
          //console.log('update password failed');
          //console.log( reason );
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason );
        });

      return defer.promise;
    }

    //recover password for an account
    function recoverPassword( payload ) {
      var defer, request;

      defer = $q.defer();
      request = JSON.stringify( payload );

      $http.post( appConstant.baseURL + 'Webservices/reset_password.json', request )
        .success(function ( res ) {
          if ( res.response.status === 'OK' ) {
            //console.log('password reset successful.');
            defer.resolve( res.response.data.toLowerCase() );
          } else {
            //console.log('reset password failed');
            //console.log( res );
            defer.reject( res.response.data[0].toLowerCase() );
          }
        })
        .error(function ( reason, status ) {
          //console.log('reset password failed');
          //console.log( reason );
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason );
        });

      return defer.promise;
    }

    //contact request API for contact us page
    function contactRequest( payload ) {
      var defer, request;

      defer = $q.defer();
      request = JSON.stringify( payload );

      $http.post( appConstant.baseURL + 'webservices/contact_form.json', request )
        .success(function ( res ) {
          if ( res.response.status === 'OK' ) {
            //console.log('contact request successful.');
            defer.resolve( res.response.data.message.toLowerCase() );
          } else {
            //console.log('contact request failed');
            //console.log( res );
            defer.reject( res.response.data[0].toLowerCase() );
          }
        })
        .error(function ( reason, status ) {
          //console.log('contact request failed');
          //console.log( reason );
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason );
        });

      return defer.promise;
    }

    // gets an empty user model
    function getEmptyUserModel() {
      return {
        TbAssociation:{
          parent_user_id: '0'
        },
        TbUser: {
          username: '',
          password: '',
          type: 1,
          company_name: '',
          first_name: '',
          last_name: '',
          users_id: '',
          parent_user_id: '0',
          office_id: '0'
        },
        TbAddress: [{
          address: '',
          address_2: '',
          city: '',
          state: '',
          zip: ''
        }],
        TbPhone: [{
          phone: ''
        }]
      };
    }

    //opens up a login model
    function openLoginModel() {
      return $modal.open({
        controller: 'LoginCtrl',
        templateUrl: 'app/shared/login/login.view.html',
        size: 'sm',
        windowClass: 'login'
      });
    }

    //opens up a forgot password model
    function openForgotPassModel() {
      return $modal.open({
        controller: 'ForgotCtrl',
        templateUrl: 'app/shared/forgot/forgot.view.html',
        size: 'sm',
        windowClass: 'forgot-pass'
      });
    }
  }

})();
