/**
 * Created by Shahzad on 5/04/2015.
 */

(function () {

    'use strict';

  angular
    .module('workExample')
    .factory('utilFactory', utilFactory );

  utilFactory.$inject = ['FIELD_TYPES'];

  function utilFactory(FIELD_TYPES) {

    return {
      plainObjToArray: plainObjToArray,
      sliceObjectProps: sliceObjectProps,
      deleteAllPropsFromObj: deleteAllPropsFromObj,
      findWhereArrayObj: findWhereArrayObj,
      translateFieldType: translateFieldType,
      requestErrorHandler: requestErrorHandler
    };

    //creates an array from an object collection based on key, value pair
    function plainObjToArray(obj){
      var array;

      array = [];
      angular.forEach(obj, function( value, key ) {
        array.push({
          key: key,
          value: value
        });
      });

      return array;
    }

    //deletes selected properties form an object and returns new sliced object
    function sliceObjectProps( obj, propArray, cloneObj ) {
      var prop;

      obj = cloneObj ? angular.copy(obj) : obj;

      for ( var i = 0, len = propArray.length; i < len; i++ ) {
        prop = propArray[i];

        //deletes selected property
        delete obj[ prop ];
      }

      return obj;
    }

    //delete all properties inside an object. useful to empty an object without breaking the reference
    function deleteAllPropsFromObj( obj ) {
      for ( var prop in obj ) {
        if ( obj.hasOwnProperty(prop) ) {
          delete obj[ prop ];
        }
      }
    }

    // to look for an object by property, in an array
    function findWhereArrayObj( propertyName, propertyValue, array ) {
      var obj, match;

      for ( var i = 0, len = array.length; i < len; i++ ) {
        obj = array[i];

        if( obj[ propertyName ] == propertyValue ) {
          match = obj;
          break;
        }

      }

      return match || null;
    }

    //translates field types.
    function translateFieldType( typeNum, fieldName ) {
      var typeObj;

      typeObj = this.findWhereArrayObj('value', typeNum, FIELD_TYPES[fieldName] );

      return typeObj ? typeObj.title : '-';
    }

    // general request error handler
    function requestErrorHandler( res, status ) {
      var reason;

      if( typeof res === 'string' ) {
        return res.toLowerCase();
      }

      if( typeof res === 'object' && res !== null ) {
        return res.message.toLowerCase();
      }

      switch( status ) {
        case 404:
          reason = 'Could not found Server destination.';
          break;
        case 0:
          reason = 'Some error occurred in contacting Server.';
          break;
        default:
          reason = 'Some error occurred in contacting Server.';
      }

      return reason;
    }
  }

})();
