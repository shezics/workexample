/**
 *  * Created by Shahzad on 5/04/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .filter('filterBy', filterBy );

  filterBy.$inject = [];

  function filterBy() {

    return function( origItems, propName, searchString, exportsObj ) {
      var filteredItems, filterMethod, item;

      if ( !searchString ) {
        filteredItems = origItems;
        updateExport();
        return filteredItems;
      }

      filterMethod = propName === '$' ? matchesAnyPropValue : matchesGivenPropValue;
      filteredItems = [];
      for ( var i = 0; i < origItems.length; i++ ) {
        item = origItems[i];

        if ( filterMethod(item, propName) ) {
          filteredItems.push(item);
        }
      }

      updateExport();

      return filteredItems;

      //updates export object for filtered items length count
      function updateExport() {
        if ( exportsObj ) {
          exportsObj.searchItemsLength = filteredItems.length;
        }
      }

      //filters item that contains search string in any property.
      function matchesAnyPropValue( item ) {
        var matches;

        for ( var prop in item ) {
          if( item.hasOwnProperty(prop) && matchesGivenPropValue(item, prop) ) {
            matches = true;
            break;
          }
        }

        return matches;
      }

      //filters item that contains search string in given property.
      function matchesGivenPropValue( item, prop ) {
        return typeof item[prop] === 'string' && item[prop].toLowerCase().indexOf(searchString) >= 0;
      }
    };
  }
})();
