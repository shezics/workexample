/**
 *  * Created by Shahzad on 5/04/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .filter('pagination', PaginationFilter );

  PaginationFilter.$inject = [];

  function PaginationFilter() {
    var begin, end,
      perPage;

    perPage = 10; //should be dynamic when per-page drop-down is available.

    return function( items, currentPageIndex ) {

      if ( items.length <= perPage ) {
        return items;
      }

      begin = ( currentPageIndex - 1 ) * perPage;
      end = begin + perPage;

      return items.slice( begin, end );
    };
  }
})();
