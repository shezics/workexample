/**
 * Created by Shahzad on 5/04/2015.
 */

(function () {

  'use strict';

  angular.module('workExample')
    .controller('MainCtrl', MainCtrl );

  MainCtrl.$inject = ['$scope', '$rootScope', '$state', 'authFactory'];

  function MainCtrl( $scope, $rootScope, $state, authFactory ) {

    //listeners
    //$rootScope.$on('auth', authChanged );

    /*VM props*/
    $scope.$state = $state;

    //private variables
    //...

    //initialize stuff
    //$scope.isAuth = !!authFactory.isAuthenticated();

    //sets any layout adjustment as user changed its logging state.
    //function authChanged( evt, user ) {
    //  $scope.isAuth = !!user;
    //}
  }

})();

