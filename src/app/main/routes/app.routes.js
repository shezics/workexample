/**
 * Created by Shahzad on 5/04/2015.
 */

(function() {

  'use strict';

  angular
    .module('workExample')
    .config( RoutesConfig );

  RoutesConfig.$inject = [
    '$urlRouterProvider',
    '$locationProvider'
  ];

  function RoutesConfig( $urlRouterProvider, $locationProvider ) {

    var $state, isAuth;

    //to handle 404 type routes.
    //$urlRouterProvider.otherwise('/');
    $urlRouterProvider.otherwise(function ($injector, $location) {
     $state = $injector.get('$state');
     isAuth = $injector.get('authFactory').isAuthenticated();

      $state.transitionTo( isAuth ? 'user.home' : 'public.home');
    });

    //to support SPA indexing for SEO crawlers.
    $locationProvider.hashPrefix('!');

  }

})();
