/**
 * Created by Shahzad on 5/04/2015.
 */

(function() {

  'use strict';

  angular
    .module('workExample')
    .config( RoutesConfig );

  RoutesConfig.$inject = [
    '$stateProvider'
  ];

  function RoutesConfig( $stateProvider ) {
    var templatesBaseUrl = 'app/pages/';

    $stateProvider
      .state('public', {
        abstract: true,
        templateUrl: 'app/shared/abstract/abstract.public.view.html'
      })
      .state('public.home', {
        url: '/',
        templateUrl: templatesBaseUrl + 'home/home.public.view.html',
        controller: 'HomePublicCtrl'
      })
      .state('public.signUp', {
        url: '/sign-up',
        templateUrl: templatesBaseUrl + 'signUp/signUp.view.html',
        controller: 'SignUpCtrl'
      })
      .state('public.aboutUs', {
        url: '/about-us',
        templateUrl: templatesBaseUrl + 'aboutUs/aboutUs.view.html',
        controller: 'AboutUsCtrl'
      })
      .state('public.products', {
        url: '/products',
        templateUrl: templatesBaseUrl + 'products/products.view.html',
        controller: 'ProductsCtrl'
      })
      .state('public.productDetails', {
        url: '/products/:productId',
        templateUrl: templatesBaseUrl + 'productDetails/productDetails.view.html',
        controller: 'ProductDetailsCtrl'
      })
      .state('public.support', {
        url: '/support',
        templateUrl: templatesBaseUrl + 'support/support.view.html',
        controller: 'SupportCtrl'
      })
      .state('public.supportCategory', {
        url: '/support/:categoryId',
        templateUrl: templatesBaseUrl + 'supportCategory/supportCategory.view.html',
        controller: 'SupportCategoryCtrl'
      })
      .state('public.contact', {
        url: '/contact',
        templateUrl: templatesBaseUrl + 'contact/contact.view.html',
        controller: 'ContactCtrl'
      });

  }

})();
