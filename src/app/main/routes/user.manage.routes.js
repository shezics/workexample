/**
 * Created by Shahzad on 5/04/2015.
 */

(function() {

  'use strict';

  angular
    .module('workExample')
    .config( RoutesConfig );

  RoutesConfig.$inject = [
    '$stateProvider'
  ];

  function RoutesConfig( $stateProvider ) {
    var templatesBaseUrl = 'app/pages/userManage';

    /*manage abstract state*/
    $stateProvider
      .state('user.manage', {
        abstract: true,
        url: '/manage',
        templateUrl: templatesBaseUrl + '/manage.view.html',
        controller: 'ManageCtrl'
      })

      /*manage reports states*/
      .state('user.manage.reports', {
        abstract: true,
        url: '/reports',
        templateUrl: templatesBaseUrl + '/manage.reports.view.html',
        controller: 'ManageReportsCtrl'
      })
      .state('user.manage.reports.orderReport', {
        url: '/order-report',
        templateUrl: templatesBaseUrl + '/manage.reports.orderReport.view.html',
        controller: 'ManageReportsOrderReportCtrl'
      })
      .state('user.manage.reports.uploadRecords', {
        url: '/upload-record',
        templateUrl: templatesBaseUrl + '/manage.reports.uploadRecords.view.html',
        controller: 'ManageReportsUploadRecordsCtrl'
      })
      .state('user.manage.reports.orderHistory', {
        url: '/order-history',
        templateUrl: templatesBaseUrl + '/manage.reports.orderHistory.view.html',
        controller: 'ManageReportsOrderHistoryCtrl'
      })

      /*manage users state*/
      .state('user.manage.admin', {
        abstract: true,
        url: '/admin',
        templateUrl: templatesBaseUrl + '/manage.admin.view.html',
        controller: 'ManageAdminCtrl'
      })
      .state('user.manage.admin.users', {
        url: '/users',
        templateUrl: templatesBaseUrl + '/manage.admin.users.view.html',
        controller: 'ManageAdminUsersCtrl'
      })
      .state('user.manage.admin.offices', {
        url: '/offices',
        templateUrl: templatesBaseUrl + '/manage.admin.offices.view.html',
        controller: 'ManageAdminOfficesCtrl'
      })
      .state('user.manage.admin.employees', {
        url: '/employees',
        templateUrl: templatesBaseUrl + '/manage.admin.employees.view.html',
        controller: 'ManageAdminEmployeesCtrl'
      })

      /*manage account state*/
      .state('user.manage.account', {
        abstract: true,
        url: '/account',
        templateUrl: templatesBaseUrl + '/manage.account.view.html',
        controller: 'ManageAccountCtrl'
      })
      .state('user.manage.account.accountInfo', {
        url: '/account-information',
        templateUrl: templatesBaseUrl + '/manage.account.accountInfo.view.html',
        controller: 'ManageAccountAccountInfoCtrl'
      })
      .state('user.manage.account.billingInfo', {
        url: '/billing-information',
        templateUrl: templatesBaseUrl + '/manage.account.billingInfo.view.html',
        controller: 'ManageAccountBillingInfoCtrl'
      })
      .state('user.manage.account.downloadHistory', {
        url: '/download-history',
        templateUrl: templatesBaseUrl + '/manage.account.downloadHistory.view.html',
        controller: 'ManageAccountDownloadHistoryCtrl'
      });

  }

})();
