/**
 * Created by Shahzad on 5/04/2015.
 */

(function() {

  'use strict';

  angular
    .module('workExample')
    .config( RoutesConfig );

  RoutesConfig.$inject = [
    '$stateProvider'
  ];

  function RoutesConfig( $stateProvider ) {
    var templatesBaseUrl = 'app/pages/';

    $stateProvider
      .state('user', {
        abstract: true,
        url: '/user',
        templateUrl: 'app/shared/abstract/abstract.user.view.html'
      })
      .state('user.home', {
        url: '/dashboard',
        templateUrl: templatesBaseUrl + 'home/home.user.view.html',
        controller: 'HomeUserCtrl'
      });
  }

})();
