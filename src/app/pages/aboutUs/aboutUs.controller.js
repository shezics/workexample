/**
 * Created by Shahzad on 5/04/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('AboutUsCtrl',  AboutUsCtrl );

  AboutUsCtrl.$inject = ['$scope', '$controller', 'ourTeamFactory', '$window'];

  function AboutUsCtrl( $scope, $controller, ourTeamFactory, $window ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    $scope.description = 'This is a about us page.';

    /*VM properties*/
    $scope.ourTeams = ourTeamFactory.getOurTeam().slice(0, 6);

    /*function declarations*/

  }

})();
