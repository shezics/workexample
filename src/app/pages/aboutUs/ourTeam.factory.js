/**
 * Created by Owais raza on 12/16/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .service('ourTeamFactory', ourTeamFactory);

  ourTeamFactory.$inject = ['$state', 'utilFactory', 'OUR_TEAM'];

  function ourTeamFactory( $state, utilFactory, OUR_TEAM ) {

    var lastFoundOurTeam;

    return {
      getOurTeam: getOurTeam,
      getLastFoundOurTeam: getLastFoundOurTeam,
      validateOurTeam: validateOurTeam
    };

    //get newsFeed list
    function getOurTeam() {
      //TODO get newsFeed from local Storage or JSON ajax
      return OUR_TEAM;
    }

    //gets newsFeed object against current route
    function getLastFoundOurTeam() {
      return lastFoundOurTeam;
    }

    //validates newsFeed route
    function validateOurTeam( event, toState, toParams, fromState, fromParams ) {
      //validates new state for products > productId
      var ourTeam;

      ourTeam = utilFactory.findWhereArrayObj('url', toParams.ourTeamId, OUR_TEAM );

      if( ourTeam ) {
        lastFoundOurTeam = ourTeam;
      } else {
        event.preventDefault();
        $state.transitionTo('about-us.ourTeam');
      }
    }
  }

})();

