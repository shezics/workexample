/**
 * Created by Shahzad on 5/04/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('ContactCtrl',  ContactCtrl );

  ContactCtrl.$inject = ['$scope', '$controller', '$timeout', 'userFactory'];

  function ContactCtrl( $scope, $controller, $timeout, userFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    /*initialization*/
    setContactModel();

    /*VM functions*/
    $scope.resetForm = resetForm;
    $scope.sendContactRequest = sendContactRequest;

    /*VM properties*/
    $scope.status = {};
    $scope.formSending = false;

    //resets the form fields and validation states.
    function resetForm() {

      //resets last attempt results.
      $scope.status = {};

      //resets form validation and interaction state
      $scope.form.$setPristine();
      $scope.form.$setUntouched();

      //resets contact model
      setContactModel();
    }

    //resets contact model to default.
    function setContactModel() {
      $scope.contactObj = {
        Name: '',
        Email: '',
        Message: ''
      };
    }

    //to send a contact request
    function sendContactRequest() {

      //handle form submission with invalid data
      if ( $scope.form.$invalid ) {
        $scope.status.type = 'error';
        $scope.status.message = 'please correct the red marked fields first.';
        return;
      }

      //resets last attempt results.
      $scope.status = {};

      //setting formSending flag to true.
      $scope.formSending = true;

      userFactory.contactRequest( $scope.contactObj )
        .then(function( response ) {

          $scope.status.type = 'success';
          $scope.status.message = response;
          $scope.formSending = false;

          $timeout(function() {
            resetForm();
          }, 2500);

        }, function( reason ) {

          $scope.status.type = 'error';
          $scope.status.message = reason;
          $scope.formSending = false;

        });
    }
  }

})();
