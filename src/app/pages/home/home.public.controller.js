/**
 * Created by Shahzad on 5/04/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('HomePublicCtrl', HomePublicCtrl );

  HomePublicCtrl.$inject = ['$scope', '$controller', '$rootScope', '$localStorage', 'userFactory', 'productsFactory', 'newsFeedFactory', '$window'];

  function HomePublicCtrl( $scope, $controller, $rootScope, $localStorage, userFactory, productsFactory, newsFeedFactory, $window ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    /*private variables*/
    var onAttemptToUserPageDestroyer;

    /*listeners*/
    onAttemptToUserPageDestroyer = $rootScope.$on('attemptToUserPage', onAttemptToUserPage);

    /*VM properties*/
    $scope.products = productsFactory.getProducts().slice(0, 4);
    $scope.newsFeeds = newsFeedFactory.getNewsFeed().slice(0, 6);
    $scope.onDestroy = onDestroy;

    /*initialization*/

    if($localStorage.redirectTo){
      userFactory.openLoginModel();
    }

    /*function declarations*/

    //listner on destroy of current scope.
      function onDestroy() {
        onAttemptToUserPageDestroyer();
      }

    //listener to attempt user pages routes
    function onAttemptToUserPage() {
      userFactory.openLoginModel();
    }
  }

})();
