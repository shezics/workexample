/**
 * Created by Shahzad on 6/27/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('HomeAddLeadToFarmModalCtrl', HomeAddLeadToFarmModalCtrl );

  HomeAddLeadToFarmModalCtrl.$inject = ['$scope', '$controller', '$modal', 'homeUserFactory', 'manageFormFactory'];

  function HomeAddLeadToFarmModalCtrl( $scope, $controller, $modal,  homeUserFactory, manageFormFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseModalCtrl', {$scope: $scope});

    /*VM functions*/

    /*VM props*/
    $scope.addLeadToFarmObj = {
      status : {}
    };

    /*private variables*/
    $scope.farmLeads = [];
    //...

    //defer resolve.
    manageFormFactory.getLeadFarms()
      .then(function(res) {
        $scope.farmLeads = res;
      }, function() {
        $scope.status.sending = false;
      });

    //initialize Stuff
    //inherited from BaseModalCtrl.to prevent modal from closing, while modal is in sending status.
    $scope.$listenFormStatus(  $scope.addLeadToFarmObj );

  }

})();
