/**
 * Created by Shahzad on 5/04/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('HomeUserCtrl', HomeUserCtrl );

  HomeUserCtrl.$inject = ['$scope', '$controller'];

  function HomeUserCtrl( $scope, $controller ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    /*listeners*/

    /*VM function*/

    /*VM properties*/
    $scope.mapOptions = {
      //lonLat: [-117.844105, 33.683219],
      //markers: [{
      //  lonLat: [-117.844105, 33.683219],
      //  image: 'assets/images/_pin.png',
      //  popup: {
      //    width: 200,
      //    height: 180,
      //    markup: [
      //      '<div class="info-box">',
      //      ' <h3>Benutech Inc. LLC</h3>',
      //      ' <p>2525 Main Street, <br/>',
      //      '  Suite 200, Irvine, <br/>',
      //      '  CA, 92614 <br/>',
      //      '  United States',
      //      ' </p>',
      //      '</div>'
      //    ].join(' ')
      //  }
      //}],
      zoomControls: false,
      calculateHeight : true,
      listenResize : true
    };

    /*functions declarations */

  }
})();
