/**
 * Created by Shahzad on 5/04/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .service('homeUserFactory', homeUserFactory );

  homeUserFactory.$inject = ['$state', '$modal', 'utilFactory'];

  function homeUserFactory( $state, $modal, utilFactory ) {

    return {
      searchForm: searchForm,
      selectFromMap: selectFromMap,
      addLeadToFarm: addLeadToFarm,
      showFarmDetails: showFarmDetails,
      openQueryToolModal: openQueryToolModal
    };

    //opens up search form modal for user home page.
    function searchForm() {
      return $modal.open({
        controller: 'HomeSearchFormModalCtrl',
        templateUrl: 'app/pages/home/home.user.searchFormModal.view.html',
        size: 'sm',
        windowClass: 'search-form'
      });
    }

    //opens select from map modal.
    function selectFromMap() {
      return $modal.open({
        controller: 'HomeSelectFromMapModalCtrl',
        templateUrl: 'app/pages/home/home.user.selectFromMapModal.view.html',
        //size: 'md',
        windowClass: 'select-from-map'
      });
    }

    //opens add lead to farm modal.
    function addLeadToFarm() {
      return $modal.open({
        controller: 'HomeAddLeadToFarmModalCtrl',
        templateUrl: 'app/pages/home/home.user.addLeadToFarmModal.view.html',
        size: 'sm',
        windowClass: 'add-lead'
      });
    }

    //opens a farm details modal.
    function showFarmDetails() {
      return $modal.open({
        controller: 'HomeFarmDetailsModalCtrl',
        templateUrl: 'app/pages/home/home.user.farmDetailsModal.view.html',
        size: 'lg',
        windowClass: 'farm-details'
      });
    }

    //opens a search form modal for different orders like lead type, geography, character etc.
    function openQueryToolModal() {
      return $modal.open({
        controller: 'QueryToolModalCtrl',
        templateUrl: 'app/shared/queryTool/queryToolModal.view.html',
        size: 'lg',
        windowClass: 'search-order query-tool'
      });
    }
  }

})();
