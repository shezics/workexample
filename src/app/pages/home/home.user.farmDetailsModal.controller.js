/**
 * Created by Shahzad on 6/27/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('HomeFarmDetailsModalCtrl', HomeFarmDetailsModalCtrl );

  HomeFarmDetailsModalCtrl.$inject = ['$scope', '$controller', '$modal', 'homeUserFactory', 'manageFormFactory', 'FIELD_TYPES'];

  function HomeFarmDetailsModalCtrl( $scope, $controller, $modal,  homeUserFactory, manageFormFactory, FIELD_TYPES ) {

    /*Extend baseCtrl*/
    $controller('BaseModalCtrl', {$scope: $scope});

    /*VM functions*/
    $scope.getAllFarms = manageFormFactory.getAllFarms;
    $scope.viewDetails = manageFormFactory.viewDetails;
    $scope.viewOnMap = manageFormFactory.viewOnMap;
    $scope.types = FIELD_TYPES.farmOptions;
    $scope.selectAll = selectAll;

    /*VM props*/
    $scope.farmDetailsObj = {
      status : {}
    };

    /*private variables*/
    $scope.farms = [];


    /*initialize Stuff*/
    //inherited from BaseModalCtrl.to prevent modal from closing, while modal is in sending status.
    $scope.$listenFormStatus( $scope.farmDetailsObj );

    //get list of all farms
    manageFormFactory.getAllFarms()
      .then(function(res) {
        $scope.farms = res;
      }, function(reason) {
        $scope.status.sending = false;
      });

    /*functions declarations*/

    //select all checkboxes
    function selectAll () {
      angular.forEach($scope.farms, function (farm) {
        farm.selected = $scope.selectedAll;
      });

    }
  }

})();
