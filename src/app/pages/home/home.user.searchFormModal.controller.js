/**
 * Created by Shahzad on 6/12/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('HomeSearchFormModalCtrl', HomeSearchFormModalCtrl );

  HomeSearchFormModalCtrl.$inject = ['$scope', '$controller', '$modal', '$state', '$timeout', 'homeUserFactory'];

  function HomeSearchFormModalCtrl( $scope, $controller, $modal, $state, $timeout, homeUserFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseModalCtrl', {$scope: $scope});

    /*VM functions*/
    $scope.changeTab = changeTab;

    /*VM props*/
    $scope.searchFormObj = {
      status : {}
    };

    /*private variables*/
    $scope.activeTabIndex = 0;
    //...

    //initialize Stuff
    //inherited from BaseModalCtrl.to prevent modal from closing, while modal is in sending status.
    $scope.$listenFormStatus(  $scope.searchFormObj );


    //change activeTabIndex function
    function changeTab( index ){
      $scope.activeTabIndex = index;
    }

    //close the modal after successful login
    function closeModal() {
      $timeout(function() {
        $scope.$close();
        $state.transitionTo('user.home');
      }, 2000);
    }
  }

})();
