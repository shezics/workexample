/**
 * Created by Shahzad on 6/12/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('HomeSelectFromMapModalCtrl', HomeSelectFromMapModalCtrl );

  HomeSelectFromMapModalCtrl.$inject = ['$scope', '$controller', '$modal', '$state', '$timeout', 'homeUserFactory'];

  function HomeSelectFromMapModalCtrl( $scope, $controller, $modal, $state, $timeout, homeUserFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseModalCtrl', {$scope: $scope});

    /*VM functions*/
    $scope.changeTab = changeTab;
    $scope.changeSubTab = changeSubTab;

    /*VM props*/
    $scope.selectFromMapObj = {
      status : {}
    };
    $scope.mapOptions = {
      zoomControls: false
    };

    /*private variables*/
    $scope.activeTabIndex = 0;
    $scope.activeSubTabIndex = 0;
    //...

    //initialize Stuff
    //inherited from BaseModalCtrl.to prevent modal from closing, while modal is in sending status.
    $scope.$listenFormStatus( $scope.selectFromMapObj );

    //to change a tab
    function changeTab( index ){
      $scope.activeTabIndex = index;
    }

    function changeSubTab(index) {
      $scope.activeSubTabIndex = index;
    }

    //close the modal after successful login
    function closeModal() {
      $timeout(function() {
        $scope.$close();
        $state.transitionTo('user.home');
      }, 2000);
    }
  }

})();
