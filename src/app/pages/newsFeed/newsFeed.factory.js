/**
 * Created by Owais raza on 8/6/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .service('newsFeedFactory', newsFeedFactory);

  newsFeedFactory.$inject = ['$state', 'utilFactory', 'NEWS_FEED'];

  function newsFeedFactory( $state, utilFactory, NEWS_FEED ) {

    var lastFoundNewsFeed;

    return {
      getNewsFeed: getNewsFeed,
      getLastFoundNewsFeed: getLastFoundNewsFeed,
      validateNewsFeed: validateNewsFeed
    };

    //get newsFeed list
    function getNewsFeed() {
      //TODO get newsFeed from local Storage or JSON ajax
      return NEWS_FEED;
    }

    //gets newsFeed object against current route
    function getLastFoundNewsFeed() {
      return lastFoundNewsFeed;
    }

    //validates newsFeed route
    function validateNewsFeed( event, toState, toParams, fromState, fromParams ) {
      //validates new state for products > productId
      var newsFeed;

      newsFeed = utilFactory.findWhereArrayObj('url', toParams.newsFeedId, NEWS_FEED );

      if( newsFeed ) {
        lastFoundNewsFeed = newsFeed;
      } else {
        event.preventDefault();
        $state.transitionTo('public.newsFeed');
      }
    }
  }

})();

