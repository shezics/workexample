/**
 * Created by Shahzad on 5/22/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('ProductDetailsCtrl',  ProductDetailsCtrl );

  ProductDetailsCtrl.$inject = ['$scope', '$controller', 'productsFactory'];

  function ProductDetailsCtrl( $scope, $controller, productsFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    /*VM properties*/
    $scope.products = productsFactory.getProducts();
    $scope.currentProduct = productsFactory.getLastFoundProduct();

  }

})();
