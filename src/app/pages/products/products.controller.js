/**
 * Created by Shahzad on 5/04/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('ProductsCtrl',  ProductsCtrl );

  ProductsCtrl.$inject = ['$scope', '$controller', 'productsFactory'];

  function ProductsCtrl( $scope, $controller, productsFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    $scope.products = productsFactory.getProducts();
  }

})();
