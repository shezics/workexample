/**
 * Created by Shahzad on 5/04/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .service('productsFactory', productsFactory );

  productsFactory.$inject = ['$state', 'utilFactory', 'PRODUCTS'];

  function productsFactory( $state, utilFactory, PRODUCTS ) {

    var lastFoundProduct;

    return {
      getProducts: getProducts,
      getLastFoundProduct: getLastFoundProduct,
      validateProduct: validateProduct
    };

    //get products list
    function getProducts() {
      //TODO get products from local Storage or JSON ajax
      return PRODUCTS;
    }

    //gets product object against current route
    function getLastFoundProduct() {
      return lastFoundProduct;
    }

    //validates product route
    function validateProduct( event, toState, toParams, fromState, fromParams ) {
      //validates new state for products > productId
      var product;

      product = utilFactory.findWhereArrayObj('url', toParams.productId, PRODUCTS );

      if( product ) {
        lastFoundProduct = product;
      } else {
        event.preventDefault();
        $state.transitionTo('public.products');
      }
    }
  }

})();
