/**
 * Created by Shahzad on 5/04/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('SignUpCtrl',  SignUpCtrl );

  SignUpCtrl.$inject = ['$scope', '$controller', 'userFactory'];

  function SignUpCtrl( $scope, $controller, userFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    /*VM functions*/
    //...

    /*VM properties*/
    $scope.userFormObj = {
      handlers: {},
      //user: { TbUser: { type: 1 }},
      config: {
        page: 'signup',
        controlsOnRight: true,
        editUser: false,
        hideControls: ['userType', 'offices'],
        submitText: 'Create my account',
        successMessage: 'You account has been created successfully.'
      }
    };


    /*initialization*/
    var user;

    //TODO: Remove - for development purpose only
    window.debug = false;
    //window.debug = true;

    //pre-defined user model for development purpose only
    user = window.debug ? getDummyUserData() : userFactory.getEmptyUserModel();
    user.TbAssociation.parent_user_id = undefined;
    $scope.userFormObj.user = user;

    /*functions declarations*/

    //gets user model with dummy details
    function getDummyUserData() {
      return {
        TbAssociation:{
          parent_user_id: 'none'
        },
        TbUser: {
          username: 'sometestuser1@domain.com',
          password: '1234567',
          confirm_password: '1234567',
          type: 1,
          company_name: 'Test Company',
          first_name: 'Test',
          last_name: 'User'
        },
        TbAddress: [{
          address: '1234 Main St',
          address_2: '',
          city: 'Newport Beach',
          state: 'Ca',
          zip: '92646'
        }],
        TbPhone: [{
          phone: '3334445556'
        }]
      };
    }
  }

})();
