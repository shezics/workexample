/**
 * Created by Shahzad on 5/04/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('SupportCtrl',  SupportCtrl );

  SupportCtrl.$inject = ['$scope', '$controller', 'SUPPORT_CATEGORIES'];

  function SupportCtrl( $scope, $controller, SUPPORT_CATEGORIES ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    $scope.supportCategories = SUPPORT_CATEGORIES;
  }

})();
