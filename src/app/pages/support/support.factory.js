/**
 * Created by Shahzad on 5/04/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .service('supportFactory', supportFactory );

  supportFactory.$inject = ['$state', 'utilFactory', 'SUPPORT_CATEGORIES'];

  function supportFactory( $state, utilFactory, SUPPORT_CATEGORIES ) {

    var lastFoundCategory;

    return {
      getSupportCategories: getSupportCategories,
      getLastFoundCategory: getLastFoundCategory,
      validateSupportCategory: validateSupportCategory
    };

    //gets support categories
    function getSupportCategories() {
      //TODO get support-categories from local Storage or JSON ajax
      return SUPPORT_CATEGORIES;
    }

    //gets category object against the current route
    function getLastFoundCategory() {
      return lastFoundCategory;
    }

    //validate support category route
    function validateSupportCategory( event, toState, toParams, fromState, fromParams ) {
      //validates new state for support > categoryName
      var category;

      category = utilFactory.findWhereArrayObj('url', toParams.categoryId, SUPPORT_CATEGORIES );

      if( category ) {
        lastFoundCategory = category;
      } else {
        event.preventDefault();
        $state.transitionTo('public.support');
      }
    }
  }

})();
