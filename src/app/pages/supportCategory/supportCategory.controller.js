/**
 * Created by Shahzad on 5/22/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('SupportCategoryCtrl',  SupportCategoryCtrl );

  SupportCategoryCtrl.$inject = ['$scope', '$controller', 'supportFactory'];

  function SupportCategoryCtrl( $scope, $controller, supportFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    /*VM properties*/
    $scope.supportCategories = supportFactory.getSupportCategories();
    $scope.currentCategory = supportFactory.getLastFoundCategory();
    $scope.accordionStates = [true]; //array of isOpen flags for accordian groups. first is set to true by default.
  }

})();
