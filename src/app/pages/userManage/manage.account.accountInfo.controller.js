/**
 * Created by Shahzad on 5/04/2015.
 */

(function () {

  'use strict';

  angular.module('workExample')
    .controller('ManageAccountAccountInfoCtrl',  ManageAccountAccountInfoCtrl );

  ManageAccountAccountInfoCtrl.$inject = ['$scope', '$controller', 'manageAccountFactory'];

  function ManageAccountAccountInfoCtrl ( $scope, $controller, manageAccountFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    /*VM functions*/
    $scope.changePassword = changePassword;
    $scope.resetPasswordForm = resetPasswordForm;

    /*VM variables*/
    $scope.passwordFormObj = {};
    $scope.userFormObj = {
      handlers: {},
      user: manageAccountFactory.getUserPersonalInfo(),
      config: {
        page: 'profile',
        controlsOnRight: false,
        editUser: true,
        onPhoneRemove : manageAccountFactory.onPhoneRemove,
        onSuccess: onSuccess,
        onReset: onResetUserForm,
        hideControls: ['username', 'userType', 'offices', 'reps', 'password', 'agreeCheck'],
        submitText:'Update my profile',
        successMessage: 'Your profile has been updated successfully.'
      }
    };

    /*private variables*/
    //...
    //if ( $scope.passwordFormObj.data.TbUser.password !== $scope.passwordFormObj.data.TbUser.confirm_password ) {
    //
    //}
    /*functions declarations*/

    //on success handler for user-form directive config.
    function onSuccess( userDetails ) {
      manageAccountFactory.updateLoggedInUserObj(userDetails);
      $scope.userFormObj.user = manageAccountFactory.getUserPersonalInfo();
    }

    //on reset handler for user-form directive config.
    function onResetUserForm() {
      $scope.userFormObj.user = manageAccountFactory.getUserPersonalInfo();
    }

    //changes user's password
    function changePassword( passwordForm ) {

      if( passwordForm.$invalid ) {
        $scope.passwordFormObj.status = {
          type : 'error' ,
          message : 'please correct the red marked fields first.'
        };
        return;
      }

      if ( $scope.passwordFormObj.data.TbUser.password !== $scope.passwordFormObj.data.TbUser.confirm_password ) {
        $scope.passwordFormObj.status = {
          type : 'error' ,
          message : 'password does not match.'
        };

        $scope.passwordForm.confirm_password.$setValidity('minlength', false);
        return;
      }

      //setting formSending flag to true.
      $scope.passwordFormObj.status = {
        sending: true
      };

      manageAccountFactory.changePassword( $scope.passwordFormObj.data )
        .then(function( res ) {
          $scope.passwordFormObj.status = {
            type : 'success' ,
            message : 'password has been changed successfully.'
          };

        }, function( reason ) {
          $scope.passwordFormObj.status = {
            type : 'error' ,
            message : reason
          };
        });
    }

    //resets password form
    function resetPasswordForm( passwordForm ) {
      //resets password form obj
      $scope.passwordFormObj = {};

      //resets form validation and interaction state
      passwordForm.$setPristine();
      passwordForm.$setUntouched();

    }
  }
})();
