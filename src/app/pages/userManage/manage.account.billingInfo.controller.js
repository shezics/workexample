/**
 * Created by Shahzad on 5/04/2015.
 */

(function () {

  'use strict';

  angular.module('workExample')
    .controller('ManageAccountBillingInfoCtrl',  ManageAccountBillingInfoCtrl );

  ManageAccountBillingInfoCtrl.$inject = ['$scope', '$controller', 'manageAccountFactory'];

  function ManageAccountBillingInfoCtrl ( $scope, $controller, manageAccountFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    /*VM functions*/

    /*VM properties*/
    $scope.billingHistory = [];
    $scope.currentPage = 1;

    /*private variables*/
    //...

    /*Initialization stuff*/
    //getBillingHistory();

    /*functions declarations*/

    //gets download history from Server for the current logged-in user.
    function getBillingHistory() {
      manageAccountFactory.getBillingHistory()
        .then(function( res ) {
          $scope.billingHistoryError = false;
          $scope.billingHistory = res.data;
        }, function( reason ) {
          //handle this case
          $scope.billingHistoryError = reason;
        });
    }
  }

})();
