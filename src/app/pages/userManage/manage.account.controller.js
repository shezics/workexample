/**
 * Created by Shahzad on 6/05/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('ManageAccountCtrl', ManageAccountCtrl );

  ManageAccountCtrl.$inject = ['$scope', '$controller', 'manageAccountFactory', 'manageFactory'];

  function ManageAccountCtrl( $scope, $controller, manageAccountFactory, manageFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    /*VM functions*/
    //...

    /*VM properties*/
    //...

  }

})();
