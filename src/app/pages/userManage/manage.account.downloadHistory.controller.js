/**
 * Created by Shahzad on 5/04/2015.
 */

(function () {

  'use strict';

  angular.module('workExample')
    .controller('ManageAccountDownloadHistoryCtrl',  ManageAccountDownloadHistoryCtrl );

  ManageAccountDownloadHistoryCtrl.$inject = ['$scope', '$controller', 'manageAccountFactory'];

  function ManageAccountDownloadHistoryCtrl ( $scope, $controller, manageAccountFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    /*VM functions*/
    //...

    /*VM properties*/
    $scope.downloadHistory = [];
    $scope.currentPage = 1;

    /*private variables*/
    //...

    /*Initialization stuff*/
    getDownloadHistory();


    /*functions declarations*/

    //gets download history from Server for the current logged-in user.
    function getDownloadHistory() {
      manageAccountFactory.getDownloadHistory()
        .then(function( res ) {
          $scope.downloadHistoryError = false;
          $scope.downloadHistory = res.data;
        }, function( reason ) {
          //handle this case
          $scope.downloadHistoryError = reason;
        });
    }
  }

})();
