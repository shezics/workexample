/**
 * Created by Shahzad on 5/04/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .service('manageAccountFactory', manageAccountFactory );

  manageAccountFactory.$inject = [
    '$q',
    '$http',
    'appConstant',
    'manageFactory',
    'cacheFactory',
    'utilFactory',
    'userFactory',
    '$localStorage'
  ];

  function manageAccountFactory( $q, $http, appConstant, manageFactory, cacheFactory, utilFactory, userFactory, $localStorage ) {

    return {
      changePassword: userFactory.changePassword,
      updateLoggedInUserObj: manageFactory.updateLoggedInUserObj,
      getDownloadHistory: getDownloadHistory,
      getBillingHistory: getBillingHistory,
      getUserPersonalInfo: getUserPersonalInfo,
      onPhoneRemove: onPhoneRemove
    };

    //to get download history of current user, from server.
    function getDownloadHistory() {
      var defer;

      defer = $q.defer();
      $http.get( appConstant.baseURL + 'webservices/download_history.json')
        .success(function ( res ) {
          if ( res.response.status === 'OK' ) {
            //console.log('download history fetched successfully.');

            //TODO remove when records length is sufficient for pagination.
            //HECK adds dummy data to simulate pagination.
            if ( res.response.data.length <= 10 ) {
              var item = res.response.data[0];

              for (var i = 0; i < 60; i++ ) {
                res.response.data.push( {
                  "cusip":"00000000" + i,
                  "coupon":"0.04050",
                  "principle":"96908.70",
                  "date_viewed":"02-01-2015",
                  "status":"Viewed",
                  "payment":"Subscription"
                });
              }
            }

            defer.resolve( res.response );
          } else {
            //console.log('fetching download history failed.');
            //console.log( res );
            defer.reject(  res.response.data[0] );
          }
        })
        .error(function ( reason, status ) {
          //console.log('fetching download history failed');
          //console.log( reason );
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason);
        });

      return defer.promise;
    }

    //to get billing history of current user, from server.
    function getBillingHistory() {
      var defer;

      defer = $q.defer();
      $http.get( appConstant.baseURL + 'webservices/billing_history.json')
        .success(function ( res ) {
          if ( res.response.status === 'OK' ) {
            //console.log('download history fetched successfully.');

            //TODO remove when records length is sufficient for pagination.
            //HECK adds dummy data to simulate pagination.
            if ( res.response.data.length <= 10 ) {
              var item = res.response.data[1];

              for (var i = 0; i < 60; i++ ) {
                res.response.data.push({
                  "date":"06-01-2014",
                  "trans_id":"0000000" + i,
                  "type":"Monthly Subscription",
                  "price":"499.00",
                  "discount":"10%",
                  "total":"450.00",
                  "actions":"<a href='#'>Invoice</a>"
                });
              }
            }

            defer.resolve( res.response );
          } else {
            //console.log('fetching download history failed.');
            //console.log( res );
            defer.reject(  res.response.data[0] );
          }
        })
        .error(function ( reason, status ) {
          //console.log('fetching download history failed');
          //console.log( reason );
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason);
        });

      return defer.promise;
    }

    //gets logged-in user's model , received in login response.
    function getUserPersonalInfo() {
      //obtaining a copy so that changes can only reflect when personal info request succeed.
      return cacheFactory.getModel('user', true);
      //return angular.copy( $localStorage.user );
    }

    // on  handler for user-form directive config
    function onPhoneRemove( index, phone ) {
      $localStorage.user.TbPhone.splice(index, 1);
      phone.sending = false;
    }

  }

})();
