/**
 * Created by Shahzad on 6/05/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('ManageAdminCtrl', ManageAdminCtrl );

  ManageAdminCtrl.$inject = ['$scope', '$controller', '$timeout', 'manageUsersFactory', 'userFactory', 'manageOfficesFactory'];

  function ManageAdminCtrl( $scope, $controller, $timeout, manageUsersFactory, userFactory, manageOfficesFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    /*VM function*/
    $scope.isOfficeAllowed = manageOfficesFactory.isOfficeAllowed;

  }

})();
