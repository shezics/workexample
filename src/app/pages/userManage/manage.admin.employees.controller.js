/**
 * Created by Shahzad on 8/01/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('ManageAdminEmployeesCtrl', ManageAdminEmployeesCtrl );

  ManageAdminEmployeesCtrl.$inject = ['$scope', '$controller', '$timeout', 'manageEmployeesFactory', 'userFactory'];

  function ManageAdminEmployeesCtrl( $scope, $controller, $timeout, manageEmployeesFactory, userFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    /*VM functions*/
    $scope.showEmployeesPipeline = showEmployeesPipeline;
    $scope.disableEmployee = disableEmployee;
    $scope.resetEmployeeStatusForm = resetEmployeeStatusForm;
    $scope.resetEmployeeForm = resetEmployeeForm;
    $scope.submitEmployeeForm = submitEmployeeForm;

    /*VM properties*/
    $scope.employeeStatusFormObj = {status: {} };
    $scope.employeeFormObj = { status:{} };

    //for employees pipeline
    $scope.pipelineObj = {
      data: {},
      status: {},
      config: {
        type: 'employees',
        selectOne: false,
        addRecord: {
          title: 'Add New Employee',
          handler: showAddUpdateEmployee
        },
        columns: [{
          value : 'customName',
          title : 'Name'
        }, {
          value : 'customAddress',
          title : 'Address'
        }, {
          value : 'customPhone',
          title : 'Phone'
        }, {
          value : 'customEmail',
          title : 'Email'
        }, {
          value : 'customEmployeeInfo',
          title : 'Employee Info'
        }]
        , actions: [{
          title: 'Edit',
          handler: showAddUpdateEmployee
        }]
      }
    };

    /*initialize*/
    setAddModeEmployeesForm();

    /*Functions declarations*/

    //disables/removes/deletes selected employee's profile
    function disableEmployee() {

      $scope.employeeStatusFormObj.status = { sending: true };

      manageEmployeesFactory.disableEmployeeProfile( $scope.employeeStatusFormObj.data )
        .then(function( res ) {
          $scope.employeeStatusFormObj.status = {
            type: 'success',
            message: res
          };

          //deletes employee from pipeline
          manageEmployeesFactory.removeEmployeeFromPipeline( $scope.employeeStatusFormObj.serial_number, $scope );

          $timeout(function(){
            showEmployeesPipeline();
          }, 2000);
        }, function( reason ) {
          $scope.employeeStatusFormObj.status = {
            type: 'error',
            message: reason
          };
        });
    }

    function resetEmployeeStatusForm(){
      $scope.employeeStatusFormObj.disableEmployee = false;
    }

    //setups the add/update employee view
    function showAddUpdateEmployee( employee ) {

      //sets object for creating office form
      employee ? setEditModeEmployeesForm(employee) : setAddModeEmployeesForm();

      //uses flip-box directive to show and hide flip node with selected indexes,
      $scope.flip(1, 0);
    }

    //sets properties to add a new employee using employeeForm directive
    function setAddModeEmployeesForm() {
      $scope.employeeFormObj.currentEmployee= getEmptyEmployee();
      $scope.employeeFormObj.data = getEmptyEmployee();
      $scope.employeeFormObj.status = {};
      $scope.employeeFormObj.editState = false;

      $scope.employeeStatusFormObj.serial_number = undefined;
      $scope.employeeStatusFormObj.data = {};
      $scope.employeeStatusFormObj.status = {};
    }

    //sets properties to edit existing employee using employeeForm directive
    function setEditModeEmployeesForm( employee ) {
      employee.TbUser = employee.TbUser[0];
      $scope.employeeFormObj.currentEmployee = employee;
      $scope.employeeFormObj.data = angular.copy(employee);
      $scope.employeeFormObj.status = {};
      $scope.employeeFormObj.editState = true;

      $scope.employeeStatusFormObj.serial_number = employee.serial_number;
      $scope.employeeStatusFormObj.data = {
        TbEmployee: {
          employee_id: employee.TbEmployee.employee_id
        }
      };
      $scope.employeeStatusFormObj.status = {};
    }

    //creates an empty object for office form - HACK angular-digest.
    function getEmptyEmployee() {
      return {
        TbEmployee: {},
        TbEmail: [{
          email: '',
          type: ''
        }],
        TbPhone: [{
          phone: '',
          type: ''
        }],
        TbAddress: [{}]
      }
    }

    //get back to employees pipeline view
    function showEmployeesPipeline() {

      //uses data-employee-form="" directive, which gives resetHandler to reset form from outside.
      resetEmployeeForm();

      //uses data-flip-box="" directive to show and hide flip node with selected indexes,
      $scope.flip(0, 1);
    }

    //resets employee form
    function resetEmployeeForm() {
      $scope.employeeFormObj.data = angular.copy( $scope.employeeFormObj.currentEmployee );
      $scope.employeeFormObj.status = {};

      //resets form validation and interaction state
      $scope.employeeForm.$setPristine();
      $scope.employeeForm.$setUntouched();
      $scope.employeeForm.$setDirty();
    }

    //creates or updates an employees.
    function submitEmployeeForm() {
      //handle form submission with invalid data
      if ( $scope.employeeForm.$invalid ) {
        resetEmployeesFormStatus('error', 'please correct the red marked fields first.');
        return;
      }

      //handle if employee submits form with no changes
      if ( !$scope.employeeForm.$dirty ) {
        resetEmployeesFormStatus('error', 'No changes made to save.');
        return;
      }

      //resets previous attempt
      resetEmployeesFormStatus();
      $scope.employeeFormObj.status.sending = true;

      manageEmployeesFactory.addUpdateEmployee( $scope.employeeStatusFormObj.serial_number, $scope.employeeFormObj.data, $scope )
        .then(function(res) {
          resetEmployeesFormStatus('success', res.message);
          $scope.employeeForm.$setPristine();
          //setEditModeEmployeesForm(res.employee);
        }, function(reson){
          resetEmployeesFormStatus('error', reson);
        });
    }

    //resets status object without breaking reference.
    function resetEmployeesFormStatus(type, message) {
      $scope.employeeFormObj.status.sending = false;
      $scope.employeeFormObj.status.type = type;
      $scope.employeeFormObj.status.message = message;
    }
  }
})();
