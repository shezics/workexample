/**
 * Created by Shahzad on 8/12/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .service('manageEmployeesFactory', manageEmployeesFactory );

  manageEmployeesFactory.$inject = [
    '$q',
    '$http',
    'appConstant',
    'pipelineFactory',
    'utilFactory'
  ];

  function manageEmployeesFactory( $q, $http, appConstant, pipelineFactory, utilFactory ) {

    return {
      fetchEmployeesPipeline: fetchEmployeesPipeline,
      parseEmployeesModel: parseEmployeesModel,
      removeEmployeeFromPipeline: removeEmployeeFromPipeline,
      disableEmployeeProfile: disableEmployeeProfile,
      addUpdateEmployee: addUpdateEmployee
    };

    //Adds a new employee on server and then in local pipeline.
    function addUpdateEmployee(serial_number, payload, $scope) {
      var defer, request, endpoint;

      defer = $q.defer();
      request = JSON.stringify( payload );
      endpoint = serial_number >= 0 ? 'edit_employee.json' : 'add_employee.json';

      $http.post( appConstant.baseURL + 'webservices/' + endpoint, request)
        .success(function ( res ) {
          res = res.response;

          if ( res.status === 'OK' ) {
            defer.resolve({
              employee: res.data.object,
              message: 'employee has been '+  (serial_number >= 0 ? 'updated' : 'created') +' successfully.'
            });

            AddUpdateEmployeesInPipeline(serial_number, res.data.object, $scope);
          } else {
            defer.reject( res.data.length ? res.data.join(', ').toLowerCase() : 'error occurred.' );
          }
        })
        .error(function ( reason, status ) {
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject(reason);
        });

      return defer.promise;
    }

    //adds or updates a employee profile in user pipeline
    function AddUpdateEmployeesInPipeline( serial_number, employee, scope ) {
      var pipelineEmployee, index;

      if ( serial_number ) {
        //parse the model before adding/updating in pipeline
        parseEmployeesModel(employee, serial_number);

        pipelineEmployee = utilFactory.findWhereArrayObj('serial_number', serial_number, scope.pipelineObj.data.records);
        index = scope.pipelineObj.data.records.indexOf(pipelineEmployee);

        angular.extend(scope.pipelineObj.data.records[index], employee);
      } else {
        //parse the model before adding/updating in pipeline
        parseEmployeesModel(employee, scope.pipelineObj.data.records + 1);

        scope.pipelineObj.data.records.push( employee );
      }
    }

    //removes a employee from user pipeline
    function removeEmployeeFromPipeline( serial_number, scope ){
      var user, index;

      user = utilFactory.findWhereArrayObj('serial_number', serial_number, scope.pipelineObj.data.records);
      index = scope.pipelineObj.data.records.indexOf(user);

      scope.pipelineObj.data.records.splice(index, 1);
    }

    //fetches employee records for given page
    function fetchEmployeesPipeline( options ) {
      var defer;

      defer = $q.defer();
      $http.get( appConstant.baseURL + 'webservices/list_employees/page:'+ options.pageNum + '.json')
        .success(function ( res ) {

          if ( res && res.response.status === 'OK' ) {
            res = res.response.data;
            angular.forEach(res.object, parseEmployeesModel);

            defer.resolve( res );

          } else {
            //console.log( res );
            defer.reject();
          }
        })
        .error(function ( reason, status ) {
          //console.log( reason );
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason );
        });

      return defer.promise;
    }

    //parse each employee model and insert custom properties
    function parseEmployeesModel( model, index ) {

      //for serial number
      model.serial_number = index;

      //for Name column
      model.customName = setName(model);

      //for Address column
      model.customAddress = pipelineFactory.setAddress(model.TbAddress);

      //for Phone column
      model.customPhone = pipelineFactory.setPhone(model.TbPhone);

      //for Email column
      model.customEmail = pipelineFactory.setEmail(model.TbEmail);

      //for Employee-info column
      model.customEmployeeInfo = setEmployeeInfo(model);

      //returns the formatted model
      return model;

      //formats the value for the Name column
      function setName( model ) {
        //TODO need confirmation for this field
        return model.TbUser[0] ? model.TbUser[0].full_name : '-'
      }

      //formats the value for the Employee-nfo column
      function setEmployeeInfo( model ) {
        return '<strong>' + utilFactory.translateFieldType(model.TbEmployee.type, 'employee') + '</strong>' +
          pipelineFactory.breakValue('Created: ' + pipelineFactory.formatDate(model.TbEmployee.created_date) ) +
          pipelineFactory.breakValue(model.TbEmployee.is_manager == 1 ? '( Manager )' : '');
      }
    }

    //disables selected employee's profile
    function disableEmployeeProfile( payload ) {
      var defer, request;

      defer = $q.defer();
      request = JSON.stringify( payload );

      $http.post( appConstant.baseURL + 'webservices/disable_employee.json', request)
        .success(function ( res ) {
          if ( res.response.status === 'OK' ) {
            defer.resolve('employee has been disabled successfully.');
          } else {
            defer.reject( res.response.data.length ? res.response.data.join(', ').toLowerCase() : 'error occurred.' );
          }
        })
        .error(function ( reason, status ) {
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason );
        });

      return defer.promise;
    }
  }

})();
