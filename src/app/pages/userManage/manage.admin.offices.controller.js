/**
 * Created by Shahzad on 8/01/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('ManageAdminOfficesCtrl', ManageAdminOfficesCtrl );

  ManageAdminOfficesCtrl.$inject = ['$scope', '$controller', '$timeout', 'manageOfficesFactory'];

  function ManageAdminOfficesCtrl( $scope, $controller, $timeout, manageOfficesFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    /*VM functions*/
    $scope.openSwitchUsersModal = openSwitchUsersModal;
    $scope.showOfficesPipeline = showOfficesPipeline;
    $scope.submitDisableOfficeForm = submitDisableOfficeForm;
    $scope.resetDisableOfficeForm = resetDisableOfficeForm;
    $scope.submitOfficeForm = submitOfficeForm;
    $scope.resetOfficeForm = resetOfficeForm;

    /*VM properties*/
    $scope.disableOfficeFormObj = { status: {} };
    $scope.officeFormObj = { status: {}, handlers: {} };

    //for offices pipeline
    $scope.pipelineObj = {
      data: {},
      status: {},
      config: {
        selectOne: false,
        type: 'offices',
        addRecord: {
          title: 'Add New Office',
          handler: showAddUpdateOffice
        },
        advanceSearch: {
          searchObj: {},
          useAdvanceSearch: true,
          searchHandler: showOfficeAdvanceSearch,
          resetHandler: null
        },
        actionsAllowedCheck: true,
        actions: [{
          title: 'Edit',
          handler: showAddUpdateOffice
        }],
        columns: [{
          value : 'customName',
          title : 'Name'
        }, {
          value : 'customAddress',
          title : 'Address'
        }, {
          value : 'customPhone',
          title : 'Phone'
        }, {
          value : 'customEmail',
          title : 'Email'
        }, {
          value : 'customOfficeInfo',
          title : 'Office Info'
        }]
      }
    };

    /*initialize*/
    setAddModeOfficeForm();

    /*Functions declarations*/

    //used to handle advance search for offices pipeline
    function showOfficeAdvanceSearch() {
      manageOfficesFactory.openOfficeAdvanceSearchModal({
        searchObj: $scope.pipelineObj.config.advanceSearch.searchObj,
        pipelineObj: $scope.pipelineObj
      });
    }

    //update office profile in offices pipeline
    function updateOfficePipeline(office) {
      manageOfficesFactory.AddUpdateOfficeInPipeline( $scope.disableOfficeFormObj.serial_number, office, $scope );
    }

    //setups the add/update office view
    function showAddUpdateOffice( office ) {

      //sets object for creating office form
      office ? setEditModeOfficeForm(office) : setAddModeOfficeForm();

      //uses flip-box directive to show and hide flip node with selected indexes,
      $scope.flip(1, 0);
    }

    //get back to offices pipeline view
    function showOfficesPipeline() {

      //uses data-office-form="" directive, which gives resetHandler to reset form from outside.
      resetOfficeForm();

      //resets disable office form.
      resetDisableOfficeForm();

      //uses data-flip-box="" directive to show and hide flip node with selected indexes,
      $scope.flip(0, 1);
    }

    //sets properties to add a new office using officeForm directive
    function setAddModeOfficeForm() {

      $scope.officeFormObj.office = getEmptyOffice();
      $scope.officeFormObj.config = {
        page: 'manageOffice',
        controlsOnRight: false,
        editOffice: false,
        onSuccess: updateOfficePipeline,
        hideControls: [],
        submitText: 'Add office',
        successMessage: 'Office has been added successfully.'
      };

      $scope.disableOfficeFormObj.status = {};
      $scope.disableOfficeFormObj.serial_number = undefined;
      $scope.disableOfficeFormObj.data = {};
    }

    //sets properties to edit existing office using officeForm directive
    function setEditModeOfficeForm( office ) {
      $scope.officeFormObj.office = office;
      $scope.officeFormObj.config = {
        page: 'manageOffice',
        controlsOnRight: false,
        editOffice: true,
        onSuccess: updateOfficePipeline,
        hideControls: [],
        submitText: 'Update office',
        successMessage: 'Office\'s profile has been updated successfully.'
      };

      $scope.disableOfficeFormObj.status = {};
      $scope.disableOfficeFormObj.serial_number = office.serial_number;
      $scope.disableOfficeFormObj.data = {
        TbOffice: {
          office_id: office.TbOffice.office_id
        }
      };
    }

    //disables/removes/deletes selected office's profile
    function submitDisableOfficeForm() {

      resetDisableOfficeForm(null, null, true, false);
      $scope.disableOfficeFormObj.status = { sending: true };

      manageOfficesFactory.disableOfficeProfile( $scope.disableOfficeFormObj.data )
        .then(function( res ) {
          resetDisableOfficeForm('success', res, true, false);
          //deletes office from pipeline
          manageOfficesFactory.removeOfficeFromPipeline( $scope.disableOfficeFormObj.serial_number, $scope );

          $timeout(function(){
            showOfficesPipeline();
          }, 2000);
        }, function( reason ) {
          resetDisableOfficeForm('error', reason.message, true, reason.usersAssociated);
        });
    }

    //creates or updates an office.
    function submitOfficeForm() {

      //handle form submission with invalid data
      if ( $scope.officeForm.$invalid ) {
        resetOfficeFormStatus('error', 'please correct the red marked fields first.');
        return;
      }

      //handle if user submits form with no changes
      if ( !$scope.officeForm.$dirty ) {
        resetOfficeFormStatus('error', 'No changes made to save.');
        return;
      }

      //resets previous attempt
      resetOfficeFormStatus();
      $scope.officeFormObj.status.sending = true;

      manageOfficesFactory.addUpdateOffice( $scope.disableOfficeFormObj.serial_number, $scope.officeFormObj.office, $scope )
        .then(function(res) {
          resetOfficeForm('success', res.message);
          //$scope.officeForm.$setPristine();
          //setEditModeOfficeForm(res.office);
        }, function(reason){
          resetOfficeFormStatus('error', reason);
        });
    }

    //resets office status form's status object without breaking reference.
    function resetDisableOfficeForm(type, message, disableCheck, usersAssociated) {
      $scope.disableOfficeFormObj.status.sending = false;
      $scope.disableOfficeFormObj.status.type = type;
      $scope.disableOfficeFormObj.status.message = message;

      $scope.disableOfficeFormObj.disableCheck = !!disableCheck;
      $scope.disableOfficeFormObj.usersAssociated = !!usersAssociated;
    }

    //resets office form's status object without breaking reference.
    function resetOfficeFormStatus(type, message) {
      $scope.officeFormObj.status.sending = false;
      $scope.officeFormObj.status.type = type;
      $scope.officeFormObj.status.message = message;
    }

    //resets office form
    function resetOfficeForm(type, message) {
      $scope.officeFormObj.data = angular.copy( $scope.officeFormObj.office );

      //$scope.officeFormObj.status = {};
      resetOfficeFormStatus(type, message);

      //resets form validation and interaction state
      $scope.officeFormObj.handlers.resetForm();
    }

    //to open a switch users modal when disable office action failed
    function openSwitchUsersModal(){
      manageOfficesFactory.openSwitchUsersModal($scope.officeFormObj.office, resetDisableOfficeForm);
    }

    //creates an empty object for office form - HACK angular-digest.
    function getEmptyOffice() {
      return {
        TbOffice: {},
        TbEmail: [{
          email: '',
          type: ''
        }],
        TbPhone: [{
          phone: '',
          type: ''
        }],
        TbAddress: [{}]
      }
    }
  }

})();
