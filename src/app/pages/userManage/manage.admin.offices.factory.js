/**
 * Created by Shahzad on 8/12/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .service('manageOfficesFactory', manageOfficesFactory );

  manageOfficesFactory.$inject = [
    '$q',
    '$http',
    '$modal',
    'appConstant',
    'pipelineFactory',
    'manageUsersFactory',
    'utilFactory',
    '$localStorage'
  ];

  function manageOfficesFactory( $q, $http, $modal, appConstant, pipelineFactory, manageUsersFactory, utilFactory, $localStorage ) {

    return {
      addUpdateOffice: addUpdateOffice,
      AddUpdateOfficeInPipeline: AddUpdateOfficeInPipeline,
      removeOfficeFromPipeline: removeOfficeFromPipeline,
      disableOfficeProfile: disableOfficeProfile,
      searchOffice : searchOffice,
      getOffice: getOffice,
      assignUserToOffice: assignUserToOffice,
      isOfficeAllowed: isOfficeAllowed,
      listOffices: listOffices,
      fetchOfficesDropDown: fetchOfficesDropDown,
      fetchOfficesPipeline: fetchOfficesPipeline,
      parseOfficeModel: parseOfficeModel,
      fetchOfficeUsersPipeline: fetchOfficeUsersPipeline,
      switchOffice: switchOffice,
      openSwitchUsersModal: openSwitchUsersModal,
      openOfficeAdvanceSearchModal: openOfficeAdvanceSearchModal
    };

    // to assign any user to an office
    function assignUserToOffice(payload) {
      var defer, request;

      defer = $q.defer();
      request = JSON.stringify( payload );

      $http.post( appConstant.baseURL + 'webservices/assign_user_to_office.json', request)
        .success(function ( res ) {
          res = res.response;

          if ( res.status === 'OK' ) {
            defer.resolve({msg: 'User has been assigned to selected Office successfully.'});
          } else if ( res.data.stop == 1 ) {
            res.data.msg += '. Please confirm that you want to re assign to another Office.';
            defer.reject(res.data);
          } else {
            defer.reject({ msg: 'error occurred.'});
          }
        })
        .error(function ( reason, status ) {
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject({ msg: reason});
        });

      return defer.promise;
    }

    //Adds a new office on server and then in local pipeline.
    function addUpdateOffice(serial_number, payload, $scope) {
      var defer, request, endpoint;

      defer = $q.defer();
      request = JSON.stringify( payload );
      endpoint = serial_number >= 0 ? 'edit_office.json' : 'add_office.json';

      $http.post( appConstant.baseURL + 'webservices/' + endpoint, request)
        .success(function ( res ) {
          res = res.response;

          if ( res.status === 'OK' ) {
            defer.resolve({
              info: res.data.object,
              message: 'Office has been '+  (serial_number >= 0 ? 'updated' : 'created') +' successfully.'
            });

            AddUpdateOfficeInPipeline(serial_number, res.data.object, $scope);
          } else {
            defer.reject( res.data.length ? res.data.join(', ').toLowerCase() : 'error occurred.' );
          }
        })
        .error(function ( reason, status ) {
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject(reason);
        });

      return defer.promise;
    }

    //adds or updates a user profile in user pipeline
    function AddUpdateOfficeInPipeline( serial_number, office, scope ) {
      var pipelineOffice, index;

      if ( serial_number ) {
        //parse the model before adding/updating in pipeline
        parseOfficeModel(office, serial_number);

        pipelineOffice = utilFactory.findWhereArrayObj('serial_number', serial_number, scope.pipelineObj.data.records);
        index = scope.pipelineObj.data.records.indexOf(pipelineOffice);

        angular.extend(scope.pipelineObj.data.records[index], office);
      } else {
        //parse the model before adding/updating in pipeline
        parseOfficeModel(office, scope.pipelineObj.data.records + 1);

        scope.pipelineObj.data.records.push( office );
      }
    }

    //Search Office in Office pipeline.
    function searchOffice(options) {
      var defer, request;

      defer = $q.defer();
      request = JSON.stringify( options.payload );

      $http.post( appConstant.baseURL + 'webservices/search_office.json', request)
        .success(function ( res ) {
          res = res.response;
          if ( res.status === 'OK' ) {
            defer.resolve({
              info: res.data.object,
              message: res.data.object.length + ' Offices matched.'
            });
          } else {
            defer.reject('No offices matched.');
          }
        })
        .error(function ( reason, status ) {
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason);
        });

      return defer.promise;
    }

    //removes a user from user pipeline
    function removeOfficeFromPipeline( serial_number, scope ){
      var user, index;

      user = utilFactory.findWhereArrayObj('serial_number', serial_number, scope.pipelineObj.data.records);
      index = scope.pipelineObj.data.records.indexOf(user);

      scope.pipelineObj.data.records.splice(index, 1);
    }

    //get all offices visa list_office.json
    function listOffices( options ) {
      var defer;

      defer = $q.defer();
      $http.get( appConstant.baseURL + 'webservices/list_offices/page:'+ options.pageNum + '.json')
        .success(function ( res ) {

          if ( res && res.response.status === 'OK' ) {
            res = res.response.data;

            //console.log('offices pipeline fetched successfully.');
            defer.resolve( res );

          } else {
            //console.log('fetching offices pipeline failed.');
            //console.log( res );
            defer.reject();
          }
        })
        .error(function ( reason, status ) {
          //console.log('fetching offices pipeline  failed');
          //console.log( reason );
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason );
        });

      return defer.promise;
    }

    //extends listOffices() to fetch offices and parse for dropdown view
    function fetchOfficesDropDown(options){

      return listOffices(options)
        .then(function(res){
          angular.forEach(res.object, function(res, index){
            res.customFullName =  res.TbOffice.corporate_name
          });

          res.object.unshift({
            customFullName: 'leave blank to be assigned one',
            TbOffice: { office_id: 'none', type: 'none'}
          });

          //pass modified response to next then in promise chain
          return res.object;
        });
    }

    //extends listOffices() to fetch offices and parse for pipeline view
    function fetchOfficesPipeline( options ) {
      var skipRecordIndex;

      return listOffices(options)
        .then(function(res){

          if ( options.skipRecord ) {
            angular.forEach(res.object, checkSkipThenParse);
            res.object.splice(skipRecordIndex, 1);
          } else {
            angular.forEach(res.object, parseOfficeModel);
          }

          //pass modified response to next then in promise chain
          return res;
        });

      // store the index of the record that need to be eliminated from records.
      function checkSkipThenParse( record, index ) {
        if ( record.TbOffice.office_id === options.skipRecord ) {
          skipRecordIndex = index;
          return ;
        }

        parseOfficeModel( record, index );
      }
    }

    //get a list of users in an office
    function fetchOfficeUsersPipeline( options ) {
      var defer;

      defer = $q.defer();

      $http.get( appConstant.baseURL + 'webservices/list_office_users/'+ options.officeId +'/page:'+ options.pageNum + '.json')
        .success(function ( res ) {
          if ( res && res.response.status === 'OK' ) {
            res = res.response.data;

            angular.forEach(res.object, manageUsersFactory.parseUserModel);

            defer.resolve( res );

          } else {
            //console.log('fetching offices pipeline failed.');
            //console.log( res );
            defer.reject();
          }
        })
        .error(function ( reason, status ) {
          console.log( reason );
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason );
        });

      return defer.promise;
    }

    //parse each user model and insert custom properties
    function parseOfficeModel( model, index ) {

      //for serial number
      model.serial_number = index;

      //for Name column
      model.customName = model.TbOffice.corporate_name || '-';

      //for Address column
      model.customAddress = pipelineFactory.setAddress(model.TbAddress);

      //for Phone column
      model.customPhone = pipelineFactory.setPhone(model.TbPhone);

      //for Email column
      model.customEmail = pipelineFactory.setEmail(model.TbEmail);

      //for office Info column
      model.customOfficeInfo = setOfficeInfo(model);

      //For allow actions
      model.customAllowAction = true;

      //returns the formatted model
      return model;

      //formats the value for Office-info column
      function setOfficeInfo( model ) {
        return model.TbOffice.dba +
          pipelineFactory.breakValue('License Number: ' + model.TbOffice.lic_number) +
          pipelineFactory.breakValue( 'Created: ' + pipelineFactory.formatDate(model.TbOffice.created_date) );
      }
    }

    //disables selected user's profile
    function disableOfficeProfile( payload ) {
      var defer, request;

      defer = $q.defer();
      request = JSON.stringify( payload );

      $http.post( appConstant.baseURL + 'webservices/disable_office.json', request)
        .success(function ( res ) {
          if ( res.response.status === 'OK' ) {
            defer.resolve('office has been disabled successfully.');
          } else if ( res.response.data[0] === 'This office still has users assoc with object.' ) {
            defer.reject({
              usersAssociated: true,
              message: 'This office has users associated. <br> Please click "SWITCH USERS" to switch these users to a new office.'
            });
          } else {
            defer.reject({
              usersAssociated: false,
              message: 'error occurred.'
            });
          }
        })
        .error(function ( reason, status ) {
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject({
            usersAssociated: false,
            message: reason
          });
        });

      return defer.promise;
    }

    // to get an office from office_id
    function getOffice( officeId ) {
      var defer, request;

      defer = $q.defer();
      request = JSON.stringify({ office_id: officeId });

      $http.post( appConstant.baseURL + 'webservices/get_office.json', request)
        .success(function ( res ) {
          if ( res ) {
            defer.resolve( res.response.data.object );
          } else {
            defer.reject( 'error occurred.' );
          }
        })
        .error(function ( reason, status ) {
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason );
        });

      return defer.promise;
    }

    //switch user in new Office
    function switchOffice( payload ){
      var defer, request;

      defer = $q.defer();
      request = JSON.stringify( payload );

      $http.post( appConstant.baseURL + 'webservices/switch_office.json', request)
        .success(function( res ){
          if ( res.response.status === 'OK' ) {
            defer.resolve('All users have been successfully switched to the new office.');
          } else {
            defer.reject('error occurred.');
          }
        })
        .error(function ( reason ) {
          defer.reject( reason );
        });

      return defer.promise;
    }

    // all offices functionality if according to signed-in user type.
    function isOfficeAllowed(){
      var allowed;

      // TODO create type encryption
      allowed = $localStorage.user.TbUser.type === '5' || $localStorage.user.TbUser.type === '4' ;

      return allowed;
    }

    //opens switch users modal to switch all users of current office to a new office.
    function openSwitchUsersModal(office, onSuccess) {
      //console.log(currentOffice);
      return $modal.open({
        controller: 'ManageAdminOfficesSwitchUsersModalCtrl',
        templateUrl: 'app/pages/userManage/manage.admin.offices.switchUsersModal.view.html',
        size: 'lg',
        windowClass: 'switch-users generate-report',
        resolve: {
          configObj: function() {
            return {
              currentOffice: office,
              onSuccess: onSuccess
            };
          }
        }
      });
    }

    //opens switch users modal to switch all users of current office to a new office.
    function openOfficeAdvanceSearchModal( configObj ) {
      //console.log(configObj);
      return $modal.open({
        controller: 'ManageAdminOfficeSearchOfficeModalCtrl',
        templateUrl: 'app/pages/userManage/manage.admin.offices.searchOfficeModal.view.html',
        size: 'sm',
        windowClass: 'search-office generate-report',
        resolve: {
          configObj: function() {
            return configObj;
          }
        }
      });
    }
  }

})();
