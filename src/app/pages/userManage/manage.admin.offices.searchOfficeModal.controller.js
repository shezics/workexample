/**
 * Created by Shahzad on 6/27/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('ManageAdminOfficeSearchOfficeModalCtrl', ManageAdminOfficeSearchOfficeModalCtrl );

  ManageAdminOfficeSearchOfficeModalCtrl
    .$inject = ['$scope', '$controller', '$timeout', 'configObj', 'manageFactory', 'manageOfficesFactory'];

  function ManageAdminOfficeSearchOfficeModalCtrl( $scope, $controller, $timeout, configObj, manageFactory, manageOfficeFactory ) {

    /*Extend BaseModalCtrl*/
    $controller('BaseModalCtrl', {$scope: $scope});

    /*VM functions*/
    //...

    /*VM props*/
    //...

    /*initialize Stuff*/
    $scope.officeFormStatusObj = {
      status: {}
    };

    //inherited from BaseModalCtrl.to prevent modal from closing, while modal is in sending status.
    $scope.$listenFormStatus( $scope.officeFormStatusObj );

    //$scope.searchUserObj = $scope.searchObj || {};
    $scope.officeFormObj = {
      handlers: {},
      office: configObj.searchObj,
      config: {
        page: 'search-office',
        controlsOnRight: false,
        editOffice: false,
        noRequired: true,
        updateOriginalModal: true,
        onSuccess: updatePipeline,
        onReset: resetSearch,
        status: $scope.officeFormStatusObj.status,
        hideControls: [],
        submitText: 'Search Office',
        successMessage: null
      }
    };

    /*private variables*/
    //...

    /*functions declarations*/


    //clears user model for search form
    function resetSearch() {
      //clears the search object without breaking reference.
      manageFactory.deleteAllPropsFromObj( $scope.officeFormObj.office );
    }

    //Updates pipeline records with the new matching ones.
    function updatePipeline( matchingRecords ) {

      angular.forEach(matchingRecords, manageOfficeFactory.parseOfficeModel);
      configObj.pipelineObj.data.records = matchingRecords;

      $timeout($scope.$close, 2000);
    }

  }

})();
