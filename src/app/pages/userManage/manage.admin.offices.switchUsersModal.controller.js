/**
 * Created by Shahzad on 6/27/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('ManageAdminOfficesSwitchUsersModalCtrl', ManageAdminOfficesSwitchUsersModalCtrl );

  ManageAdminOfficesSwitchUsersModalCtrl.$inject = [
    '$scope',
    '$controller',
    '$timeout',
    '$rootScope',
    'configObj',
    'manageOfficesFactory'

  ];

  function ManageAdminOfficesSwitchUsersModalCtrl( $scope, $controller, $timeout, $rootScope, configObj, manageOfficesFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseModalCtrl', {$scope: $scope});

    /*private variables*/
    //...

    /*VM functions*/
    $scope.switchOffice = switchOffice;
    $scope.changeTab = changeTab;

    /*VM props*/
    $scope.activeTabIndex = 0;
    $scope.currentOffice = configObj.currentOffice;
    $scope.switchOfficeObj = { status: {} };

    //for office-users pipeline tab
    $scope.usersPipelineObj = {
      data: {},
      status: {},
      config: {
        type: 'office-users',
        apiPropsObj: { officeId:  $scope.currentOffice.TbOffice.office_id },
        selectRecord: 'multiple',
        columns: [{
          value : 'customName',
          title : 'Name'
        }, {
          value : 'customAddress',
          title : 'Address'
        }, {
          value : 'customPhone',
          title : 'Phone'
        }, {
          value : 'customEmail',
          title : 'Email'
        }, {
          value : 'customOfficeInfo',
          title : 'User Info'
        }]
      }
    };

    //for office pipeline tab
    $scope.officesPipelineObj = {
      data: {},
      status: {},
      config: {
        type: 'offices',
        selectRecord: 'one',
        skipRecord: $scope.currentOffice.TbOffice.office_id,
        columns: [{
          value : 'customName',
          title : 'Name'
        }, {
          value : 'customAddress',
          title : 'Address'
        }, {
          value : 'customPhone',
          title : 'Phone'
        }, {
          value : 'customEmail',
          title : 'Email'
        }, {
          value : 'customOfficeInfo',
          title : 'Office Info'
        }]
      }
    };

    /*initialization*/
    //inherited from BaseModalCtrl.to prevent modal from closing, while modal is in sending status.
    $scope.$listenFormStatus( $scope.switchOfficeObj );

    //to change a tab
    function changeTab( index ){
      $scope.activeTabIndex = index;
    }

    //submit handler for association
    function switchOffice() {
      var payload;

      payload = {
        TbOffice: {
          old_office_id : $scope.currentOffice.TbOffice.office_id,
          new_office_id  : $scope.officesPipelineObj.data.selectedRecord.TbOffice.office_id
        }
      };

      $scope.switchOfficeObj.status.sending = true;

      manageOfficesFactory.switchOffice( payload )
        .then(function( res ) {
          configObj.onSuccess();
          resetStatusForm('success', res);

          $timeout(function() {
            $scope.$close();
          }, 2000);

        }, function( reason ) {
          resetStatusForm('error', reason.message );
        });
    }

    //resets status properties without breaking reference
    function resetStatusForm( type, message ) {
      $scope.switchOfficeObj.status.sending = false;
      $scope.switchOfficeObj.status.type = type;
      $scope.switchOfficeObj.status.message = message;
    }

  }

})();
