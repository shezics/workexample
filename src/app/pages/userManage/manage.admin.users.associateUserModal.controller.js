/**
 * Created by Shahzad on 6/27/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('ManageAdminUsersAssocUserModalCtrl', ManageAdminUsersAssocUserModalCtrl );

  ManageAdminUsersAssocUserModalCtrl.$inject = [
    '$scope',
    '$controller',
    '$timeout',
    'configObj',
    'userFactory',
    'manageUsersFactory',
    'manageOfficesFactory'
  ];

  function ManageAdminUsersAssocUserModalCtrl( $scope, $controller, $timeout, configObj, userFactory, manageUsersFactory, manageOfficesFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseModalCtrl', {$scope: $scope});

    /*private variables*/
    //...

    /*VM functions*/
    $scope.associateUser = associateUser;
    $scope.resetForm = resetForm;

    /*VM props*/
    $scope.selectOnly = configObj.selectOnly;
    $scope.association = configObj.association;
    $scope.pipelineUserObj = configObj.pipelineUserObj;

    $scope.confirmObj = { needConfirm: false};
    $scope.status = {};
    $scope.currentAssociation = 'Fetching...';

    //HACK for the logic prevent model from closing
    $scope.associateObj = { status: $scope.status };
    $scope.pipelineObj = {
      data: {},
      status: {},
      config: {
        type: configObj.association === 'Office' ? 'offices' : 'reps',
        selectRecord: 'one',
        skipRecord:  getSkipRecord(),
        columns: [{
          value : 'customName',
          title : 'Name'
        }, {
          value : 'customAddress',
          title : 'Address'
        }, {
          value : 'customPhone',
          title : 'Phone'
        }, {
          value : 'customEmail',
          title : 'Email'
        }, {
          value : configObj.association === 'Office' ? 'customOfficeInfo' : 'customRepsInfo',
          title : configObj.association === 'Office' ? 'Office Info' : 'Reps Info'
        }]
        //, actions: [{
        //  option: 'Edit',
        //  handler: recordEditHandler
        //}]
      }
    };

    /*initialization*/
    //inherited from BaseModalCtrl.to prevent modal from closing, while modal is in sending status.
    $scope.$listenFormStatus( $scope.associateObj );

    //get association to show current association of user
    getCurrentAssoc();

    //to get current association of selected user from pipeline.
    function getCurrentAssoc() {
      var userCol;

      userCol = configObj.pipelineUserObj ? configObj.pipelineUserObj.user_col : configObj.userObj.TbUser;

      if ( configObj.association === 'Office' ) {
        if ( !userCol.office_id || userCol.office_id === '0' ) {
          $scope.currentAssociation = 'Un Assigned';
          return;
        }

        manageOfficesFactory.getOffice( userCol.office_id )
          .then(function( res ) {
            $scope.currentAssociation = res.TbOffice.corporate_name;
          }, function( reason ) {
            $scope.currentAssociation = 'Fetching error occurred';
          });

      } else {
        if ( !userCol.parent_user_id || userCol.parent_user_id === '0' ) {
          $scope.currentAssociation = 'Un Assigned';
          return;
        }

        manageUsersFactory.loadUserProfile( userCol.parent_user_id )
          .then(function( res ) {
            $scope.currentAssociation = res.TbUser.first_name + ' ' + res.TbUser.last_name;
          }, function( reason ) {
            $scope.currentAssociation = 'Fetching error occurred';
          });
      }
    }

    //submit handler for association
    function associateUser() {
      var options, promise;


      //if assoc modal is opened to select-only instead of actually assigning to office/rep
      if ( configObj.selectOnly ) {
        setAssocAndClose();
        return;
      }

      options = {
        user_id: configObj.pipelineUserObj.user_col.users_id,
        confirm: $scope.confirmObj.needConfirm && $scope.confirmObj.confirmCheck ? 1 : 0
      };

      resetStatusForm();
      $scope.status.sending = true;

      if ( configObj.association === 'Office' ) {

        //adds parent office for payload and request
        options.parent_office_id =  $scope.pipelineObj.data.selectedRecord.TbOffice.office_id;
        promise = manageOfficesFactory.assignUserToOffice(options);

      } else {
        //adds parent rep for payload and request
        options.parent_user_id = $scope.pipelineObj.data.selectedRecord.TbUser.users_id;
        promise = userFactory.assignUserToRep(options);
      }

      promise.then(function(res){
        resetStatusForm('success', res.msg);

        //clears confirm state
        $scope.confirmObj = {};

        //auto close the associate user modal
        $timeout(function() {
          setAssocAndClose();
        }, 2500);
      }, function(reason){

        resetStatusForm('error', reason.msg);

        //if assign-to-rep warning raised from server. now will need to pass confirm: 1
        if ( reason.stop == 1 ) {
          $scope.confirmObj.needConfirm = true;
        }
      });
    }

    //sets association object for add/edit user form
    function setAssocAndClose(){
      var selectedRecord, assocObj;

      selectedRecord = $scope.pipelineObj.data.selectedRecord;

      if (configObj.association === 'Office') {
        assocObj = {
          id: selectedRecord.TbOffice.office_id,
          name: selectedRecord.TbOffice.corporate_name
        };
      } else {
        assocObj = {
          id: selectedRecord.TbUser.users_id,
          name: selectedRecord.TbUser.first_name + ' ' +  selectedRecord.TbUser.last_name
        };
      }

      configObj.onSuccess( configObj.pipelineUserObj, configObj.userObj, assocObj );
      $scope.$close();
    }

    //resets any previous attempt.
    function resetForm() {
      resetStatusForm();
      $scope.pipelineObj.data.selectedRecord = undefined;
      $scope.confirmObj = {};
    }

    //resets status properties without breaking reference
    function resetStatusForm( type, message ) {
      $scope.status.sending = false;
      $scope.status.type = type;
      $scope.status.message = message;
    }

    //checks for current association. if need to skip record to hide any record.
    function getSkipRecord(){
      var skipRecord;

      if ( configObj.association === 'Office' ){
        skipRecord = configObj.pipelineUserObj ?
          configObj.pipelineUserObj.user_col.office_id : configObj.userObj.TbUser.office_id || null;
      } else {
        skipRecord = configObj.pipelineUserObj ?
          configObj.pipelineUserObj.user_col.parent_user_id : configObj.userObj.TbUser.parent_user_id || null;
      }

      return skipRecord;
    }
  }

})();
