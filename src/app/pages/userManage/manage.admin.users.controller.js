/**
 * Created by Shahzad on 8/01/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('ManageAdminUsersCtrl', ManageAdminUsersCtrl );

  ManageAdminUsersCtrl.$inject = [
    '$scope',
    '$controller',
    '$timeout',
    '$q',
    'userFactory',
    'manageUsersFactory',
    'manageOfficesFactory'
  ];

  function ManageAdminUsersCtrl( $scope, $controller, $timeout, $q, userFactory, manageUsersFactory, manageOfficesFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    /*VM functions*/
     $scope.showUserDetails = manageUsersFactory.showUserDetails;
     $scope.openRunReportModal = manageUsersFactory.openRunReportModal;
     $scope.openSearchUserModal = openSearchUserModal;
     $scope.showAddUpdateUser = showAddUpdateUser;
     $scope.showUsersPipeline = showUsersPipeline;
     $scope.disableUser = disableUser;
     $scope.resetUserStatusForm = resetUserStatusForm;
     $scope.associateToOffice = associateToOffice;
     $scope.associateToRep = associateToRep;

    /*VM properties*/
    $scope.userStatusFormObj = {};
    $scope.userFormObj = { handlers: {}, status: {} };

    //for users pipeline
    $scope.pipelineObj = {
      data: {},
      status: {},
      config: {
        selectRecord: null,
        type: 'users',
        addRecord: {
          title: 'Add New User',
          handler: showAddUpdateUser
        },
        advanceSearch: {
          searchObj: userFactory.getEmptyUserModel(),
          useAdvanceSearch: true,
          searchHandler: openSearchUserModal,
          resetHandler: null
        },
        actionsAllowedCheck: false,
        actions: [{
          title: 'Edit',
          handler: showAddUpdateUser
        },{
            title: 'associate to office',
            handler: associateToOffice
        },{
            title: 'associate to rep',
            handler: associateToRep
        }],
        columns: [{
          value : 'customName',
          title : 'Name'
        }, {
          value : 'customAddress',
          title : 'Address'
        }, {
          value : 'customPhone',
          title : 'Phone'
        }, {
          value : 'customEmail',
          title : 'Email'
        }, {
          value : 'customUserInfo',
          title : 'Users Info'
        }]
      }
    };

    /*initialize*/
    setAddModeUserForm();

    /*Functions declarations*/

    //disables/removes/deletes selected user's profile
    function disableUser() {

      $scope.userStatusFormObj.status = {
        sending: true
      };

      manageUsersFactory.disableUserProfile( $scope.userStatusFormObj.data )
        .then(function( res ) {
          $scope.userStatusFormObj.status = {
            type: 'success',
            message: res
          };

          //deletes user
          manageUsersFactory.removeUserFromPipeline( $scope.userStatusFormObj.serial_number, $scope );

          $timeout(function(){
            showUsersPipeline();
          }, 2000);
        }, function( reason ) {
          $scope.userStatusFormObj.status = {
            type: 'error',
            message: reason
          }
        });
    }

    //setups the add/update user view
    function showAddUpdateUser( user ) {

      //sets object for creating user form
      user ? setEditModeUserForm(user) : setAddModeUserForm();

      //uses flip-box directive to show and hide flip node with selected indexes,
      $scope.flip(1, 0);
    }

    //sets properties to add a new user using userForm directive
    function setAddModeUserForm() {
      $scope.userFormObj.user = userFactory.getEmptyUserModel();
      $scope.userFormObj.config = {
        page: 'manageUser',
        status: $scope.userFormObj.status,
        controlsOnRight: false,
        editUser: false,
        onSuccess: updateUsersPipeline,
        hideControls: ['agreeCheck', 'reps', 'offices'],
        submitText: 'Add user',
        successMessage: 'User has been added successfully.',
        associationObj: {
          pipelineUserObj: null,
          associateToRep: associateToRep,
          associateToOffice: associateToOffice
        }
      };

      $scope.userStatusFormObj = {
        disableUserCheck: false,
        serial_number: undefined,
        data: {}
      };
    }

    //sets properties to edit existing user using userForm directive
    function setEditModeUserForm( pipelineUserObj ) {
      var options;

      //$scope.userFormObj.user = {}; // handled below
      $scope.userFormObj.config = {
        page: 'manageUser',
        status: $scope.userFormObj.status,
        controlsOnRight: false,
        editUser: true,
        onSuccess: updateUsersPipeline,
        hideControls: ['username', 'password', 'agreeCheck', 'reps', 'offices'],
        submitText: 'Update user',
        successMessage: 'User\'s profile has been updated successfully.',
        associationObj: {
          pipelineUserObj: pipelineUserObj,
          associateToRep: associateToRep,
          associateToOffice: associateToOffice
        }
      };

      //for disable-user-form
      $scope.userStatusFormObj = {
        disableUserCheck: false,
        serial_number: pipelineUserObj.serial_number,
        data: {
          TbUser: {
            users_id: pipelineUserObj.user_col.users_id
          }
        }
      };

      options = {
        userId: pipelineUserObj.user_col.users_id,
        repId: pipelineUserObj.user_col.parent_user_id,
        officeId: pipelineUserObj.user_col.office_id,
        getOfficeMethod: manageOfficesFactory.getOffice
      };

      //loads selected user's profile
      $scope.userFormObj.fetching = true;
      $scope.userFormObj.config.status.sending = true;

      manageUsersFactory.loadUserWithAssoc(options)
        .then(function( res ) {
          $scope.userFormObj.user = res.userObj;

          fillRepNameField(pipelineUserObj, res.userObj, {
            id: res.rep.TbUser.users_id,
            name: res.rep.TbUser.full_name || (res.rep.TbUser.first_name + ' ' + res.rep.TbUser.last_name)
          });

          fillOfficeNameField(pipelineUserObj, res.userObj, {
            id: res.office.TbOffice.office_id,
            name: res.office.TbOffice.corporate_name
          });

          $scope.userFormObj.fetching = false;
          $scope.userFormObj.config.status.sending = false;
        }, function( reason ) {
          $scope.userFormObj.fetching = false;
          $scope.userFormObj.fetchError = reason;
          $scope.userFormObj.config.status.sending = false;
        });
    }

    //associate to an office
    function associateToOffice( pipelineUserObj, userObj, selectOnly ) {
      manageUsersFactory.openAssociateUserModal({
        association: 'Office',
        pipelineUserObj: pipelineUserObj,
        userObj: userObj,
        selectOnly: selectOnly,
        onSuccess: fillOfficeNameField
      });
    }

    //associate to a rep
    function associateToRep(pipelineUserObj, userObj, selectOnly ) {
      manageUsersFactory.openAssociateUserModal({
        association: 'Rep',
        pipelineUserObj: pipelineUserObj,
        userObj: userObj,
        selectOnly: selectOnly,
        onSuccess: fillRepNameField
      });
    }

    //fills rep name field for add/edit user form
    function fillRepNameField(pipelineUserObj, userObj, repInfo ) {
      //var userObj = $scope.userFormObj.user;

      if ( userObj ) {
        userObj.TbUser.parent_user_id = repInfo.id;

        userObj.TbAssociation = userObj.TbAssociation || {};
        userObj.TbAssociation.parent_user_id = repInfo.id;
        userObj.TbAssociation.parent_user_name = repInfo.name;
      }

      if ( pipelineUserObj ) {
        pipelineUserObj.user_col.parent_user_id = repInfo.id;
        pipelineUserObj.user_col.is_associated = '1';
      }
    }

     //fills office name field for add/edit user form
    function fillOfficeNameField( pipelineUserObj, userObj, officeInfo ) {
      //var userObj = $scope.userFormObj.user;

      if ( userObj ) {
        userObj.TbUser.office_id = officeInfo.id;
        userObj.TbUser.office_name = officeInfo.name;
      }

      if ( pipelineUserObj ) {
        pipelineUserObj.user_col.office_id = officeInfo.id;
      }
    }

    //resets user-status-form or disable-user-form.
    function resetUserStatusForm(){
      $scope.userStatusFormObj.disableUserCheck = false;
    }

    //update user profile in users pipeline
    function updateUsersPipeline(user) {
      manageUsersFactory.AddUpdateUserInPipeline( $scope.userStatusFormObj.serial_number, user, $scope );
    }

    //get back to users pipeline view
    function showUsersPipeline() {

      //uses data-user-form="" directive, which gives resetHandler to reset form from outside.
      $scope.userFormObj.handlers.resetForm();

      //uses data-flip-box="" directive to show and hide flip node with selected indexes,
      $scope.flip(0, 1);
    }

    //opens search model with same scope.
    function openSearchUserModal(){
      manageUsersFactory.openSearchUserModal({
        searchObj: $scope.pipelineObj.config.advanceSearch.searchObj,
        pipelineObj: $scope.pipelineObj
      });
    }

  }

})();
