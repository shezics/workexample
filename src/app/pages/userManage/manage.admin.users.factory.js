/**
 * Created by Shahzad on 6/05/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .service('manageUsersFactory', manageUsersFactory );

  manageUsersFactory.$inject = [
    '$q',
    '$http',
    '$modal',
    'appConstant',
    'pipelineFactory',
    'utilFactory'
  ];

  function manageUsersFactory( $q, $http, $modal, appConstant, pipelineFactory, utilFactory ) {

    return {
      fetchUsersPipeline: fetchUsersPipeline,
      parseUserModel: parseUserModel,
      searchUser: searchUser,
      AddUpdateUserInPipeline: AddUpdateUserInPipeline,
      removeUserFromPipeline: removeUserFromPipeline,
      disableUserProfile: disableUserProfile,
      loadUserProfile: loadUserProfile,
      loadUserWithAssoc: loadUserWithAssoc,
      openRunReportModal: openRunReportModal,
      openSearchUserModal: openSearchUserModal,
      openAssociateUserModal: openAssociateUserModal,
      removePhone: removePhone
    };

    //to load user profile and its associated rep, office profiles, and send back in a single object.
    function loadUserWithAssoc( options ) {
      var defer, promise, promises;

      defer = $q.defer();

      //promises for loading rep, office, and the user
      promises = [];

      //load user profile
      promise = loadUserProfile( options.userId );
      promises.push(promise);

      //load rep
      if ( options.repId == 0 || !options.repId ) {
        promise = $q.when({
          TbUser: { users_id: '0', full_name: 'Un Assigned'}
        });
      } else {
        promise = loadUserProfile( options.repId );
      }
      promises.push(promise);

      //load office
      if ( options.officeId == 0 || !options.officeId ) {
        promise = $q.when({
          TbOffice: {office_id: '0', corporate_name: 'Un Assigned'}
        });
      } else {
        promise = options.getOfficeMethod( options.officeId );
      }

      promises.push(promise);

      $q.all(promises)
        .then(function( responses ) {
          defer.resolve({
            userObj: responses[0],
            rep: responses[1],
            office: responses[2]
          });

        }, function(reasons ) {
          defer.reject('Could not fetch data.');
        });

      return defer.promise;
    }

    //adds or updates a user profile in user pipeline
    function AddUpdateUserInPipeline( serial_number, user, scope ) {
      var options;

      //username
      options = {
        payload: {
          TbUser: user.TbUser
        }
      };

      return searchUser(options)
        .then(addUpdateUser);

      function addUpdateUser( info ) {
        var pipelineUser, index, user;

        user = info.data[0];

        if ( serial_number ) {

          //parse the model before updating in pipeline
          parseUserModel(user, serial_number);

          pipelineUser = utilFactory.findWhereArrayObj('serial_number', serial_number, scope.pipelineObj.data.records);
          index = scope.pipelineObj.data.records.indexOf(pipelineUser);

          angular.extend(scope.pipelineObj.data.records[index], user);
        } else {

          //parse the model before updating in pipeline
          parseUserModel(user, scope.pipelineObj.data.records.length + 1);

          scope.pipelineObj.data.records.push( user );
        }
      }
    }

    //removes a user from user pipeline
    function removeUserFromPipeline( serial_number, scope ){
      var user, index;

      user = utilFactory.findWhereArrayObj('serial_number', serial_number, scope.pipelineObj.data.records);
      index = scope.pipelineObj.data.records.indexOf(user);

      scope.users.splice(index, 1);
    }

    //fetches user records for given page
    function fetchUsersPipeline( options ) {
      var defer;

      defer = $q.defer();
      $http.get( appConstant.baseURL + 'webservices/user_pipeline/page:' + options.pageNum + '/limit:1000.json')
        .success(function ( res ) {
          if ( res ) {
            res.object = res.data;
            angular.forEach(res.object, parseUserModel);

            //console.log('users pipeline fetched successfully.');
            defer.resolve( res );

          } else {
            //console.log('fetching users pipeline failed.');
            //console.log( res );
            defer.reject();
          }
        })
        .error(function ( reason, status ) {
          //console.log('fetching users pipeline  failed');
          //console.log( reason );
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason );
        });

      return defer.promise;
    }

    //parse each user model and insert custom properties
    function parseUserModel( model, index ) {

      //for serial number
      model.serial_number = index;

      //for Name column
      model.customName = setName( model.name_col || model.TbUser );

      //for Address column
      model.customAddress = pipelineFactory.setAddress( model.address_col || model.TbAddress );

      //for Phone column
      model.customPhone = pipelineFactory.setPhone( model.phone_col || model.TbPhone );

      //for Email column
      model.customEmail = pipelineFactory.setEmail( model.email_col || model.TbEmail );

      //for user-info column
      model.customUserInfo = setUserInfo( model.user_col || model.TbUser );

      //For allow actions
      model.customAllowAction = true;

      //returns the formatted model
      return model;

      //formats the value for name column
      function setName( model ) {
        return ( model.full_name || ( model.first_name ? model.first_name + ' ' + model.last_name : '-') ) +
          pipelineFactory.breakValue(model.company_name ? 'Company Name: ' + model.company_name : '') +
          pipelineFactory.breakValue(model.type ? 'Type: ' + getUserType(model) : '') +
          pipelineFactory.breakValue(model.status ? 'Status: ' + utilFactory.translateFieldType(model.status, 'status') : '' );
      }

      //to translate user-type if numbers provided, otherwise return already translated one
      function getUserType(model) {
        if ( typeof(model.type) === "string" && model.type.length > 1 ) {
          return model.type;
        } else {
          return utilFactory.translateFieldType(model.type, 'user');
        }
      }

      //formats the value for user-info column
      function setUserInfo( model ) {
        return ('Last order: ' +
            ( model.last_order && model.last_order !== 'none' ?
              pipelineFactory.formatDate(model.last_order) : 'none')
          ) +
          pipelineFactory.breakValue('Total orders: ' + ( model.total_orders || 0 ) )
      }
    }

    //to show user details in a new page
    function loadUserProfile( userID ) {
      var defer, request;

      defer = $q.defer();
      request = JSON.stringify({
        TbUser: {
          users_id: userID
        }
      });

      $http.post( appConstant.baseURL + 'webservices/load_edit_user.json', request)
        .success(function ( res ) {
          if (  res.response.status === 'OK' ) {
            defer.resolve( res.response.data.object );
          } else {
            defer.reject( res.response.data.length ? res.response.data.join(', ').toLowerCase() : 'no records were found.' );
          }
        })
        .error(function ( reason, status ) {
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason);
        });

      return defer.promise;
    }

    //disables selected user's profile
    function disableUserProfile( payload ) {
      var defer, request;

      defer = $q.defer();
      request = JSON.stringify( payload );

      $http.post( appConstant.baseURL + 'webservices/disable_user.json', request)
        .success(function ( res ) {
          if ( res.response.status === 'OK' ) {
            defer.resolve('user has been disabled successfully.');
          } else {
            defer.reject( res.response.data.length ? res.response.data.join(', ').toLowerCase() : 'error occurred.' );
          }
        })
        .error(function ( reason, status ) {
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason);
        });

      return defer.promise;
    }

    //search users from user form.
    function searchUser( options ) {
      var defer, request;

      defer = $q.defer();
      request = JSON.stringify( options.payload );

      $http.post( appConstant.baseURL + 'webservices/search_user_pipeline.json', request)
        .success(function ( res ) {
          if ( res && res.data.length ) {
            defer.resolve(res);
          } else {
            defer.reject('No records matched.');
          }
        })
        .error(function ( reason, status ) {
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason);
        });

      return defer.promise;
    }

    //to disable or remove a phone from server
    function removePhone(phone) {
      var defer, request;

      defer = $q.defer();
      request = JSON.stringify({
        TbPhone:{
          phone_id : phone.phone_id
        }
      });

      $http.post( appConstant.baseURL + 'webservices/disable_phone.json', request)
        .success(function ( res ) {
          if ( res ) {
            defer.resolve(res);
          } else {
            defer.reject('Error occurred.');
          }
        })
        .error(function ( reason, status ) {
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason);
        });

      return defer.promise;
    }

    //opens run report modal.
    function openRunReportModal() {
      return $modal.open({
        controller: 'ManageAdminUsersRunReportModalCtrl',
        templateUrl: 'app/pages/userManage/manage.admin.users.runReportModal.view.html',
        size: 'sm',
        windowClass: 'generate-report'
      });
    }

    //opens search user modal.
    function openSearchUserModal( configObj ) {
      return $modal.open({
        controller: 'ManageAdminUsersSearchUserModalCtrl',
        templateUrl: 'app/pages/userManage/manage.admin.users.searchUserModal.view.html',
        size: 'sm',
        windowClass: 'search-user generate-report', //TODO: remove generate-report class
        resolve: {
          configObj: function() {
            return configObj;
          }
        }
      });
    }

    //opens associate user modal, to assoc user to office or to rep/employee..
    function openAssociateUserModal( configObj ) {
      return $modal.open({
        controller: 'ManageAdminUsersAssocUserModalCtrl',
        templateUrl: 'app/pages/userManage/manage.admin.users.associateUserModal.view.html',
        size: 'lg',
        windowClass: 'associate-user generate-report',
        resolve: {
          configObj: function() {
            return configObj;
          }
        }
      });
    }
  }

})();
