/**
 * Created by Shahzad on 6/27/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('ManageAdminUsersRunReportModalCtrl', ManageAdminUsersRunReportModalCtrl );

  ManageAdminUsersRunReportModalCtrl.$inject = ['$scope', '$controller', '$modal', 'manageFactory'];

  function ManageAdminUsersRunReportModalCtrl( $scope, $controller, $modal,  manageFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseModalCtrl', {$scope: $scope});

    /*VM functions*/

    /*VM props*/
    $scope.reportSearchObj = {
      status : { }
    };

    /*private variables*/
    //...

    //initialize Stuff
    //inherited from BaseModalCtrl.to prevent modal from closing, while modal is in sending status.
    $scope.$listenFormStatus(  $scope.reportSearchObj );
  }

})();
