/**
 * Created by Shahzad on 6/27/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('ManageAdminUsersSearchUserModalCtrl', ManageAdminUsersSearchUserModalCtrl );

  ManageAdminUsersSearchUserModalCtrl.$inject = ['$scope', '$controller', '$timeout', 'configObj', 'userFactory', 'manageFactory', 'manageUsersFactory'];

  function ManageAdminUsersSearchUserModalCtrl( $scope, $controller, $timeout, configObj, userFactory, manageFactory, manageUsersFactory ) {

    /*Extend BaseModalCtrl*/
    $controller('BaseModalCtrl', {$scope: $scope});

    /*VM functions*/
    //...

    /*VM props*/
    //...

    /*initialize Stuff*/
    $scope.userStatusFormObj = {
      status: {}
    };

    $scope.datePickerObj = {
      options: {},
      fromDateOpened: false,
      toDateOpened: false
    };

    //inherited from BaseModalCtrl.to prevent modal from closing, while modal is in sending status.
    $scope.$listenFormStatus( $scope.userStatusFormObj );

    $scope.TbOrder = {};
    //adds TbOrder Object to searchObj
    angular.extend(configObj.searchObj, $scope.TbOrder );
    //$scope.searchUserObj = $scope.searchObj || {};

    $scope.userFormObj = {
      handlers: {},
      user: configObj.searchObj,
      config: {
        page: 'search-user',
        controlsOnRight: false,
        editUser: false,
        noRequired: true,
        updateOriginalModal: true,
        onSuccess: updatePipeline,
        onReset: resetSearch,
        status: $scope.userStatusFormObj.status,
        hideControls: ['password', 'agreeCheck', 'reps', 'offices'],
        submitText: 'Search',
        successMessage: null
      }
    };

    /*private variables*/
    //...


    /*functions declarations*/

    //clears user model for search form
    function resetSearch() {
      //clears the search object without breaking reference.
      manageFactory.deleteAllPropsFromObj( $scope.userFormObj.user );
      manageFactory.deleteAllPropsFromObj( $scope.TbOrder );

      angular.extend($scope.userFormObj.user, userFactory.getEmptyUserModel(), $scope.TbOrder );
    }

    //Updates pipeline records with the new matching ones.
    function updatePipeline( matchingRecords ) {

      angular.forEach(matchingRecords, manageUsersFactory.parseUserModel);
      configObj.pipelineObj.data.records = matchingRecords;;

      $timeout($scope.$close, 2000);
    }

  }

})();
