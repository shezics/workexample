/**
 * Created by Shahzad on 6/05/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('ManageCtrl', ManageCtrl );

  ManageCtrl.$inject = ['$scope', '$controller', '$state', 'manageFactory'];

  function ManageCtrl( $scope, $controller, $state, manageFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    /*VM functions*/
    $scope.$state = $state;

    /*VM properties*/
    $scope.types = manageFactory.getFieldTypes();

  }

})();
