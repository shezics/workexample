/**
 * Created by Shahzad on 6/05/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .service('manageFactory', manageFactory );

  manageFactory.$inject = [
    '$q',
    '$http',
    'FIELD_TYPES',
    'appConstant',
    'utilFactory',
    'pipelineFactory',
    'authFactory',
    '$modal'
  ];

  function manageFactory( $q, $http, FIELD_TYPES, appConstant, utilFactory, pipelineFactory, authFactory, $modal ) {

    var result;

    var responseMessages = {
      searchMany: 'Records found! Please select a record below to generate report. <br/> OR Hit "Reset" to start a new search.',
      searchOne: 'Record found! Please Hit generate report to generate a report. <br/> OR Hit "Reset" to start a new search.'
    };

    return {
      deleteAllPropsFromObj: utilFactory.deleteAllPropsFromObj,
      getFieldTypes: getFieldTypes,
      autocompleteFips: autocompleteFips,
      searchAddressOwnerParcel: searchAddressOwnerParcel,
      generateReport: generateReport,
      fetchOrdersPipeline: fetchOrdersPipeline,
      parseOrderModel: parseOrderModel,
      updateLoggedInUserObj: updateLoggedInUserObj,
      openShowResultModal: openShowResultModal,
      openOrderAdvanceSearchModal: openOrderAdvanceSearchModal
    };


    //to get field types for all forms in the application
    function getFieldTypes() {
      return FIELD_TYPES;
    }

    //autocomplete fips for given county name
    function autocompleteFips( countyName ) {
      var defer, mappedCounties, mappedCounty;

      defer = $q.defer();

      $http.get( appConstant.baseURL + 'Webservices/autocomplete_fips/' + countyName + '.json')
        .success(function ( res ) {
          if ( res.response.status === 'OK' ) {
            //console.log('county fips fetched successfully.');

            //defer.resolve( res.response.data );

            //maps counties to an array for ui-select
            mappedCounties = [];
            angular.forEach(res.response.data, function( val, key ){
              mappedCounty = {
                fips: key,
                name: val
              };

              mappedCounties.push( mappedCounty );
            });

            defer.resolve( mappedCounties );
          } else {
            //console.log(' fetching county fips failed.');
            //console.log( res );
            defer.reject( res.response.data.length ? res.response.data.join(', ').toLowerCase() : 'no matching records were found.' );
          }
        })
        .error(function ( reason, status ) {
          //console.log('fetching county fips failed');
          //console.log( reason );
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason );
        });

      return defer.promise;
    }

    //to lookup an address/property, owner, or parcel/APN.
    function searchAddressOwnerParcel( searchType, payload ) {
      var defer, request, searchEndpoint;

      defer = $q.defer();

      switch( searchType ) {
        case 'address':
          searchEndpoint = 'search_property.json';
          payload.site_address = payload.site_street_number ? payload.site_street_number + ' ' + payload.site_route : payload.site_route;
          break;
        case 'owner':
          searchEndpoint = 'search_owner_name.json';
          break;
        case 'APN':
          searchEndpoint = 'search_parcel_number.json';
          break;
      }

      request = JSON.stringify( payload );

      $http.post( appConstant.baseURL + 'Webservices/' + searchEndpoint, request )
        .success(function ( res ) {
          if ( res.response.status === 'OK' && res.response.data.length ) {
            //console.log('search records fetched successfully.');

            result = res.response.data;
            defer.resolve({
              results: res.response.data,
              message: responseMessages.searchMany
            });
          } else {
            //console.log(' fetching search records failed.');
            //console.log( res );
            defer.reject( res.response.data.length ? res.response.data.join(', ').toLowerCase() : 'Record not found.' );
          }
        })
        .error(function ( reason, status ) {
          //console.log('fetching  search records failed');
          //console.log( reason );
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason );
        });

      return defer.promise;
    }


    //orders a report for given property
    function generateReport( payload, order ) {
      var defer, request;

      defer = $q.defer();

      payload.state_county_fips = order.state_county_fips;
      payload.sa_property_id = order.SA_PROPERTY_ID;

      request = JSON.stringify( payload );

      $http.post( appConstant.baseURL + 'Webservices/order_report.json', request )
        .success(function ( res ) {
          var reportObj = {};

          //if no error object received
          if ( typeof res !== 'object') {

            if ( payload.output === 'link' ) {
              reportObj.link = res;
            } else {
              reportObj.markup = res;
            }

            defer.resolve( reportObj );
          } else {
            //console.log('order report fetched successfully.', res);
            defer.reject( 'Failed! ' + ( res.response.data[0] || 'some unknown error occurred.' ) );
          }
        })
        .error(function ( reason, status ) {
          //console.log('fetching order report failed');
          //console.log( reason );
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason );
        });

      return defer.promise;
    }

    //fetches orders/reports pipeline for given page.
    function fetchOrdersPipeline( options ) {
      var defer, request;

      defer = $q.defer();
      request = {
        method: options.searchMode ? 'POST' : 'GET',
        url: appConstant.baseURL + 'webservices/order_pipeline/page:' + options.pageNum + '.json',
        data: options.payload ? JSON.stringify(options.payload) : undefined
      };

      $http(request)
        .success(function ( res ) {

          if ( res && res.count ) {
            angular.forEach(res.data, parseOrderModel);
            res.object = res.data;
            defer.resolve( res );

          } else {
            //console.log( res );
            res =  options.searchMode ? 'No records found.' : 'No records matched.';
            defer.reject(res);
          }
        })
        .error(function ( reason, status ) {
          //console.log( reason );
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason );
        });

      return defer.promise;
    }

    //parses each order model and insert custom properties
    function parseOrderModel( model, index ) {

      //for serial number
      model.serial_number = index;

      //for Office Name column
      model.customOfficeName = model.office_col.name;

      //for Rep Name column
      model.customRepName = model.rep_col.name;

      //for Ordered By column
      model.customOrderedBy = '-';

      //for Address column
      model.customAddress = setAddress(model.address_col);

      //for Report Type column
      model.customReportType = setReport(model.report_col);

      //for Report Date column
      model.customReportDate = setDate(model.status_col);

      //for Status column
      model.customStatus = model.status_col.status;

      //for allowing actions for the record
      model.customAllowAction = !!model.status_col.report_fetch_id;

      //returns the modified model
      return model;

      //formats the address field
      function setAddress( model ) {
        return (model.site_address || '') +
          pipelineFactory.breakValue(model.site_city) +
          pipelineFactory.breakValue(model.site_state) +
          pipelineFactory.breakValue(model.site_zip);
      }

      //formats the report field
      function setReport( model ) {
        return !model ? '-' : utilFactory.translateFieldType(model.type.toLowerCase(), 'reports');
      }

      //formats the date field
      function setDate( model ) {
        return !model ? '-' : pipelineFactory.formatDate(model.date_time);
      }
    }

    //on successful profile update, it updates logged in user obj into local storage,
    function updateLoggedInUserObj( userObj ) {

      authFactory.generateAndSaveAuthKey( userObj );
    }

    //opens show result modal.
    function openShowResultModal() {
      return $modal.open({
        controller: 'ManageReportsOrderReportResultModalCtrl',
        templateUrl: 'app/pages/userManage/manage.reports.orderReport.resultModal.html',
        size: 'sm',
        windowClass: 'order-results generate-report',
        resolve: {
          result: function () {
            return result;
          }
        }
      });
    }

    //opens a model to provide advance search for orders
    function openOrderAdvanceSearchModal( configObj ) {
      return $modal.open({
        controller: 'ManageReportsOrderHistorySearchOrderModalCtrl',
        templateUrl: 'app/pages/userManage/manage.reports.orderHistory.searchOrderModal.view.html',
        size: 'md',
        windowClass: 'report-search-order generate-report',
        resolve: {
          configObj : function() {
            return configObj;
          }
        }
      });
    }

  }

})();
