/**
 * Created by Israr Ali 11/6/2015.
 */
(function() {

  'use strict';
  angular
    .module('workExample')
    .service('manageFormFactory', manageFormFactory);

    manageFormFactory.$inject=['$q', '$http', 'appConstant', 'utilFactory'];

    function manageFormFactory($q, $http, appConstant, utilFactory, FIELD_TYPES) {

      return{
        getAllFarms : getAllFarms,
        viewDetails : viewDetails,
        viewOnMap : viewOnMap,
        getLeadFarms : getLeadFarms,
        getFarmDetails : getFarmDetails,
        getFarmsMetaInfo: getFarmsMetaInfo
      };

      //gets all Farms
      function getAllFarms() {
        var defer;

        defer = $q.defer();

        //Todo remove when api available
        defer.resolve( getDummyFarm() );
        return defer.promise;

        $http.get( appConstant.baseURL + 'Webservices/get_all_farms.json')
          .success(function ( res ) {
            if ( res === 'OK' ) {

              defer.resolve( res );
            } else {
              defer.reject( res );
            }
          })
          .error(function ( reason, status ) {
            reason = utilFactory.requestErrorHandler( reason, status );
            defer.reject( reason);
          });

        return defer.promise;
      }

      //gets details for any Farm by farmId.
      function getFarmDetails( farmId ) {
        var defer;

        defer = $q.defer();

        $http.get( appConstant.baseURL + 'Webservices/get_farm/' + farmId +'.json')
          .success(function (res) {
            if ( res ) {
              console.log('farm',res);
              defer.resolve( res );
            } else {
              defer.reject( res );
            }
          })
          .error(function ( reason, status ) {
            reason = utilFactory.requestErrorHandler( reason, status );
            defer.reject( reason);
          });

        return defer.promise;
      }

      //gets Farms MetaInfo.
      function getFarmsMetaInfo() {
        var defer;

        defer = $q.defer();

        $http.get( appConstant.baseURL + 'Webservices/get_farms_metainfo.json')
          .success(function (res) {
            if ( res ) {
              defer.resolve( res );
            } else {
              defer.reject( res );
            }
          })
          .error(function ( reason, status ) {
            reason = utilFactory.requestErrorHandler( reason, status );
            defer.reject( reason);
          });

        return defer.promise;
      }

      // creates dummy data for farms.
      function getDummyFarm() {
        var farms = [];

        for ( var i = 1; i < 6; i++ ) {
          farms.push( {
            id: i,
            status : 'active',
            location : '821 Lenox Ave., Sherman Oaks, CA, 90671',
            capability : 'Single Family Home',
            br: 3,
            ba: 3 ,
            sqft: 3
          });
        }

        return farms;
      }

      function viewDetails() {
        console.log('viewDetails');
      }

      function viewOnMap() {
        console.log('viewOnMap');
      }

      //gets Lead Farms
      function getLeadFarms() {
        var defer;

        defer = $q.defer();

        //Todo remove when api available

        defer.resolve( getDummyLeadFarm() );
        return defer.promise;

        $http.get( appConstant.baseURL + 'Webservices/some_api.json')
          .success(function ( res ) {
            if ( res.response.status === 'OK' ) {

              if ( res.response.data.length <= 10 ) {
                var item = res.response.data[0];

              }

              defer.resolve( res.response );
            } else {
              defer.reject(  res.response.data[0] );
            }
          })
          .error(function ( reason, status ) {
            reason = utilFactory.requestErrorHandler( reason, status );
            defer.reject( reason);
          })

        return defer.promise;
      }

      function getDummyLeadFarm() {
        var leadFarm = [];

        for (var i = 1; i <= 10; i++) {
          leadFarm.push({
            id: i,
            name: 'Saved Farm Number '+i,
            leads: 28 * i % 100,
            createDate: new Date()
          });
        }

        return leadFarm
      }
    }
})();
