/**
 * Created by Shahzad on 6/05/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('ManageReportsCtrl', ManageReportsCtrl );

  ManageReportsCtrl.$inject = ['$scope', '$controller', 'manageFactory'];

  function ManageReportsCtrl( $scope, $controller, manageFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    /*VM functions*/
    //...

    /*VM properties*/
    //...

  }

})();
