/**
 * Created by Shahzad on 6/05/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('ManageReportsOrderHistoryCtrl', ManageReportsOrderHistoryCtrl );

  ManageReportsOrderHistoryCtrl.$inject = ['$scope', '$controller', 'manageFactory'];

  function ManageReportsOrderHistoryCtrl( $scope, $controller, manageFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    /*initialize*/
    //...

    /*VM functions*/
    //...

    /*VM properties*/

    //for orders pipeline
    $scope.pipelineObj = {
      data: {},
      status: {},
      config: {
        selectOne: false,
        type: 'orders',
        addRecord: null,
        advanceSearch: {
          searchObj: {},
          useAdvanceSearch: true,
          searchHandler: showOrderAdvanceSearch,
          resetHandler: null
        },
        actionsAllowedCheck: true,
        actions: [{
          title: 'Download PDF',
          handler: downloadOrder
        }, {
          title: 'Send To Email',
          handler: sendToEmail
        }, {
          title: 'Delete',
          handler: deleteOrder
        }],
        columns: [{
          value: 'customOfficeName',
          title: 'Office Name'
        },{
          value: 'customRepName',
          title: 'Rep Name'
        },{
          value: 'customOrderedBy',
          title: 'Ordered By'
        },{
          value: 'customAddress',
          title: 'Address'
        },{
          value: 'customReportType',
          title: 'Report Type'
        },{
          value: 'customReportDate',
          title: 'Report Date'
        },{
          value: 'customStatus',
          title: 'Status'
        }]
      }
    };

    /*functions declarations*/

    //used to handle advance search for orders history pipeline
    function showOrderAdvanceSearch() {
      manageFactory.openOrderAdvanceSearchModal({
        searchObj: $scope.pipelineObj.config.advanceSearch.searchObj,
        pipelineObj:  $scope.pipelineObj
      });
    }

    //to download selected order
    function downloadOrder( order ) {
      console.log(order);
    }

    //to send the selected order to any email
    function sendToEmail( order ) {
      console.log(order);
    }

    //to remove/delete the selected order
    function deleteOrder( order ) {
      console.log(order);
    }

  }

})();
