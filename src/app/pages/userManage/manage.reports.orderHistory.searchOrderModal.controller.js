/**
 * Created by Shahzad on 6/27/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('ManageReportsOrderHistorySearchOrderModalCtrl', ManageReportsOrderHistorySearchOrderModalCtrl );

  ManageReportsOrderHistorySearchOrderModalCtrl
    .$inject = ['$scope', '$controller', '$timeout', 'configObj', 'FIELD_TYPES', 'manageFactory'];

  function ManageReportsOrderHistorySearchOrderModalCtrl( $scope, $controller, $timeout, configObj, FIELD_TYPES, manageFactory ) {

    /*Extend BaseModalCtrl*/
    $controller('BaseModalCtrl', {$scope: $scope});

    /*VM functions*/
    $scope.submitForm = submitForm;
    $scope.resetForm = resetForm;
    $scope.changeTab = changeTab;

    /*VM props*/
    $scope.types = FIELD_TYPES;

    $scope.searchOrderFormObj = {
      data: configObj.searchObj,
      status: {}
    };
    $scope.activeTabIndex = 0;

    $scope.datePickerObj = {
      options: {},
      fromDateOpened: false,
      toDateOpened: false
    };

    /*initialize Stuff*/
    //inherited from BaseModalCtrl.to prevent modal from closing, while modal is in sending status.
    $scope.$listenFormStatus( $scope.searchOrderFormObj );

    /*private variables*/
    //...
    $scope.usersPipelineObj = {
      data: {},
      status: {},
      config: {
        selectRecord: 'one',
        type: 'users',
        actionsAllowedCheck: false,
        columns: [{
          value : 'customName',
          title : 'Name'
        }, {
          value : 'customAddress',
          title : 'Address'
        }, {
          value : 'customPhone',
          title : 'Phone'
        }, {
          value : 'customEmail',
          title : 'Email'
        }, {
          value : 'customOfficeInfo',
          title : 'Users Info'
        }]
      }
    };

    $scope.officesPipelineObj = {
      data: {},
      status: {},
      config: {
        selectRecord: 'one',
        type: 'offices',
        actionsAllowedCheck: false,
        columns: [{
          value : 'customName',
          title : 'Name'
        }, {
          value : 'customAddress',
          title : 'Address'
        }, {
          value : 'customPhone',
          title : 'Phone'
        }, {
          value : 'customEmail',
          title : 'Email'
        }, {
          value : 'customOfficeInfo',
          title : 'Office Info'
        }]
      }
    };
    /*functions declarations*/

    //clears user model for search form
    function resetForm() {
      manageFactory.deleteAllPropsFromObj( $scope.searchOrderFormObj.data );
      $scope.usersPipelineObj.data.selectedRecord = undefined;
      $scope.officesPipelineObj.data.selectedRecord = undefined;
    }

    //submit form handler to search orders
    function submitForm() {

      //resets records
      configObj.pipelineObj.data.records = [];

      //get user_id
      if ( $scope.usersPipelineObj.data.selectedRecord ) {
        $scope.searchOrderFormObj.data.users_id =  $scope.usersPipelineObj.data.selectedRecord.user_col.users_id;
      }
      //get office_id
      if ( $scope.officesPipelineObj.data.selectedRecord ) {
        $scope.searchOrderFormObj.data.office_id =  $scope.officesPipelineObj.data.selectedRecord.TbOffice.office_id;
      }
      //get employee_id

      //showing fetching status
      $scope.searchOrderFormObj.status.sending = true;

      manageFactory.fetchOrdersPipeline({
        searchMode: true,
        payload: $scope.searchOrderFormObj.data,
        pageNum: 1, //This would be always 1 , when searching from this modal
        skipRecord: false
      })
        .then(function( res ) {

          $scope.searchOrderFormObj.status = {
            type: 'success',
            message: res.count + ' orders found successfully.'
          };

          //pipelineFactory.createRecordGroups(res.pages, $scope.groupObj);

          //Updates pipeline records with the new matching ones.
          angular.forEach( res.object, manageFactory.parseOrderModel );
          configObj.pipelineObj.data.records = res.object;

          $timeout($scope.$close, 2000);

        }, function( reason ) {

          $scope.searchOrderFormObj.status = {
            type: 'error',
            message: reason
          };

        });
    }

    function changeTab(index) {
      $scope.activeTabIndex = index;
    }
  }

})();
