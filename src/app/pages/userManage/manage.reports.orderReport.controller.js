/**
 * Created by Shahzad on 6/05/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('ManageReportsOrderReportCtrl', ManageReportsOrderReportCtrl );

  ManageReportsOrderReportCtrl.$inject = ['$scope', '$controller', 'manageFactory'];

  function ManageReportsOrderReportCtrl( $scope, $controller, manageFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseCtrl', {$scope: $scope});

    /*VM functions*/
    $scope.openShowResultModal = manageFactory.openShowResultModal;
    $scope.searchCounties = searchCounties;
    $scope.lookupAddress = lookupAddress;
    $scope.lookupOwner = lookupOwner;
    $scope.lookupAPN = lookupAPN;
    $scope.resetForm = resetForm;


    /*VM properties*/
    $scope.accordionStates = [true, false, false];
    $scope.lookupAddressObj = {
      autocomplete: '',
      results: [],
      data: {},
      mapOptions: { zoomControls: false, noClickout: true , drag: true, details:{} },
      mapObject: {}
    };
    $scope.lookupOwnerObj = {
      results: []
    };
    $scope.lookupAPNObj = {
      results: []
    };

    /*functions declarations*/
    //to reset a form
    function resetForm( form ) {

      $scope.showButton = '';

      switch( form.$name ) {
        case 'lookupAddressForm':
          $scope.lookupAddressObj.autocomplete = '';
          $scope.lookupAddressObj.results = [];
          $scope.lookupAddressObj.sending = false;
          $scope.lookupAddressObj.status = {};
          manageFactory.deleteAllPropsFromObj($scope.lookupAddressObj.data);

          $scope.lookupAddressObj.mapObject.resetMapHandler &&  $scope.lookupAddressObj.mapObject.resetMapHandler();
          break;
        case 'lookupOwnerForm':
          $scope.lookupOwnerObj = { results: [] };
          break;
        case 'lookupAPNForm':
          $scope.lookupAPNObj = { results: [] };
          break;
      }

      //resets form validation and interaction state
      form.$setPristine();
      form.$setUntouched();
    }

    //search counties with fips
    function searchCounties( lookupObjName, countyName ) {
      //resets previous search results
      $scope[lookupObjName].counties = [];

      if( countyName.length < 3 ) {
        return;
      }

      //console.log(countyName);
      $scope[lookupObjName].sendingCounty = true;

      manageFactory.autocompleteFips( countyName )
        .then(function( counties ) {
          $scope[lookupObjName].counties = counties;
          $scope[lookupObjName].sendingCounty = false;
        }, function() {
          $scope[lookupObjName].sendingCounty = false;
         //...
        });
    }

    //Change lookUp address data
    $scope.lookupAddressObj.mapOptions.details = $scope.lookupAddressObj.data;

    //to lookup an address for given details
    function lookupAddress( lookupAddressForm ) {
      var data;

      //Empty lookupAddressObj.
      $scope.lookupAddressObj.results = [];
      data = $scope.lookupAddressObj.data;

      //handle form submission with invalid data
      if ( !data.site_zip && !(data.site_city && data.site_state) ) {

        $scope.lookupAddressObj.status = {
          type : 'error',
          //message : 'Please fill in the required fields, or search for the property <br/> using the map or the general address search box.'
          message : 'City & State or Zip code required.'
        };

        return;
      }

      //resets last attempt results.
      $scope.lookupAddressObj.status = {};

      //setting formSending flag to true.
      $scope.lookupAddressObj.sending = true;

      manageFactory.searchAddressOwnerParcel('address', $scope.lookupAddressObj.data )
        .then(function( res ) {

          $scope.lookupAddressObj.status = {
            type : 'success',
            message : res.message
          };

          $scope.lookupAddressObj.results = res.results;
          $scope.lookupAddressObj.sending = false;

        }, function( reason ) {

          $scope.lookupAddressObj.status = {
            type : 'error',
            message : reason
          };

          $scope.lookupAddressObj.sending = false;

        });
    }

    //to lookup an owner for given details
    function lookupOwner( lookupOwnerForm ) {

      //Empty lookupAddressObj.
      $scope.lookupOwnerObj.results = [];
      //handle form submission with invalid data
      if ( lookupOwnerForm.$invalid ) {

        $scope.lookupOwnerObj.status = {
          type : 'error',
          message : 'please correct the red marked fields first.'
        };

        return;
      }

      //resets last attempt results.
      $scope.lookupOwnerObj.status = {};

      //setting formSending flag to true.
      $scope.lookupOwnerObj.sending = true;

      manageFactory.searchAddressOwnerParcel('owner', $scope.lookupOwnerObj.data )
        .then(function( res ) {

          $scope.lookupOwnerObj.status = {
            type : 'success',
            message : res.message
          };

          $scope.lookupOwnerObj.results = res.results;
          $scope.lookupOwnerObj.sending = false;

        }, function( reason ) {

          $scope.lookupOwnerObj.status = {
            type : 'error',
            message : reason
          };

          $scope.lookupOwnerObj.sending = false;

        });
    }

    //to lookup APN for given details
    function lookupAPN( lookupAPNForm ) {

      //Empty lookupAddressObj.
      $scope.lookupAPNObj.results = [];
      //handle form submission with invalid data
      if ( lookupAPNForm.$invalid ) {

        $scope.lookupAPNObj.status = {
          type: 'error',
          message: 'please correct the red marked fields first.'
        };

        return;
      }
      //resets last attempt results.
      $scope.lookupAPNObj.status = {};

      //setting formSending flag to true.
      $scope.lookupAPNObj.sending = true;

      manageFactory.searchAddressOwnerParcel('APN', $scope.lookupAPNObj.data )
        .then(function( res ) {

          $scope.lookupAPNObj.status = {
            type : 'success',
            message : res.message
          };

          $scope.lookupAPNObj.results = res.results;
          $scope.lookupAPNObj.sending = false;

        }, function( reason ) {

          $scope.lookupAPNObj.status = {
            type : 'error',
            message : reason
          };

          $scope.lookupAPNObj.sending = false;

        });
    }
  }

})();
