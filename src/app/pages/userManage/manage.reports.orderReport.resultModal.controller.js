/**
 * Created by Owais on 10/29/2015.
 */



(function(){

  'use strict';

  angular.module('workExample')
    .controller('ManageReportsOrderReportResultModalCtrl', ManageReportsOrderReportResultModalCtrl );

  ManageReportsOrderReportResultModalCtrl.$inject = ['$scope', '$controller', 'manageFactory', 'result'];

  function ManageReportsOrderReportResultModalCtrl( $scope, $controller, manageFactory, result ) {

    $scope.results = angular.isDefined(result) ? result : null;
    /*Extend BaseModalCtrl*/
    $controller('BaseModalCtrl', {$scope: $scope});

    /*listeners*/
    $scope.$watch('results', function( newVal, prevVal ){
      if ( newVal.length ){
        $scope.selectedResult = $scope.results[0];
      }
    });

    /*VM functions*/
    $scope.selectResult = selectResult;
    $scope.resetGeneratedReport= resetGeneratedReport;
    $scope.generateReport = generateReport;
    $scope.showReport = showReport;


    /*VM properties*/
    $scope.currentPage = 1;
    $scope.status = {};
    $scope.types = manageFactory.getFieldTypes();
    $scope.results = $scope.results || [];
    $scope.data  = {
      report_type : $scope.types.reports[0].value,
      output : $scope.types.deliveryFormat[0].value
    };


    /*function declarations*/
    //select a result for generating report
    function selectResult( result ) {
      $scope.selectedResult = result;
    }

    //resets last generated report
    function resetGeneratedReport() {
      $scope.status = {};
    }

    //opens up a new window containing the markup from response
    function showReport() {
      var reportWindow;

      if ( $scope.status.markup ) {
        reportWindow = $window.open();
        reportWindow.document.write( $scope.status.markup );
      }
    }

    //generates the report against selected order
    function generateReport() {
      $scope.status = {
        sending : true
      };

      manageFactory.generateReport(  $scope.data, $scope.selectedResult )
        .then(function( report ) {
          $scope.status = report;
          $scope.status.error = false;

        }, function( reason ) {

          $scope.status = {
            error: reason
          };

        });
    }

  }

})();
