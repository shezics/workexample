/**
 * Created by Shahzad on 6/05/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('ManageReportsUploadRecordsCtrl', ManageReportsUploadRecordsCtrl );

  ManageReportsUploadRecordsCtrl.$inject = ['$scope', '$controller', '$timeout', 'Upload'];

  function ManageReportsUploadRecordsCtrl( $scope, $controller, $timeout, Upload ) {

    /*Extend baseCtrl*/
    $controller('BaseModalCtrl', {$scope: $scope});

    /*VM functions*/
    //...

    /*VM properties*/
    //...
    $scope.uploadCsv = function(file) {
      file.upload = Upload.upload({
        url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
        data: {file: file}
      });

      file.upload.then(function (response) {
        $timeout(function () {
          file.result = response.data;
        });
      }, function (response) {
        if (response.status > 0)
          $scope.errorMsg = response.status + ': ' + response.data;
      }, function (evt) {
        // Math.min is to fix IE which reports 200% sometimes
        file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
      });
    }
  }

})();
