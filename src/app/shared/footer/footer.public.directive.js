/**
 *  * Created by Shahzad on 6/04/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .directive('publicFooter', publicFooter );

  publicFooter.$inject = ['$state'];

  function publicFooter( $state ) {

    return {
      restrict : 'A',
      templateUrl: 'app/shared/footer/footer.public.view.html',
      link: function ( $scope, $element, attrs ) {

        /*VM properties*/
        $scope.$state = $state;
      },
      controller: function( $scope ) {

        /*VM properties that can be inherited by child scopes.*/
        $scope.mapOptions = {
          lonLat: [-117.844105, 33.683219],
          markers: [{
            lonLat: [-117.844105, 33.683219],
            image: 'assets/images/_pin.png',
            popupInfo: {
              width: 220,
              height: 135,
              markup: [
                '<div class="info-box">',
                ' <h3>Benutech Inc. LLC</h3>',
                ' <p>2525 Main Street,',
                '  Suite 200, Irvine,',
                '  CA, 92614 <br/>',
                '  United States',
                ' </p>',
                '</div>'
              ].join(' ')
            }
          }],
          zoomControls: false
        };
      }
    }
    ;
  }

})();
