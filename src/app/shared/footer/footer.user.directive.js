/**
 *  * Created by Shahzad on 6/04/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .directive('userFooter', userFooter );

  userFooter.$inject = ['$state'];

  function userFooter( $state ) {

    return {
      restrict : 'A',
      templateUrl: 'app/shared/footer/footer.user.view.html',
      link: function ( $scope, $element, attrs ) {

        /*VM properties*/
        $scope.$state = $state;
      }
    };
  }

})();
