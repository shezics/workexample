/**
 * Created by Shahzad on 5/04/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('ForgotCtrl',  ForgotCtrl );

  ForgotCtrl.$inject = ['$scope', '$controller', '$modal', 'userFactory'];

  function ForgotCtrl( $scope, $controller, $modal, userFactory ) {

    /*Extend BaseModalCtrl*/
    $controller('BaseModalCtrl', {$scope: $scope});

    /*VM functions*/
    $scope.recoverPassword = recoverPassword;
    $scope.openLoginModal = openLoginModal;

    /*VM properties*/
    $scope.forgotFormObj = {};

    //inherited from BaseModalCtrl.to prevent modal from closing, while modal is in sending status.
    $scope.$listenFormStatus(  $scope.forgotFormObj );

    //get login modal for logging in
    function openLoginModal(){
      //close the login model first
      $scope.$close();

      //opens login modal
      userFactory.openLoginModel();
    }

    //to get reset password with provided email
    function recoverPassword( form ) {

      //handle form submission with invalid data
      if ( form.$invalid ) {
        $scope.forgotFormObj.status = {
          type: 'error',
          message: 'please correct the red marked fields first.'
        };
        return;
      }

      //setting formSending flag to true.
      $scope.forgotFormObj.status = {
        sending: true
      };

      userFactory.recoverPassword( $scope.forgotFormObj.data )
        .then(function( res ) {
          $scope.forgotFormObj.status = {
            type: 'success',
            message: res
          };

        }, function( reason ) {
          $scope.forgotFormObj.status = {
            type: 'error',
            message: reason
          };
        });
    }
  }

})();
