/**
 *  * Created by Shahzad on 6/04/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .directive('publicHeader', publicHeader );

  publicHeader.$inject = ['$state', '$localStorage', 'userFactory', 'authFactory'];

  function publicHeader( $state, $localStorage, userFactory, authFactory ) {

    return {
      restrict : 'A',
      templateUrl: 'app/shared/header/header.public.view.html',
      link: function ( $scope, $element, attrs ) {

        /*VM functions*/
        $scope.openLoginModal = userFactory.openLoginModel; //get login modal for logging in
        $scope.logout = logout;

        /*VM properties*/
        $scope.$state = $state;
        $scope.$storage = $localStorage;

        //to logout user from the App.
        function logout() {
          authFactory.logout()
            .then(function( res ) {

              //on successful logout, take user to home page
              $state.transitionTo('public.home');

            }, function( reason ) {
              //console.log( reason );
            });
        }
      }
    };
  }

})();
