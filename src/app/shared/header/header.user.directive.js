/**
 *  * Created by Shahzad on 6/04/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .directive('userHeader', userHeader );

  userHeader.$inject = ['$state', 'authFactory', 'cacheFactory', 'homeUserFactory'];

  function userHeader( $state, authFactory, cacheFactory, homeUserFactory ) {

    return {
      restrict : 'A',
      templateUrl: 'app/shared/header/header.user.view.html',
      link: function ( $scope, $element, attrs ) {

        /*VM functions*/
        $scope.logout = logout;
        $scope.searchForm = homeUserFactory.searchForm;
        $scope.selectFromMap = homeUserFactory.selectFromMap;
        $scope.addLeadToFarm = homeUserFactory.addLeadToFarm;
        $scope.showFarmDetails = homeUserFactory.showFarmDetails;
        $scope.openQueryToolModal = homeUserFactory.openQueryToolModal;

        /*VM properties*/
        $scope.$state = $state;
        $scope.models = cacheFactory.getAllModels();

        /*function declarations*/
        //get log out the current user
        function logout(){
          authFactory.logout();
          $state.go('public.home');
        }

      }
    };
  }

})();
