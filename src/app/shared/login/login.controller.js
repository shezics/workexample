/**
 * Created by Shahzad on 5/04/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('LoginCtrl', LoginCtrl );

  LoginCtrl.$inject = ['$scope', '$controller', '$modalInstance', '$state', '$timeout', '$localStorage', 'authFactory', 'userFactory'];

  function LoginCtrl( $scope, $controller, $modalInstance, $state, $timeout, $localStorage, authFactory, userFactory ) {

    /*Extend BaseModalCtrl*/
    $controller('BaseModalCtrl', {$scope: $scope});

    /*VM functions*/
    $scope.login = login;
    $scope.openForgotModal = openForgotModal;
    $scope.beforeClosing = beforeClosing;

    /*VM props*/
    $scope.loginFormObj = {};

    /*private variables*/
    //...

    //initialize Stuff
    //inherited from BaseModalCtrl.to prevent modal from closing, while modal is in sending status.
    $scope.$listenFormStatus(  $scope.loginFormObj );

    //TODO remove hard coded for production
    $scope.loginFormObj.data = {
      //TbUser: {
      //  username: 'shahzadscs@gmail.com',
      //  password: 'bonDB33947'
      //}
      //TbUser: {
      //  username: 'testuser2@gmail.com',
      //  password: '1234567'
      //}
      TbUser: {
        username: 'testOM6@ttb.com',
        password: '1234567'
      }
    };

    /*functions declaration*/

    //on closing of modal
    function beforeClosing() {
      $localStorage.redirectTo = null;
    }

    //get forgot password modal to recover password.
    function openForgotModal(){
      //close the login model first
      $scope.$close();

      //opens forgot password modal
      userFactory.openForgotPassModel();
    }

    //to get user login with provided credentials.
    function login( form ) {

      //handle form submission with invalid data
      if ( form.$invalid ) {
        $scope.loginFormObj.status = {
          type: 'error',
          message: 'please correct the red marked fields first.'
        };
        return;
      }

      //setting formSending flag to true.
      $scope.loginFormObj.status = {
        sending: true
      };

      authFactory.login( $scope.loginFormObj.data )
        .then(function( res ) {
          $scope.loginFormObj.status = {
            type: 'success',
            message: 'logged in successfully.'
          };
          closeModal();
        }, function( reason ) {
          $scope.loginFormObj.status = {
            type: 'error',
            message: reason
          };
        });
    }

    //close the modal after successful login
    function closeModal() {
      $timeout(function() {
        var redirectTo = $localStorage.redirectTo;
        $scope.$close();
        $state.transitionTo( redirectTo || 'user.home');
      }, 2000);
    }
  }

})();
