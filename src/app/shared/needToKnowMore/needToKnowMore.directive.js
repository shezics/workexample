/**
 *  * Created by Shahzad on 6/04/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .directive('needToKnowMore', needToKnowMore );

  needToKnowMore.$inject = [];

  function needToKnowMore() {

    return {
      restrict : 'A',
      templateUrl: 'app/shared/needToKnowMore/needToKnowMore.view.html',
      link: function ( $scope, $element, attrs ) {
        //...
      }
    };
  }

})();
