/**
 * Created by Israr Ali 11/11/2015.
 */

(function() {

  'use strict';

  angular
    .module('workExample')
    .directive('officeForm', officeForm);

  officeForm.$inject = ['FIELD_TYPES', 'manageOfficesFactory'];

  function officeForm( FIELD_TYPES, manageOfficesFactory ) {
    return{
      restrict: 'A',
      templateUrl: 'app/shared/officeForm/officeForm.directive.view.html',
      scope : {
        office: "=",
        handlers: "=",
        config: "="
      },
      link: link
    };

    function link( $scope, $element, $attrs ) {

      //listeners
      $scope.$watch('office', updateOfficeModal);

      //VM function.
      $scope.submitForm = $scope.handlers.submitForm = submitForm;
      $scope.resetForm = $scope.handlers.resetForm = resetForm;

      //VM properties.
      $scope.status = $scope.config.status || {};
      $scope.types = FIELD_TYPES;

      // function declarations

      //submits the form for adding or updating a Office
      function submitForm() {
        var form, options, sendingMethod;

        form = $scope.officeForm;

        //handle form submission with invalid data
        if (form.$invalid) {
          resetFormStatus('error', 'please correct the red marked fields first.');
          return;
        }

        //handle if office submits form with no changes
        if (!form.$dirty) {
          resetFormStatus('error', 'No changes made to save.');
          return;
        }

        options = {
          payload: $scope.officeObj,
          editOffice: $scope.config.editOffice
        };

        resetFormStatus();
        $scope.status.sending = true;

        sendingMethod = $scope.config.page === 'search-office' ? manageOfficesFactory.searchOffice : manageOfficesFactory.addUpdateOffice;

        sendingMethod(options)
          .then(function (res) {
            resetFormStatus('success', $scope.config.successMessage || res.message);

            //invokes success handler from config, if provided
            $scope.config.onSuccess && $scope.config.onSuccess(res.info);

          }, function (reason) {
            resetFormStatus('error', reason);
          });
      }

      //to update office modal being used in form fields.
      function updateOfficeModal() {
        $scope.officeObj = $scope.config.updateOriginalModal ? $scope.office : angular.copy( $scope.office );
      }

      //resets status object without breaking reference.
      function resetFormStatus(type, message){
        $scope.status.sending = false;
        $scope.status.type = type;
        $scope.status.message = message;

        //resets form validation and interaction state
        $scope.officeForm.$setPristine();
        $scope.officeForm.$setUntouched();

      }

      // rests the office form to it's initial state.
      function resetForm() {

        //resets office model with initial / original data.
        updateOfficeModal();

        //resets last attempt results.
        resetFormStatus();

        //invokes reset handler from config, if provided
        $scope.config.onReset && $scope.config.onReset();
      }
    }
  }
})();
