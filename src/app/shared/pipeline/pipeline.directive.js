/**
 *  * Created by Shahzad on 8/19/2015.
 */
/*
 * This user-form directive is created to be used in different pages
 * e.g. signup page, update profile page, and add/update user for manage users page.
 *
 * config {Object}
 *  apiPropsObj - {Object} - to pass extra properties to fetch API
 * */

(function () {

  'use strict';

  angular
    .module('workExample')
    .directive('pipeline', pipeline );

  pipeline.$inject = [
    'pipelineFactory',
    'manageFactory',
    'userFactory',
    'manageUsersFactory',
    'manageOfficesFactory',
    'manageEmployeesFactory'];

  function pipeline( pipelineFactory, manageFactory, userFactory, manageUsersFactory, manageOfficesFactory, manageEmployeesFactory) {
    return {
      restrict : 'A',
      templateUrl: 'app/shared/pipeline/pipeline.directive.view.html',
      scope: {
        config: '=',
        data: '=',
        status: '='
      },
      link: function ( $scope, $element, $attrs ) {

        /*private variables*/
        var handlers, clearSearchEndpoint;

        /*listeners*/
        //...

        /*VM functions*/
        $scope.onGroupChange = onGroupChange;
        $scope.onRecordSelect = onRecordSelect;
        $scope.clearAdvanceSearch = clearAdvanceSearch;


        /*VM properties*/

        //for <td> of error or "fetching..." text.
        $scope.colspan = $scope.config.columns.length + ( $scope.config.actions || $scope.config.selectRecord ? 1 : 0 );

        //for pipeline pagination current page
        $scope.currentPage = 1;

        //for controls at top of pagination to provide search functionality
        $scope.searchObj = {
          //default selected filter-by type
          filterBy: '$',

          //available types for filterBy-filter - create a separate copy from columns
          filterByOptions: angular.copy($scope.config.columns),

          //default selected search-text
          searchText: ''
        };

        //for selecting server-side record-groups / record-sets
        $scope.groupObj = {
          //default selected groupNum for GroupBy dropdown
          groupNum: 1,

          //HACK - store previously selected GroupNum to avoid duplicate select of group from dropdown
          cachedGroupNum: 1,

          //default groups list - to be modified from API reponse.
          groups: [{
            value: 1,
            title: '1-1000'
            //},{
            //  value: 2,
            //  title: '1001-2000',
          }]
        };

        /*initialization*/

        //API handlers for given pipeline.
        handlers = {
          fetch: null,
          parseModel: null
        };

        setHandlers();
        fetchRecords();

        //modifies filterBy menu to All columns for search functionality
        $scope.searchObj.filterByOptions.unshift({
          value: '$',
          title: 'All Columns'
        });

        /*functions declarations*/

        //handler for clear or reset advance search obj and load results from server with out any search
        function clearAdvanceSearch() {
          //clears the advance search object.
          manageFactory.deleteAllPropsFromObj( $scope.config.advanceSearch.searchObj );

          //clears quick search
          $scope.searchObj.searchText = '';

          //invokes any reset handler provided in config from controller
          $scope.config.advanceSearch.resetHandler && $scope.config.advanceSearch.resetHandler();

          //reload pipeline without search
          fetchRecords(true);
        }

        //to fetch and populate records in pipeline
        function fetchRecords( clearAndFetch ) {
          var options, promise;

          //resets records
          $scope.data.records = [];
          $scope.status.fetching = true;

          if ( clearAndFetch ) {
            promise = pipelineFactory.clearAndFetchPipeline({
              endpoint: clearSearchEndpoint,
              parseModel: handlers.parseModel
            });
          } else {
            options = angular.extend({
              pageNum: $scope.groupObj.groupNum,
              skipRecord: $scope.config.skipRecord,
              searchMode: $scope.config.advanceSearch ? $scope.config.advanceSearch.useAdvanceSearch : false

            }, $scope.config.apiPropsObj || {});

            promise = handlers.fetch(options);
          }

          promise
            .then(function( res ) {

              $scope.status.fetching = false;
              $scope.data.records = res.object;

              pipelineFactory.createRecordGroups(res.pages, $scope.groupObj);

            }, function( reason ) {

              $scope.status.fetching = false;
              $scope.status.fetchingError = reason;

            });
        }

        /*TODO: improve replace ng-click with jquery delegate technique*/
        //handler for when any record get selected
        //invokes when user selects a record from pipeline
        function onRecordSelect( record ) {
          //console.log('record selected', record);

          if( !$scope.config.selectRecord ) {
            return;
          }

          $scope.data.selectedRecord = $scope.selectedRecord = record;
          $scope.config.onSelect && $scope.config.onSelect(record);
        }

        //handler for when any group get selected from group-by dropdown
        //to change pipeline records from server pagination
        function onGroupChange() {

          //HACK - data-on-select="" of ui-select triggers even when user selects already selected option.
          if ( $scope.groupObj.groupNum !== $scope.groupObj.cachedGroupNum ) {
            $scope.groupObj.cachedGroupNum = $scope.groupObj.groupNum;
            fetchRecords();
          }
        }

        //to select an appropriate method for fetching, parsing records, or clearing search criteria form serv,
        function setHandlers(){
          switch ( $scope.config.type ) {
            case 'orders':
              handlers.fetch = manageFactory.fetchOrdersPipeline;
              handlers.parseModel = manageFactory.parseOrderModel;
              clearSearchEndpoint = 'clear_order_search.json';
              break;
            case 'reps':
              handlers.fetch = userFactory.fetchRepsPipeline;
              handlers.parseModel = userFactory.parseRepModel;
              //clearSearchEndpoint = 'clear_rep_search.json';
              break;
            case 'employees':
              handlers.fetch = manageEmployeesFactory.fetchEmployeesPipeline;
              handlers.parseModel = manageEmployeesFactory.parseEmployeesModel;
              clearSearchEndpoint = 'clear_employee_search.json';
              break;
            case 'offices':
              handlers.fetch = manageOfficesFactory.fetchOfficesPipeline;
              handlers.parseModel = manageOfficesFactory.parseOfficeModel;
              clearSearchEndpoint = 'clear_office_search.json';
              break;
            case 'users':
              handlers.fetch = manageUsersFactory.fetchUsersPipeline;
              handlers.parseModel = manageUsersFactory.parseUserModel;
              clearSearchEndpoint = 'clear_user_search.json';
              break;
            case 'office-users':
              handlers.fetch = manageOfficesFactory.fetchOfficeUsersPipeline;
              handlers.parseModel = manageUsersFactory.parseUserModel;
              //clearSearchEndpoint = 'clear_office_users_search.json';
              break;

          }
        }

      }
    };
  }
})();
