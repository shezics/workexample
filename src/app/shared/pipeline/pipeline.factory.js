/**
 * Created by Shahzad on 8/19/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .service('pipelineFactory', pipelineFactory );

  pipelineFactory.$inject = [
    '$q',
    '$http',
    '$filter',
    'appConstant',
    'utilFactory'
  ];

  function pipelineFactory( $q, $http, $filter, appConstant, utilFactory ) {

    var obj;

    //obj to store useful values, or handlers.
    obj = {
      dateFilter: $filter('date')
    };

    return {
      clearAndFetchPipeline: clearAndFetchPipeline,
      createRecordGroups: createRecordGroups,
      formatDate: formatDate,
      breakValue: breakValue,
      setAddress: setAddress,
      setPhone: setPhone,
      setEmail: setEmail
    };

    //parse the clear pipeline API different format response and returns it in one format.
    function parseClearResponse( res, clearEndpoint ) {
      switch( clearEndpoint ) {
        case 'clear_order_search.json':
          return {
            count: res.count,
            object: res.data,
            pages: res.pages
          };

        case 'clear_user_search.json':
          return {
            count: res.count,
            object: res.data,
            pages: res.pages
          };
        case 'clear_office_search.json':
          return {
            count: res.response.data.object.length,
            object: res.response.data.object,
            pages: res.response.data.pages
          };
        case 'clear_employee_search.json':
          return res;
      }
    }

    //clears search criteria from server session and gets results for first page of pipeline API.
    function clearAndFetchPipeline( options ) {
      var defer;

      defer = $q.defer();

      $http.get( appConstant.baseURL + 'webservices/' + options.endpoint)
        .success(function ( res ) {
          if ( res ) {

            //TODO remove when API sends response in same format
            res = parseClearResponse(res, options.endpoint);

            angular.forEach(res.object, options.parseModel);
            defer.resolve(res);

          } else {
            defer.reject('error occurred in clearing user search.' );
          }
        })
        .error(function ( reason, status ) {
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason);
        });

      return defer.promise;
    }

    //creates options for record groups
    function createRecordGroups( totalPages, groupObj ) {

      if ( totalPages <= 1 || groupObj.groups.length == totalPages ) {
        return;
      }

      for (var i = 2; i <= totalPages; i++) {
        groupObj.groups.push({
          value: i,
          title: ( (i - 1) * 1000 + 1 ) + '-' + (i * 1000)
        });
      }
    }

    //formats the date for any field in pipeline
    function formatDate( dateString, format ) {
      var filteredDate, dateInstance;

      if ( dateString ) {

        dateInstance = new Date(dateString);

        //TODO FIX UTC and Local time difference.
        filteredDate = obj.dateFilter(dateInstance.getTime(), format || 'MM-dd-yyyy hh:mma');
      }

      return filteredDate || '';
    }

    //helper function - to insert break-line markup text when finds any value
    function breakValue( val ) {
      return val ? '<br>' + val : '';
    }

    //formats the value for address column
    function setAddress( model ) {
      var customAddress = '';

      angular.forEach(model, function( address ) {
        customAddress = customAddress ? customAddress + '<br>' : '';

        customAddress += (address.address) +
          breakValue(address.city) +
          breakValue(address.state.toUpperCase()) +
          breakValue(address.zip);
      });

      return customAddress;
    }

    //formats the value for phone column
    function setPhone( model ) {
      var customPhone = '';

      angular.forEach(model, function( phone ) {
        customPhone = customPhone ? customPhone + '<br>' : '';

        customPhone +=  !phone.phone ? '-' : '<a href="tel:'+ phone.phone +' ">' + formatPhone(phone.phone) + '</a>';
      });

      return customPhone;
    }

    //formats the phone according to spec
    function formatPhone(phone){
      return '(' + phone.slice(0, 3) + ') ' + phone.slice(3, 6) + ' - ' + phone.slice(6, 10);
    }

    //formats the value for email column
    function setEmail( model ) {
      var customEmail = '';

      angular.forEach(model, function( email ) {
        customEmail = customEmail ? customEmail + '<br>' : '';
        customEmail += '<a href="mailto:' + email.email.toLowerCase() + '">' + email.email + '</a>';
      });

      return customEmail;
    }


  }

})();
