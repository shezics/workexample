/**
 *  * Created by Shahzad on 7/14/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .directive('productOverview', productOverview );

  productOverview.$inject = [];

  function productOverview() {

    return {
      restrict : 'A',
      templateUrl: 'app/shared/productOverview/productOverview.view.html',
      link: function ( $scope, $element, attrs ) {
        //...
      }
    };
  }

})();
