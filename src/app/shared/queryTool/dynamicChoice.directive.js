/**
 *  * Created by Shahzad on 19/12/2015.
 */

(function() {
  'use strict';

  angular
    .module('workExample')
    .directive('dynamicChoice', dynamicChoice);

  dynamicChoice.$inject = [
    '$timeout',
    'queryToolFactory',
    'utilFactory'
  ];

  function dynamicChoice( $timeout, queryToolFactory, utilFactory ) {
    return {
      restrict: 'A',
      scope:{
        field: '=',
        form: '=',
        formObj: '='
      },
      templateUrl: 'app/shared/queryTool/dynamicChoice.directive.view.html',
      link: link
    };

    function link($scope, $element, $attrs) {

      /*listeners*/
      if ( $scope.field.choices_source && $scope.field.choices_source.api_input ) {
        $scope.$watch( 'formObj.data.' + $scope.field.choices_source.api_input, fetchChoices);
      }

      /*VM functions*/

      /*VM properties*/
      $scope.choices = [];

      /*Initialization*/
      getChoices();
      setDefaultValue();

      //gets choices for given search
      function getChoices() {
        if ( $scope.field.choices ) {
          $scope.choices = utilFactory.plainObjToArray( $scope.field.choices );
        } else if ( !$scope.field.choices_source.api_input ) {
          fetchChoices();
        }
      }


      //fetches choices for given search
      function fetchChoices(){
        var options;

        if ( $scope.field.choices_source.api_input && !$scope.formObj.data[$scope.field.choices_source.api_input] ) {
          return;
        }

        options = {
          endpoint: $scope.field.choices_source.api_endpoint,
          input: $scope.formObj.data[$scope.field.choices_source.api_input]
        };

        $scope.fetching = true;

        queryToolFactory.fetchChoices(options)
          .then(function(choices) {

          $scope.fetching = false;
          $scope.choices = utilFactory.plainObjToArray( choices );

        }, function(){

          $scope.fetching = false;
          //handle failure here.

        });
      }

      //sets default value(s)
      function setDefaultValue(){
        //TODO handle array for default_value
        if ( $scope.field.default_value ) {
          $scope.formObj.data[$scope.field.field_name] = $scope.field.default_value;
        }
      }
    }
  }
})();
