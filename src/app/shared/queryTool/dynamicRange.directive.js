/**
 *  * Created by Shahzad on 20/12/2015.
 */

(function() {
  'use strict';

  angular
    .module('workExample')
    .directive('dynamicRange', dynamicRange);

  dynamicRange.$inject = [
    '$timeout',
    'queryToolFactory',
    'utilFactory'
  ];

  function dynamicRange( $timeout, queryToolFactory, utilFactory ) {
    return {
      restrict: 'A',
      scope:{
        field: '=',
        form: '=',
        formObj: '='
      },
      templateUrl: 'app/shared/queryTool/dynamicRange.directive.view.html',
      link: link
    };

    function link($scope, $element, $attrs) {

      /*listeners*/

      /*VM functions*/

      /*VM properties*/
      $scope.matchOptions = ['From-To', '=', '>', '>=', '<', '<='];

      /*Initialization*/
      setDefaultValue();

      //sets default value(s)
      function setDefaultValue(){
        $scope.formObj.data[$scope.field.field_name] = $scope.formObj.data[$scope.field.field_name] || {};
        $scope.formObj.data[$scope.field.field_name].match = '=';

        //TODO handle array for default_value and for "From", and "To" fields.
        if ( $scope.field.default_value ) {
          $scope.formObj.data[$scope.field.field_name].value = $scope.field.default_value;
        }
      }
    }
  }
})();
