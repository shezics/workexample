/**
 *  * Created by Shahzad on 21/12/2015.
 */

(function() {
  'use strict';

  angular
    .module('workExample')
    .directive('dynamicWildcard', dynamicWildcard);

  dynamicWildcard.$inject = [
    '$timeout',
    'queryToolFactory',
    'utilFactory'
  ];

  function dynamicWildcard( $timeout, queryToolFactory, utilFactory ) {
    return {
      restrict: 'A',
      scope:{
        field: '=',
        form: '=',
        formObj: '='
      },
      templateUrl: 'app/shared/queryTool/dynamicWildcard.directive.view.html',
      link: link
    };

    function link($scope, $element, $attrs) {

      /*listeners*/

      /*VM functions*/

      /*VM properties*/
      $scope.matchOptions = ['Contains', 'Start/w', 'Ends/w'];

      /*Initialization*/
      setDefaultValue();

      //sets default value(s)
      function setDefaultValue(){
        $scope.formObj.data[$scope.field.field_name] = $scope.formObj.data[$scope.field.field_name] || {};
        $scope.formObj.data[$scope.field.field_name].match = 'Contains';

        //TODO handle array for default_value
        if ( $scope.field.default_value ) {
          $scope.formObj.data[$scope.field.field_name].value = $scope.field.default_value;
        }
      }
    }
  }
})();
