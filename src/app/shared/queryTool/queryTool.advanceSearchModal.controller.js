/**
 * Created by Shahzad on 6/27/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('QueryToolAdvanceSearchModalCtrl', QueryToolAdvanceSearchModalCtrl );

  QueryToolAdvanceSearchModalCtrl.$inject = [
    '$scope',
    '$controller',
    'FIELD_TYPES',
    'queryToolFactory'
  ];

  function QueryToolAdvanceSearchModalCtrl( $scope, $controller, FIELD_TYPES, queryToolFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseModalCtrl', {$scope: $scope});

    /*VM functions*/

    /*VM props*/
    $scope.searchOrderAdvanceObj = {
      status : {}
    };

    $scope.types = FIELD_TYPES.farmOptions;

    /*private variables*/
    //...

    //initialize Stuff

    //inherited from BaseModalCtrl.to prevent modal from closing, while modal is in sending status.
    $scope.$listenFormStatus( $scope.searchOrderAdvanceObj );

  }

})();
