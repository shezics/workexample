/**
 * Created by Shahzad on 12/18/2015.
 */

(function () {

  'use strict';

  angular
    .module('workExample')
    .service('queryToolFactory', queryToolFactory );

  queryToolFactory.$inject = [
    '$q',
    '$http',
    '$modal',
    'appConstant',
    'utilFactory'
  ];

  function queryToolFactory($q, $http, $modal, appConstant, utilFactory ) {

    return {
      fetchChoices: fetchChoices,
      getAllSearchFields: getAllSearchFields,
      searchProperty: searchProperty,
      openAdvanceSearchModal: openAdvanceSearchModal
    };

    //fetches choices for given search
    function fetchChoices( options ) {
      var defer;

      defer = $q.defer();

      $http.get( appConstant.baseURL + options.endpoint + ( options.input ? '/' + options.input : '' ) + '.json')
        .success(function ( res ) {
          if ( res ) {
            defer.resolve( res );
          } else {
            defer.reject();
          }
        })
        .error(function ( reason, status ) {
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason );
        });

      return defer.promise;
    }

    //fetch all dynamic form fields for query tool
    function getAllSearchFields(){
      var defer;

      defer = $q.defer();

      //TODO remove when API is available
      defer.resolve( getDemoFields() );
      return defer.promise;

      $http.get( appConstant.baseURL + 'webservices/get_search_fields.json')
        .success(function ( res ) {
          if ( res ) {

            //console.log('users pipeline fetched successfully.');
            defer.resolve( res );

          } else {
            //console.log('fetching users pipeline failed.');
            //console.log( res );
            defer.reject();
          }
        })
        .error(function ( reason, status ) {
          //console.log('fetching users pipeline  failed');
          //console.log( reason );
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason );
        });

      return defer.promise;

      //gets demo fields
      function getDemoFields(){
        return [
          {
            "group_name":"Geography",
            "fields":[
              {
                "field_name":"mm_fips_state_code",
                "label":"State",
                "value_type":"text",
                "search_type":"C",
                "default_value":"06",
                "choices_source":{
                  "api_endpoint":"webservices/get_states",
                  "api_input":"",
                  "api_output":"mm_fips_state_code:state_name"
                }
              },
              {
                "field_name":"mm_fips_muni_code",
                "label":"County",
                "value_type":"text",
                "search_type":"C",
                "default_value":"",
                "choices_source":{
                  "api_endpoint":"webservices/get_counties",
                  "api_input":"mm_fips_state_code",
                  "api_output":"mm_fips_muni_code:county_name"
                }
              },
              {
                "field_name":"sa_site_city",
                "label":"City",
                "value_type":"text",
                "search_type":"C",
                "default_value":"",
                "choices_source":{
                  "api_endpoint":"webservices/get_cities",
                  "api_input":"mm_fips_muni_code",
                  "api_output":"city_name:city_name"
                }
              }
            ]
          }, {
            "group_name":"Characteristics",
            "fields":[
              {
                "field_name":"sa_sqft",
                "label":"Building Size / Total Structural Area",
                "value_type":"number",
                "search_type":"R",
                "default_value":""
              },
              {
                "field_name":"sa_lotsize",
                "label":"Lot Size (sqft)",
                "value_type":"number",
                "search_type":"R",
                "default_value":""
              },
              {
                "field_name":"sa_nbr_bedrms",
                "label":"# Bedrooms",
                "value_type":"number",
                "search_type":"R",
                "default_value":""
              },
              {
                "field_name":"sa_nbr_bath",
                "label":"# Baths",
                "value_type":"number",
                "search_type":"R",
                "default_value":"",
                "validation":{
                  "min":0,
                  "max":30
                }
              },
              {
                "field_name":"sa_owner_1",
                "label":"Owner Name",
                "value_type":"text",
                "search_type":"W",
                "default_value":"",
                "validation":{
                  "maxlength":50,
                  "alert_text":"Name cannot be longer than 50 characters"
                }
              },
              {
                "field_name":"sa_yr_blt",
                "label":"Year Built",
                "value_type":"number",
                "search_type":"R",
                "default_value":""
              },
              {
                "field_name":"sa_nbr_units",
                "label":"# Units",
                "value_type":"number",
                "search_type":"R",
                "default_value":""
              },
              {
                "field_name":"use_code_std",
                "label":"Property Type",
                "value_type":"text",
                "search_type":"CM",
                "choices":{
                  "SRFR":"Single Family Residence",
                  "RCON":"CONDO",
                  "RDUP":"Duplex",
                  "RTRI":"Triplex",
                  "RQUA":"Quadraplex"
                },
                "default_value":[
                  "SRFR",
                  "RCON"
                ],
                "required":true
              },
              {
                "field_name":"sa_pool_code",
                "label":"Pool",
                "value_type":"text",
                "search_type":"C",
                "choices":{
                  "Y":"Yes",
                  "N":"No",
                  "1":"Fiberglass\\/Plastic",
                  "2":"Gunite",
                  "3":"Reinforced Concrete",
                  "4":"Plastic Lined"
                },
                "default_value":[
                  "Y"
                ],
                "include":"optional"
              },
              {
                "field_name":"sa_driveway_code",
                "label":"Drive Way",
                "value_type":"text",
                "search_type":"C",
                "choices":{
                  "Y":"Yes",
                  "N":"No",
                  "2":"Asphalt",
                  "3":"Concrete",
                  "4":"Dirt"
                },
                "default_value":[
                  "Y"
                ],
                "include":"optional"
              },
              {
                "field_name":"sa_grg_1_code",
                "label":"Primary Garage Type",
                "value_type":"text)",
                "search_type":"C",
                "choices":{
                  "Y":"Yes",
                  "N":"No",
                  "A":"Attached",
                  "B":"Basement",
                  "C":"Detached",
                  "GC":"Garage and Carport"
                },
                "default_value":[
                  "Y"
                ],
                "include":"optional"
              }
            ]
          }
        ];
      }
    }

    //query for property global search
    function searchProperty( options ) {
      var defer, request;

      defer = $q.defer();

      request = JSON.stringify( options.payload );

      $http.post( appConstant.baseURL + 'property_global_search.json', request)
        .success(function ( res ) {
          if ( res ) {
            defer.resolve( res );
          } else {
            defer.reject('Some error occurred in contacting server.');
          }
        })
        .error(function ( reason, status ) {
          reason = utilFactory.requestErrorHandler( reason, status );
          defer.reject( reason );
        });

      return defer.promise;
    }

    //opens a advance search modal over search entities form modal.
    function openAdvanceSearchModal() {
      return $modal.open({
        controller: 'QueryToolAdvanceSearchModalCtrl',
        templateUrl: 'app/shared/queryTool/queryTool.advanceSearchModal.view.html',
        size: 'sm',
        windowClass: 'search-order-advance'
      });
    }
  }

})();
