/**
 * Created by Shahzad on 12/18/2015.
 */

(function(){

  'use strict';

  angular.module('workExample')
    .controller('QueryToolModalCtrl', QueryToolModalCtrl );

  QueryToolModalCtrl.$inject = [
    '$scope',
    '$controller',
    'queryToolFactory'
  ];

  function QueryToolModalCtrl( $scope, $controller,  queryToolFactory ) {

    /*Extend baseCtrl*/
    $controller('BaseModalCtrl', {$scope: $scope});

    /*VM functions*/
    $scope.openAdvanceSearchModal = queryToolFactory.openAdvanceSearchModal;
    $scope.changeTab = changeTab;
    $scope.submitForm = submitForm;
    $scope.resetForm = resetForm;

    /*VM props*/
    $scope.activeTabIndex = 0;
    $scope.queryFormObj = {
      fieldGroups: [],
      data: {},
      status: {}
    };

    /*private variables*/
    //...

    /*initialization*/
    //inherited from BaseModalCtrl.to prevent modal from closing, while modal is in sending status.
    $scope.$listenFormStatus( $scope.queryFormObj );

    fetchFormFields();

    /*function declarations*/

    //submits the form to query for a property
    function submitForm(){

      $scope.queryFormObj.status = {};
      $scope.queryFormObj.status.sending = true;

      queryToolFactory.searchProperty({ payload: $scope.queryFormObj.data })
        .then(function( res ) {
          $scope.queryFormObj.results = res.results;

          $scope.queryFormObj.status.sending = false;
          $scope.queryFormObj.status.type = 'success';
          $scope.queryFormObj.status.message = res.message;
        }, function( reason ) {
          $scope.queryFormObj.status.sending = false;
          $scope.queryFormObj.status.type = 'error';
          $scope.queryFormObj.status.message = reason;
        });
    }

    //resets query form fields to it's initial states
    function resetForm(){
      $scope.queryFormObj.status = {};

      //resets form validation and interaction state
      $scope.queryForm.$setPristine();
      $scope.queryForm.$setUntouched();

      //TODO Handle default values to all fields.
      $scope.queryFormObj.data = {};
    }

    //fetches all form-fields
    function fetchFormFields(){
      queryToolFactory.getAllSearchFields()
        .then(function( res ) {

          $scope.queryFormObj.fieldGroups = res;

        }, function() {

          //handle failure here

        });
    }

    //the change a tab
    function changeTab(index) {
      $scope.activeTabIndex = index;
    }

  }

})();
