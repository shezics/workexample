/**
 *  * Created by Shahzad on 7/07/2015.
 */
/*
* This user-form directive is created to be used in different pages
* e.g. signup page, update profile page, and add/update user for manage users page.
* */

(function () {

  'use strict';

  angular
    .module('workExample')
    .directive('userForm', userForm );

  userForm.$inject = ['$localStorage',  'FIELD_TYPES', 'userFactory', 'manageUsersFactory', 'manageOfficesFactory'];

  function userForm( $localStorage, FIELD_TYPES, userFactory, manageUsersFactory, manageOfficesFactory ) {
    return {
      restrict : 'A',
      templateUrl: 'app/shared/userForm/userForm.directive.view.html',
      scope: {
        user: '=',
        handlers: '=',
        //  an object that should contain editUser, hideControls, onSuccess, 'successMessage', ''
        config: '='
      },
      transclude: true,
      link: function ( $scope, $element, $attrs ) {

        /*listeners*/
        $scope.$watch('user', updateUserModal);
        $scope.$watch('user.TbUser.parent_user_id', updateRepAssoc);
        $scope.$watch('user.TbUser.office_id', updateOfficeAssoc);

        /*VM functions*/
        $scope.submitForm = $scope.handlers.submitForm = submitForm;
        $scope.resetForm = $scope.handlers.resetForm = resetForm;

        /*VM properties*/
        $scope.status = $scope.config.status || {};
        $scope.types = FIELD_TYPES;
        $scope.userTypes = getUserTypes();

        //for agree checks i.e. terms and contact
        $scope.agreeChecks = {};

        //for dropDowns
        $scope.repsDropDown = { reps: [], fetching: false };
        $scope.officesDropDown = { offices: [], fetching: false };

        /*Initialization*/
        var sendingMethod;

        setSendingMethod();
        checkForRepsDropDown();
        checkForOfficesDropDown();
        /*functions declarations*/

        //submits the form for adding or updating a user
        function submitForm() {
          var options, form;

          form = $scope.form;

          //handle form submission with invalid data
          if ( form.$invalid ) {
            resetFormStatus('error', 'please correct the red marked fields first.');
            return;
          }

          //handle if user submits form with no changes
          if ( !form.$dirty ) {
            resetFormStatus('error', 'No changes made to save.');
            return;
          }

          options = {
            payload: angular.copy( $scope.userObj ),
            editUser: $scope.config.editUser
          };

          resetFormStatus();
          $scope.status.sending = true;

          sendingMethod( options )
            .then(function( res ) {
              $scope.user = $scope.userObj;
              resetFormStatus('success', $scope.config.successMessage || (res.data.length + ' Users matched.') );

              //resets form validation and interaction state
              $scope.form.$setPristine();
              $scope.form.$setUntouched();

              //invokes success handler from config, if provided
              res = $scope.config.page === 'search-user' ? res.data : res.data.object;
              $scope.config.onSuccess && $scope.config.onSuccess( res );

            }, function( reason ) {
              resetFormStatus('error', reason);
            });
        }

        //resets status object without breaking reference.
        function resetFormStatus(type, message){
          $scope.status.sending = false;
          $scope.status.type = type;
          $scope.status.message = message;
        }

        // rests the user form to it's initial state.
        function resetForm() {

          //sets user model with initial values from $scope.user
          updateUserModal();

          //resets last attempt results.
          resetFormStatus();
          $scope.agreeChecks = {
            terms: false,
            contact: false
          };

          //resets form validation and interaction state
          $scope.form.$setPristine();
          $scope.form.$setUntouched();

          //invokes reset handler from config, if provided
          $scope.config.onReset && $scope.config.onReset();
        }

        //sets the sending method to hit API, according to given config
        function setSendingMethod(){
          sendingMethod = $scope.config.page === 'search-user' ? manageUsersFactory.searchUser: userFactory.addUpdateUser;
        }

        //checks if required in config - gets reps for the reps dropDown
        function checkForRepsDropDown(){
          if ( $scope.config.hideControls.indexOf('reps') === -1 ) {
            $scope.repsDropDown.fetching = true;
            userFactory.fetchRepsDropDown()
              .then(function(res){
                $scope.repsDropDown.reps = res;
                $scope.repsDropDown.fetching = false;
              }, function(reason){
                $scope.repsDropDown.fetching = false;
                //handle failure
              });
          }
        }

        //checks if required in config - gets offices for the offices dropDown
        function checkForOfficesDropDown(){
          if ( $scope.config.hideControls.indexOf('offices') === -1 && manageOfficesFactory.isOfficeAllowed() ) {
            $scope.officesDropDown.fetching = true;
            manageOfficesFactory.fetchOfficesDropDown({pageNum: 1})
              .then(function(res){
                $scope.officesDropDown.offices = res;
                $scope.officesDropDown.fetching = false;
              }, function(reason){
                $scope.officesDropDown.fetching = false;
                //handle failure
              });
          }
        }

        //to update user modal being used in form fields.
        function updateUserModal() {
          $scope.userObj = $scope.config.updateOriginalModal ? $scope.user : angular.copy( $scope.user );
        }

        //updates form with new Rep association
        function updateRepAssoc(){
          $scope.userObj.TbUser.parent_user_id = $scope.user.TbUser.parent_user_id;

          if ( $scope.user.TbAssociation ) {
            $scope.userObj.TbAssociation = $scope.user.TbAssociation = $scope.user.TbAssociation || {};

            $scope.userObj.TbAssociation.parent_user_id = $scope.user.TbAssociation.parent_user_id;
            $scope.userObj.TbAssociation.parent_user_name = $scope.user.TbAssociation.parent_user_name;
          }
        }

        //updates form with new Office association
        function updateOfficeAssoc(){
          $scope.userObj.TbUser.office_id = $scope.user.TbUser.office_id;
          $scope.userObj.TbUser.office_name = $scope.user.TbUser.office_name;
        }

        //to get defined or custom user-types according to logged-in user type.
        function getUserTypes() {
          var currentUserType, userTypes;

          if ( $scope.config.hideControls.indexOf('userType') >= 0 ) {
            return [];
          }

          currentUserType = $localStorage.user.TbUser.type;
          userTypes = angular.copy($scope.types.user);

          userTypes.splice( currentUserType - 1 );

          return userTypes;
        }
      }
    };
  }
})();
